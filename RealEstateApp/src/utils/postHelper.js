import geohash from "ngeohash";
import { postListing, uploadImage } from "../redux/actions/postListing";
import {
  convertIntoDollar,
  convertintoMeter,
} from "../utils/currencyConverter";
import { Translate } from "../core/i18n/IMLocalization";

export const getInitialState = (categories, selectedItem) => {
  let categoryData = categories.map((category) => ({
    key: category.id,
    label: category.name,
  }));

  if (selectedItem) {
    let {
      title,
      latitude,
      longitude,
      photos,
      description,
      place,
      floors,
      price,
      built,
      newConstruction,
      square,
      bathroom,
      bedroom,
      deposit,
      type,
    } = selectedItem;

    let category = categories.find(
      (category) => selectedItem.categoryID === category._id
    );
    return {
      categories: categoryData,
      category: category,
      title,
      noFloor: floors,
      description,
      location: {
        latitude,
        longitude,
      },
      localPhotos: photos,
      photoUrls: photos,
      price,
      formattedPrice: price,
      bedroom: bedroom,
      bathroom: bathroom,
      square,
      newConstruction,
      built,
      address: place,
      locationModalVisible: false,
      loading: false,
      deposit,
      type,
      selectedPhotoIndex: 0,
    };
  } else {
    return {
      categories: categoryData,
      category: { name: "Select..." },
      title: "",
      noFloor: "",
      description: "",
      location: {
        latitude: 31.5204,
        longitude: 74.3587,
      },
      localPhotos: [],
      photoUrls: [],
      price: "",
      formattedPrice: "",
      bedroom: [0],
      bathroom: [0],
      square: "",
      newConstruction: "",
      built: "",
      address: "",
      locationModalVisible: false,
      loading: false,
      deposit: "",
      type: "",
      selectedPhotoIndex: 0,
    };
  }
};

export const typesData = (Translate) => {
  return [
    {
      label: Translate("Buy"),
      value: "buy",
    },
    {
      label: Translate("Rent"),
      value: "rent",
    },
  ];
};

export const newConTypes = [
  { label: "Yes", value: "Yes" },
  { label: "No", value: "No" },
];

export const conDateTypes = [
  { label: "<2000", value: "<2000" },
  { label: ">2000", value: ">2000" },
];

export const onPost = async (
  state,
  setState,
  ServerConfig,
  user,
  onCancel,
  selectedItem,
  currencyRates
) => {
  if (!state.title) {
    alert(Translate("Title was not provided."));
    return;
  }
  if (!state.description) {
    alert(Translate("Description was not set."));
    return;
  } else if (!state.type) {
    alert(Translate("Type was not set."));
    return;
  } else if (!state.formattedPrice) {
    alert(Translate("Price is empty."));
    return;
  } else if (!state.square) {
    alert(Translate("square in feets was not set."));
    return;
  } else if (state.category.name == "select...") {
    alert(Translate("Category was not set."));
    return;
  } else if (state.type == "buy" && !state.deposit) {
    alert(Translate("Deposit was not set."));
    return;
  } else if (!state.bathroom) {
    alert(Translate("Bathrooms was not set."));
    return;
  } else if (!state.bedroom) {
    alert(Translate("Bedrooms was not set."));
    return;
  } else if (state.localPhotos.length == 0) {
    alert(Translate("Please choose at least one photo."));
    return;
  } else {
    setState({ ...state, loading: true });
    let photoUrls = [];
    const uploadPromiseArray = [];
    // state.localPhotos.forEach((obj) => {
    //   if (!obj.uri.startsWith("https://")) {
    //     uploadPromiseArray.push(
    //       new Promise(async (resolve, reject) => {
    //         try {
    //           let response = await uploadImage({
    //             uri: obj.uri,
    //             type: obj.type,
    //             name: obj.fileName,
    //           });
    //           photoUrls.push(response.data.secure_url);
    //           resolve();
    //         } catch (error) {
    //           reject(error);
    //         }
    //       })
    //     );
    //   }
    // });

    const hash = geohash.encode(
      state.location.latitude,
      state.location.longitude
    );

    try {
      await Promise.all(uploadPromiseArray);
      const uploadObject = {
        isApproved: !ServerConfig.isApprovalProcessEnabled,
        authorID: user._id,
        // author: user,
        categoryID: state.category._id,
        description: state.description,
        latitude: state.location.latitude,
        longitude: state.location.longitude,
        geoHash: hash,
        title: state.title,
        floors: state.noFloor,
        bedroom: state.bedroom,
        bathroom: state.bathroom,
        square: convertintoMeter(Number(state.square), user),
        type: state.type,
        ...(state.type == "buy" && { deposit: state.deposit }),
        newConstruction: state.newConstruction,
        built: state.built,
        price: convertIntoDollar(Number(state.price), user, currencyRates),
        place: state.address,
        photo: photoUrls.length > 0 ? photoUrls[0] : null,
        photos: photoUrls,
        photoURLs: photoUrls,
        delete: false,
      };
      const callBack = (success) => {
        setState({ ...state, loading: false });
        if (success) {
          onCancel();
        } else {
          alert("Something went try again!");
        }
      };
      postListing(selectedItem, uploadObject, callBack);
    } catch (error) {
      console.log("error hiwl upload", error);
      setState({ ...state, loading: false });
      alert(error);
    }
  }
};
