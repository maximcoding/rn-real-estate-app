export const selectorData = (categories) => {
  return [
    {
      title: "Rent or Buy",
      name: "rent",
      data: [
        { label: "Rent", value: "rent" },
        { label: "Buy", value: "buy" },
      ],
    },
    {
      title: "Status",
      name: "status",
      data: [
        { label: "Open", value: "open" },
        { label: "Close", value: "close" },
        { label: "Expired", value: "expired" },
      ],
    },
    {
      title: "New Construction",
      name: "newConstruction",
      data: [
        { label: "Yes", value: "Yes" },
        { label: "No", value: "No" },
      ],
    },
    {
      title: "Year Built",
      name: "built",
      data: [
        { label: "<2000", value: "<2000" },
        { label: ">2000", value: ">2000" },
      ],
    },
    {
      title: "Categories",
      name: "category",
      data: categories
        ? categories.map((c) => {
            return {
              label: c.name,
              value: c._id,
            };
          })
        : [],
    },
  ];
};
