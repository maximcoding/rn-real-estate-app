import React, { memo, useRef, useEffect, useState } from "react";
import { Modalize } from "react-native-modalize";
import { Dimensions, View, TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
const { height } = Dimensions.get("window");
const STATUSBAR_HEIGHT = 30;
const MODALIZE_FULL_HEIGHT = height - STATUSBAR_HEIGHT;
export default memo((props) => {
  const modalRef = useRef(null);
  const [isOpen, openModal] = useState(props.isOpen);

  const onOpen = (isOpen) => {
    const modal = modalRef.current;

    if (modal && isOpen) {
      modal.open();
    } else {
      modal.close();
    }
  };

  useEffect(() => {
    onOpen(props.isOpen);
    openModal(props.isOpen);
  }, [props.isOpen]);
  const closePanel = () => {
    props.closeModal();
    openModal(false);
  };
  return (
    <Modalize
      snapPoint={props.full ? MODALIZE_FULL_HEIGHT : height / 2}
      ref={modalRef}
      modalTopOffset={STATUSBAR_HEIGHT}
      closeOnTouchOutside={props.closeOnTouchOutside}
      isOpen={isOpen}
      onClose={closePanel}
      onPressCloseButton={closePanel}
    >
      <View style={styles.view}>{props.children}</View>
      <TouchableOpacity onPress={props.closeModal} style={styles.closeBtn}>
        <Icon size={18} color={"gray"} name="close" />
      </TouchableOpacity>
    </Modalize>
  );
});

const styles = StyleSheet.create({
  closeBtn: {
    position: "absolute",
    top: 10,
    right: 10,
  },
  view: {
    width: "100%",
    paddingTop: 30,
  },
  txt: {
    color: "black",
    opacity: 0.6,
    fontSize: 25,
  },
});
