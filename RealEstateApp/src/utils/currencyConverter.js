export const getSymbol = (user) => {
  switch (user.currency) {
    case "sign":
      return "‎₪";
    case "dollar":
      return "$";
    case "euro":
      return "€";
    default:
      return "$";
  }
};

export const getPrice = (price, user, currencyRates) => {
  switch (user.currency) {
    case "sign":
      return (price * currencyRates["ILS"]).toFixed(1);
    case "dollar":
      return (price * currencyRates["USD"]).toFixed(1);
    case "euro":
      return (price * currencyRates["EUR"]).toFixed(1);
    default:
      return price;
  }
};

export const convertIntoDollar = (price, user, currencyRates) => {
  switch (user.currency) {
    case "sign":
      return (price / currencyRates["ILS"]).toFixed(1);
    case "dollar":
      return price;
    case "euro":
      return (price / 1.2234).toFixed(1);
    default:
      return price;
  }
};

export const convertintoMeter = (value, user) => {
  switch (user.unit) {
    case "meter":
      return value;
    case "feet":
      return (value / 3.2808).toFixed(1);
    default:
      return value;
  }
};

export const unitConverter = (value, user) => {
  switch (user.unit) {
    case "meter":
      return value;
    case "feet":
      return (value * 3.2808).toFixed(1);
    default:
      return value;
  }
};
