import React from "react";
import { StyleSheet } from "react-native";
import { AppConfig } from "../AppConfig";
import { AppStyles } from "../AppStyles";

export const InitialRouteParams = {
  appConfig: AppConfig,
  appStyles: AppStyles,
};
export enum ScreenMode {
  Card = "card",
  Modal = "modal",
}
export enum HeaderMode {
  Screen = "screen",
  Float = "float",
  None = "none",
}
export const StackScreenAnimationConfig = {
  animation: "spring",
  config: {
    stiffness: 1000,
    damping: 100,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};
export const forFade = ({ current, closing }) => ({
  cardStyle: {
    opacity: current.progress,
  },
});
export const StackScreenOptions = {
  cardStyleInterpolator: forFade,
};

export const StackStyles = StyleSheet.create({
  headerStyle: {},
  headerTitleStyle: {
    ...AppStyles.text.headerTitleStyle,
  },
}); // props => <LogoTitle {...props} />
