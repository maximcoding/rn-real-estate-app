import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import {
  LoginScreen,
  SignUpScreen,
  SmsAuthenticationScreen,
  WelcomeScreen,
} from "../core/onboarding";
import {
  HeaderMode,
  InitialRouteParams,
  ScreenMode,
  StackScreenOptions,
} from "./AppNavigatorConfig";
import { PhoneVerification } from "./VerificationNavigation";

const Stack = createStackNavigator();

export const LoginNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={({ route, navigation }) => ({
        cardStyle: {},
        gestureEnabled: false,
        cardOverlayEnabled: false,
      })}
      mode={ScreenMode.Card}
      headerMode={HeaderMode.None}
    >
      <Stack.Screen
        name="Welcome"
        component={WelcomeScreen}
        initialParams={InitialRouteParams}
      />
      <Stack.Screen
        name="Login"
        options={StackScreenOptions}
        component={LoginScreen}
      />
      <Stack.Screen
        name="SignUp"
        options={StackScreenOptions}
        component={SignUpScreen}
      />
      <Stack.Screen
        name="Sms"
        options={StackScreenOptions}
        component={SmsAuthenticationScreen}
      />
      {/*TODO */}
      <Stack.Screen
        name="PhoneSms"
        options={StackScreenOptions}
        component={PhoneVerification}
      />
    </Stack.Navigator>
  );
};
