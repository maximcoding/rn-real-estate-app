import React, { useEffect, useRef, useState } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { LoadScreen } from "../core/onboarding/index";
import { Drawer } from "./Drawer";
import { LoginNavigation } from "./LoginNavigation";
import { NavigationContainer } from "@react-navigation/native";
import {
  HeaderMode,
  InitialRouteParams,
  StackStyles,
} from "./AppNavigatorConfig";
import firestore from "@react-native-firebase/firestore";
import auth from "@react-native-firebase/auth";
const usersRef = firestore().collection("users");
import { dispatchSetUserData } from "../redux/reducers";
import { getCurrencyPrices } from "../redux/actions/user";
import { useDispatch, useSelector } from "react-redux";
import * as RootNavigation from "./rootNavigation";
import { navigationRef } from "./rootNavigation";
import { getUser } from "../redux/actions/user";

const Stack = createStackNavigator();

const AppNavigator = () => {
  let dispatch = useDispatch();
  const isInitialMount = useRef(true);
  let [loader, setLoader] = useState(true);

  const { user, updator } = useSelector(({ auth }) => ({
    user: auth.user,
    updator: auth.updator,
  }));

  useEffect(() => {
    checkingCurrentUser();
    dispatch(getCurrencyPrices());
  }, []);

  useEffect(() => {
    console.log("in useEffect", user);

    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      if (user) {
        setLoader(false);
        RootNavigation.navigate("DrawerStack");
      } else {
        setLoader(false);
        RootNavigation.navigate("LoginStack", {
          appStyles: InitialRouteParams.appStyles,
          appConfig: InitialRouteParams.appConfig,
        });
      }
    }
  }, [user, updator]);

  const checkingCurrentUser = async () => {
    dispatch(getUser());
    
  };

  if (loader) {
    return <LoadScreen />;
  } else {
    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          screenOptions={StackStyles}
          initialRouteName="DrawerStack"
          headerMode={HeaderMode.None}
        >
          {user ? (
            <Stack.Screen
              name="DrawerStack"
              component={Drawer}
              initialParams={InitialRouteParams}
            />
          ) : (
            <Stack.Screen
              name="LoginStack"
              component={LoginNavigation}
              initialParams={InitialRouteParams}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
};

export const RootNavigator = AppNavigator;
