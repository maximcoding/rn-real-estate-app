import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import SettingScreen from "../components/SettingScreen";
import SigningInScreen from "../components/SigningInScreen";
import AboutScreen from "../components/AboutScreen";
import LegalScreen from "../components/LegalScreen";
import PrivacyScreen from "../components/PrivacyScreen";
import AgreementScreen from "../components/AgreementScreen";
import { HeaderMode, StackStyles } from "./AppNavigatorConfig";
import MyProfileScreen, {
  MyProfileScreenOptions,
} from "../components/MyProfileScreen";
import IMFormEditDetails from "../core/profile/ui/components/IMFormEditDetails";
import IMEditProfileScreen from "../core/profile/ui/components/IMEditProfileScreen";
import ListingScreen from "../screens/ListingScreen";
import DetailScreen from "../screens/DetailsScreen";
import MessagesScreen from "../screens/MessagesScreen";
import Contact from "../screens/contactScreen";
import { Translate } from "../core/i18n/IMLocalization";

const Stack = createStackNavigator();

export const MyProfileNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={StackStyles}
      initialRouteName="MyProfile"
      headerMode={HeaderMode.Float}
    >
      <Stack.Screen
        options={() => MyProfileScreenOptions()}
        name="MyProfile"
        component={MyProfileScreen}
      />
      <Stack.Screen name="Settings" component={SettingScreen} />
      <Stack.Screen name="Signing In" component={SigningInScreen} />
      <Stack.Screen name="About" component={AboutScreen} />
      <Stack.Screen name="Legal" component={LegalScreen} />
      <Stack.Screen name="Privacy" component={PrivacyScreen} />
      <Stack.Screen name="User Agreement" component={AgreementScreen} />
      <Stack.Screen
        name="AccountDetail"
        component={IMEditProfileScreen}
        options={{ title: Translate("Account Details") }}
      />
      <Stack.Screen
        name="EditDetails"
        component={IMFormEditDetails}
        options={{ title: Translate("Edit Details") }}
      />
      <Stack.Screen name="Detail" component={DetailScreen} />
      <Stack.Screen name="Messages" component={MessagesScreen} />
      <Stack.Screen name="Contact" component={Contact} />
      <Stack.Screen
        name="ListingScreen"
        component={ListingScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};
