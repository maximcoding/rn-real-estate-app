import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import SearchScreen, { SearchScreenOptions } from "../screens/SearchScreen";
import FilterScreenListing from "../screens/FilterScreenListing";
import SelectFilters from "../screens/SelectFilters";
import IMChatScreen, {
  IMChatScreenOptions,
} from "../core/chat/IMChatScreen/IMChatScreen";
import ListingProfileModal, {
  ListingProfileModalOptions,
} from "../components/ListingProfileModal";
import MapScreen, { MapScreenNavigationOptions } from "../screens/MapScreen";
import { HeaderMode, StackStyles } from "./AppNavigatorConfig";
import DetailsScreen, { DetailsScreenOptions } from "../screens/DetailsScreen";
import DetailScreen from "../screens/DetailsScreen";

const Stack = createStackNavigator();

export const SearchNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={StackStyles}
      initialRouteName="Search"
      headerMode={HeaderMode.Float}
    >
      <Stack.Screen
        name="Search"
        component={SearchScreen}
        options={({ navigation, route }) =>
          SearchScreenOptions(navigation, route)
        }
      />
      <Stack.Screen
        options={(params) => DetailsScreenOptions(params)}
        name="SearchDetail"
        component={DetailsScreen}
      />
      <Stack.Screen
        name="Filter Screen Listing"
        component={FilterScreenListing}
      />
      <Stack.Screen
        name="Filters"
        component={SelectFilters}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        options={(params) => DetailsScreenOptions(params)}
        name="ListingProfileModalDetailsScreen"
        component={DetailScreen}
      />
      <Stack.Screen
        //@ts-ignore
        options={(params) => IMChatScreenOptions(params)}
        name="PersonalChat"
        component={IMChatScreen}
      />
      <Stack.Screen
        options={() => ListingProfileModalOptions()}
        name="ListingProfileModal"
        component={ListingProfileModal}
      />
      <Stack.Screen
        options={() => MapScreenNavigationOptions()}
        name="Map"
        component={MapScreen}
      />
    </Stack.Navigator>
  );
};
