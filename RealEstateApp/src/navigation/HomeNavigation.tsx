import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import HomeScreen from "../screens/HomeScreen";

import ListingScreen, { ListingScreenOptions } from "../screens/ListingScreen";
import DetailScreen from "../screens/DetailsScreen";
import IMChatScreen, {
  IMChatScreenOptions,
} from "../core/chat/IMChatScreen/IMChatScreen";
import MapScreen, { MapScreenNavigationOptions } from "../screens/MapScreen";
import ListingProfileModal, {
  ListingProfileModalOptions,
} from "../components/ListingProfileModal";
import MyListingModal, {
  MyListingModalOptions,
} from "../components/MyListingModal";
import SavedListingScreen, {
  SavedListingScreenOptions,
} from "../screens/SavedListingScreen";
import IMContactUsScreen, {
  IMContactUsScreenOptions,
} from "../core/profile/ui/components/IMContactUsScreen";
import IMUserSettingsScreen, {
  IMUserSettingsOptions,
} from "../core/profile/ui/components/IMUserSettingsScreen";
import AdminDashboardScreen, {
  AdminDashboardScreenOptions,
} from "../screens/AdminDashboardScreen";
import { HeaderMode, ScreenMode, StackStyles } from "./AppNavigatorConfig";
import MyProfileScreen, {
  MyProfileScreenOptions,
} from "../components/MyProfileScreen";
const Stack = createStackNavigator();
import { useSelector } from "react-redux";

const MainStack = () => {
  
  let { updator } = useSelector((state) => state.languageReducer);
 
  return (
    <Stack.Navigator
      screenOptions={StackStyles}
      initialRouteName="Home"
      mode={ScreenMode.Card}
      headerMode={HeaderMode.Float}
    >
      <Stack.Screen
       options={{ headerShown: false }}
        name="Home"
        component={HomeScreen}
      />
      <Stack.Screen
        options={({ navigation, route }) =>
          ListingScreenOptions(navigation, route)
        }
        name="Listing"
        component={ListingScreen}
      />
      <Stack.Screen
        options={DetailScreen.navigationOptions}
        name="Detail"
        component={DetailScreen}
      />
      <Stack.Screen
        //@ts-ignore
        options={(params) => IMChatScreenOptions(params)}
        name="PersonalChat"
        component={IMChatScreen}
      />
      <Stack.Screen
        options={() => MapScreenNavigationOptions()}
        name="Map"
        component={MapScreen}
      />
      <Stack.Screen
        options={() => ListingProfileModalOptions()}
        name="ListingProfileModal"
        component={ListingProfileModal}
      />
      <Stack.Screen
        options={() => MyProfileScreenOptions()}
        name="MyProfile"
        component={MyProfileScreen}
      />
    </Stack.Navigator>
  );
};

export const HomeNavigation = () => {
  let { updator } = useSelector((state) => state.languageReducer);
  return (
    <Stack.Navigator screenOptions={StackStyles} headerMode={HeaderMode.None}>
      <Stack.Screen name="Home" component={MainStack} />
      <Stack.Screen
        options={() => MyProfileScreenOptions()}
        name="MyProfile"
        component={MyProfileScreen}
      />
      <Stack.Screen
        options={() => MyListingModalOptions()}
        name="MyListingModal"
        component={MyListingModal}
      />
      <Stack.Screen
        options={() => SavedListingScreenOptions()}
        name="SavedListingModal"
        component={SavedListingScreen}
      />
      <Stack.Screen
        options={(params) => DetailsScreenOptions(params)}
        name="MyListingDetailModal"
        component={DetailScreen}
      />
      <Stack.Screen
        //@ts-ignore
        options={(params) => IMChatScreenOptions(params)}
        name="PersonalChat"
        component={IMChatScreen}
      />
      <Stack.Screen
        options={({ navigation, route }) =>
          IMContactUsScreenOptions(navigation, route)
        }
        name="Contact"
        component={IMContactUsScreen}
      />
      <Stack.Screen
        options={({ navigation, route }) =>
          IMUserSettingsOptions(navigation, route)
        }
        name="Settings"
        component={IMUserSettingsScreen}
      />
      <Stack.Screen
        options={() => AdminDashboardScreenOptions()}
        name="AdminDashboard"
        component={AdminDashboardScreen}
      />
      {/* <Stack.Screen
        options={(params) => IMEditProfileScreenOptions(params)}
        name="AccountDetail"
        component={IMEditProfileScreen}
      /> */}
    </Stack.Navigator>
  );
};
