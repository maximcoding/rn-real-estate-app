import {
  forFade,
  HeaderMode,
  ScreenMode,
  StackScreenAnimationConfig,
} from "./AppNavigatorConfig";
import { FirstStepScreen } from "../core/onboarding/Verification/FirstStepScreen";
import { SecondStepScreen } from "../core/onboarding/Verification/SecondStepScreen";
import { createStackNavigator } from "@react-navigation/stack";

export type VerificationStackType = {
  FirstStep: undefined;
  SecondStep: undefined;
};

export const Stack = createStackNavigator<VerificationStackType>();

export const PhoneVerification = () => {
  return (
    <Stack.Navigator
      initialRouteName="FirstStep"
      screenOptions={({ route, navigation }) => ({
        cardStyle: {},
        gestureEnabled: false,
        cardOverlayEnabled: false,
        // headerStatusBarHeight: navigation.dangerouslyGetState().routes.indexOf(route) > 0 ? 0 : undefined, ...TransitionPresets.ModalPresentationIOS,
      })}
      mode={ScreenMode.Card}
      headerMode={HeaderMode.None}
    >
      <Stack.Screen
        options={{
          transitionSpec: {
            //@ts-ignore
            open: StackScreenAnimationConfig,
            //@ts-ignore
            close: StackScreenAnimationConfig,
          },
        }}
        name="FirstStep"
        component={FirstStepScreen}
      />
      <Stack.Screen
        options={{
          cardStyleInterpolator: forFade,
        }}
        name="SecondStep"
        component={SecondStepScreen}
      />
    </Stack.Navigator>
  );
};
