import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import CategoryScreen from "../screens/CategoryScreen";
import { Translate } from "../core/i18n/IMLocalization";
import ListingScreen, { ListingScreenOptions } from "../screens/ListingScreen";
import DetailScreen, { DetailsScreenOptions } from "../screens/DetailsScreen";
import IMChatScreen, {
  IMChatScreenOptions,
} from "../core/chat/IMChatScreen/IMChatScreen";
import ListingProfileModal, {
  ListingProfileModalOptions,
} from "../components/ListingProfileModal";
import MapScreen, { MapScreenNavigationOptions } from "../screens/MapScreen";
import { HeaderMode, StackStyles } from "./AppNavigatorConfig";
import { AppStyles } from "../AppStyles";
import { Fonts } from "../themes";
const Stack = createStackNavigator();

export const CollectionNavigation = () => {
  // @ts-ignore
  // @ts-ignore
  return (
    <Stack.Navigator
      screenOptions={StackStyles}
      initialRouteName="Category"
      headerMode={HeaderMode.Float}
    >
      <Stack.Screen
        name="Category"
        component={CategoryScreen}
        options={({ navigation, route }) => {
          return {
            title: Translate("Categories"),
            headerTitleStyle: AppStyles.text.headerTitleStyle,
          };
        }}
      />
      <Stack.Screen
        options={({ navigation, route }) =>
          ListingScreenOptions(navigation, route)
        }
        name="Listing"
        component={ListingScreen}
      />
      <Stack.Screen
        options={(params) => DetailsScreenOptions(params)}
        name="Detail"
        component={DetailScreen}
      />
      <Stack.Screen
        options={(params) => DetailsScreenOptions(params)}
        name="ListingProfileModalDetailsScreen"
        component={DetailScreen}
      />
      <Stack.Screen
        //@ts-ignore
        options={(params) => IMChatScreenOptions(params)}
        name="PersonalChat"
        component={IMChatScreen}
      />
      <Stack.Screen
        options={() => ListingProfileModalOptions()}
        name="ListingProfileModal"
        component={ListingProfileModal}
      />
      <Stack.Screen
        options={() => MapScreenNavigationOptions()}
        name="Map"
        component={MapScreen}
      />
    </Stack.Navigator>
  );
};
