import { Platform, StyleSheet, Dimensions, I18nManager } from "react-native";

const { width, height } = Dimensions.get("window");
const SCREEN_WIDTH = width < height ? width : height;
const numColumns = 2;
import { Configuration } from "./Configuration";
import { IS_ANDROID } from "./utils/statics";
import { Colors, Fonts } from "./themes";

export const AppStyles = {
  navThemeConstants: {
    light: {
      backgroundColor: "#fff",
      fontColor: "#000",
      activeTintColor: "#3875e8",
      inactiveTintColor: "#ccc",
      hairlineColor: "#e0e0e0",
    },
    dark: {
      backgroundColor: "#000",
      fontColor: "#fff",
      activeTintColor: "#3875e8",
      inactiveTintColor: "#888",
      hairlineColor: "#222222",
    },
    main: "#3875e8",
  },
  headerOptionsContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: 39,
    paddingVertical: 2,
    paddingHorizontal: 10,
  },
  screenContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    backgroundColor: Colors.backgroundColor,
  },
  screenTransparent: {
    width: "100%",
    height: "100%",
  },
  orTextStyle: {
    justifyContent: "center",
    alignItems: "center",
    color: Colors.text,
    fontSize: Fonts.size.regular,
    marginTop: 20,
    marginBottom: 10,
    alignSelf: "center",
  },
  tos: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
  },
  facebookContainer: {
    width: "70%",
    backgroundColor: Colors.facebook,
    borderRadius: 25,
    marginTop: 20,
    alignSelf: "center",
    padding: 10,
  },
  facebookText: {
    color: Colors.white,
  },
  inputContainer: {
    textAlign: I18nManager.isRTL ? "right" : "left",
    borderBottomWidth: 1,
    borderColor: Colors.border,
    color: Colors.primary,
    padding: 10,
    width: "70%",
    backgroundColor: Colors.inputColor,
    borderRadius: 4,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  buttonInverseContainer: {
    width: "70%",
    borderRadius: 8,
    marginTop: 20,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderColor: Colors.primary,
    borderWidth: 1,
  },
  buttonContainer: {
    backgroundColor: Colors.primary,
    width: "70%",
    borderRadius: 8,
    marginTop: 10,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  textInputWidth: "80%",
  pickerSelectStyles: StyleSheet.create({
    inputIOS: {
      height: 60,
      fontSize: Fonts.size.regular,
      borderRadius: 4,
    },
    inputAndroid: {
      fontSize: Fonts.size.regular,
      paddingHorizontal: 10,
      paddingVertical: 8,
    },
  }),
  menuBtn: {
    container: {
      backgroundColor: Colors.backgroundColor,
      borderRadius: 22.5,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
    },
    icon: {
      tintColor: "black",
      width: 15,
      height: 15,
    },
  },
  searchBar: {
    container: {
      marginLeft: Platform.OS === "ios" ? 30 : 0,
      backgroundColor: "transparent",
      borderBottomColor: "transparent",
      borderTopColor: "transparent",
      flex: 1,
    },
    input: {
      backgroundColor: Colors.backgroundColor,
      borderRadius: 10,
      color: "black",
    },
  },
  rightNavButton: {
    marginRight: 10,
  },
  borderRadius: {
    main: 25,
    small: 5,
  },
  navigationIcon: {
    height: 15,
    width: 15,
    tintColor: Colors.grey9,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  backArrowStyle: {
    resizeMode: "contain",
    tintColor: "#041b90",
    width: 20,
    height: 20,
    marginTop: Platform.OS === "ios" ? 50 : 20,
    marginLeft: 10,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  placeholderTextColor: Colors.placeholder,
  text: {
    headerTitleStyle: {
      fontWeight: "bold",
      fontSize: Fonts.size.normal,
    },
    headerTitle: {
      justifyContent: "center",
      width: "100%",
      textAlign: "center",
      fontWeight: "bold",
      fontSize: Fonts.size.normal,
    },
    headerButton: {
      backgroundColor: Colors.transparent,
      color: Colors.green,
      fontWeight: "300",
      fontSize: Fonts.size.normal,
      fontFamily: "Arial",
    },
    modalTitle: {
      justifyContent: "center",
      width: "100%",
      textAlign: "center",
      fontWeight: "bold",
      fontSize: Fonts.size.normal,
    },
    listTitle: {
      fontFamily: "Arial",
      fontSize: Fonts.size.regular,
      color: Colors.title,
    },
    listValue: {
      fontFamily: "Arial",
      textTransform: "capitalize",
      fontSize: Fonts.size.regular - 1,
      opacity: 0.6,
    },
    title: {
      fontFamily: "Arial",
      fontWeight: "bold",
      textTransform: "capitalize",
      color: Colors.title,
      fontSize: Fonts.size.large,
    },
    sectionTitle: {
      fontFamily: "Arial",
      paddingVertical: 10,
      fontWeight: "bold",
      textTransform: "capitalize",
      color: Colors.title,
      fontSize: Fonts.size.normal,
    },
    avatar:{
      width: 50,
      height: 50,
      borderRadius: 50,
      
    },
    agentCont:{
      flexDirection: "row",
    },
    agentDetail:{
      paddingLeft: 10,
      paddingTop:5
    },

    subTitle: {
      fontFamily: "Arial",
      paddingTop: 10,
      paddingBottom: 20,
      color: Colors.subTitle,
      fontSize: Fonts.style.subTitle,
    },
    description: {
      fontFamily: "Arial",
      fontSize: Fonts.size.small,
      opacity: 0.6,
    },
    buttonText: {
      color: Colors.white,
    },
    buttonInverseText: {
      color: Colors.primary,
    },
    buttonInverse: {
      fontSize: Fonts.size.large - 2,
    },
    placeholder: {
      fontSize: Fonts.size.regular,
      fontFamily: "Arial",
      color: Colors.placeholder,
    },
    input: {
      fontSize: Fonts.size.regular,
      color: Colors.title,
    },
  },
  logo: {
    width: 150,
    height: 150,
    alignSelf: "center",
  },
  logoImage: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
  tabStyles:{
    tabBar: {
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: 'rgb(197, 198, 202)',
      marginBottom: 20,
      marginHorizontal: 20
    },
    tabItem: {
      flex: 1,
      alignItems: 'center',
      paddingVertical: 12,
    },
    tabItemActive: {
      flex: 1,
      alignItems: 'center',
      paddingVertical: 12,
      borderBottomWidth: 2,
      borderBottomColor:  Colors.primary,
    },
    tabItemTxt: {
      fontSize: 14,
      fontFamily: 'CircularStd-Medium',
      color: 'rgb(197, 198, 202)',
    },
    tabItemTxtActive: {
      fontSize: 14,
      fontFamily: 'CircularStd-Medium',
      color:   Colors.primary,
    },
  }
};

export const AppIcon = {
  container: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 8,
    marginRight: 10,
  },
  containerMapBtn: {
    backgroundColor: "white",
    borderRadius: 20,
    paddingLeft: 8,
    paddingRight: 8,
    marginRight: 10,
  },
  containerAddListingBtn: {
    backgroundColor: "white",
    borderRadius: 20,
    paddingLeft: 8,
    paddingRight: 8,
  },
  style: {
    tintColor: Colors.green,
    width: 25,
    height: 25,
  },
};

export const HeaderButtonStyle = StyleSheet.create({
  headerButtonContainer: {
    justifyContent: "flex-end",
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: 20,
  },
  container: {
    padding: 10,
  },
  image: {
    justifyContent: "center",
    width: 35,
    height: 35,
    margin: 6,
  },
  rightButton: {
    color: Colors.tint,
    marginRight: 10,
    fontWeight: "normal",
    fontFamily: Fonts.family.main,
  },
  main: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 5,
    backgroundColor: Colors.backgroundColor,
    elevation: 3,
  },
  title: {
    fontSize: 17,
  },
});
export const ListStyle = StyleSheet.create({
  subtitleView: {
    minHeight: 55,
    flexDirection: "row",
    paddingTop: 5,
    marginLeft: 10,
  },
  leftSubtitle: {
    flex: 2,
  },
  time: {
    fontFamily: Fonts.family.main,
  },
  place: {
    fontWeight: "bold",
  },
  price: {
    fontFamily: Fonts.family.main,
  },
  avatarStyle: {
    height: 120,
    width: 120,
    marginLeft: 5,
  },
  itemContainer: {
    flex: 1,
    width: "100%",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
});
export const TwoColumnListStyle = {
  listings: {
    marginTop: 15,
    width: "100%",
    flex: 1,
  },
  showAllButtonContainer: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.border,
    height: 50,
    width: "100%",
    marginBottom: 30,
  },
  showAllButtonText: {
    padding: 10,
    textAlign: "center",
    color: Colors.border,
    fontFamily: Fonts.family.main,
    justifyContent: "center",
  },
  listingItemContainer: {
    justifyContent: "center",
    marginBottom: 20,
    marginRight: Configuration.home.listing_item.offset,
    width:
      (SCREEN_WIDTH - Configuration.home.listing_item.offset * 3) / numColumns,
    borderRadius: 5,
    paddingBottom: 10,
    backgroundColor: "white",
    shadowColor: Colors.shadow,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderWidth: 1,
    borderColor: Colors.border,
  },
  photo: {
    // position: "absolute",
  },
  listingPhoto: {
    width: "100%",
    height: Configuration.home.listing_item.height,
    borderRadius: 5,
  },
  savedIcon: {
    position: "absolute",
    top: Configuration.home.listing_item.saved.position_top,
    left:
      (SCREEN_WIDTH - Configuration.home.listing_item.offset * 3) / numColumns -
      Configuration.home.listing_item.offset -
      Configuration.home.listing_item.saved.size,
    width: Configuration.home.listing_item.saved.size,
    height: Configuration.home.listing_item.saved.size,
  },
  listingView: {
    height: 60,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: Colors.border,
    backgroundColor: Colors.backgroundColor,
  },
  listingDescription: {
    fontSize: Fonts.size.small,
    fontWeight: "500",
    color: "rgba(0,0,0,0.5)",
  },
  listingPlace: {
    paddingHorizontal: 10,
    fontFamily: Fonts.family.main,
    fontSize: Fonts.size.small,
    marginTop: 5,
  },
};
export const ModalSelectorStyle = {
  optionTextStyle: {
    fontSize: 17,
    fontFamily: Fonts.family.main,
  },
  selectedItemTextStyle: {
    fontSize: 18,
    fontFamily: Fonts.family.main,
    fontWeight: "bold",
  },
  optionContainerStyle: {
    backgroundColor: Colors.white,
  },
  cancelContainerStyle: {
    backgroundColor: Colors.white,
    borderRadius: 10,
  },
  sectionTextStyle: {
    fontSize: 21,
    color: Colors.title,
    fontFamily: Fonts.family.main,
    fontWeight: "bold",
  },

  cancelTextStyle: {
    fontSize: 21,
    fontFamily: Fonts.family.main,
  },
};
export const ModalHeaderStyle = {
  bar: {
    justifyContent: "flex-end",
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: 20,
  },
  text: {
    title: {
      fontSize: Fonts.size.regular,
      alignSelf: "center",
      fontWeight: "bold",
    },
  },
};
