import { Translate } from "../../../i18n/IMLocalization";

export const renderChangePassword = {
  type: "password",
  fields: [
    {
      name: "password",
      displayName: Translate("Password"),
      placeholder: Translate("Your password"),
    },
    {
      name: "newpassword",
      displayName: Translate("New Password"),
      placeholder: Translate("Your New password"),
    },
  ],
};

export const renderChangeName = {
  type: "name",
  fields: [
    {
      name: "cfname",
      displayName: Translate("New First name"),
      placeholder: Translate("Your First name"),
    },
    {
      name: "clname",
      displayName: Translate("New Last name"),
      placeholder: Translate("Your Last name"),
    },
  ],
};

export const renderChangeEmailOne = {
  type: "email",
  fields: [
    {
      name: "cemail",
      displayName: Translate("New Email"),
      placeholder: Translate("Your New Email"),
    },
    {
      name: "password",
      displayName: Translate("Password"),
      placeholder: Translate("Your password"),
    },
  ],
};

export const renderChangeEmailTwo = {
  type: "email",
  fields: [
    {
      name: "cemail",
      displayName: Translate("New Email"),
      placeholder: Translate("Your New Email"),
    },
  ],
};

export const renderChangePhone = {
  type: "phone",
  fields: [
    {
      name: "cphone",
      displayName: Translate("New Phone Number"),
      placeholder: Translate("Your New Phone"),
    },
  ],
};

export const renderData = [
  {
    title: Translate("PUBLIC PROFILE"),
    fields: [
      {
        name: "firstName",
        displayName: Translate("First Name"),
        placeholder: Translate("Your first name"),
      },
      {
        name: "lastName",
        displayName: Translate("Last Name"),
        placeholder: Translate("Your last name"),
      },
      {
        name: "registration",
        displayName: Translate("Registration Date"),
        placeholder: "",
      },
    ],
  },
  {
    title: Translate("PRIVATE DETAILS"),
    fields: [
      {
        name: "email",
        displayName: Translate("Email"),
        placeholder: Translate("Your email"),
      },
      {
        name: "phone",
        displayName: Translate("Phone Number"),
        placeholder: Translate("Your phone Number"),
      },
      {
        name: "password",
        displayName: Translate("Password"),
        placeholder: Translate("Your password"),
      },
    ],
  },
];
