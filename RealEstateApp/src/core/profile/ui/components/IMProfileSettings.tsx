import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import authManager from "../../../onboarding/AuthManager";
import { Translate } from "../../../i18n/IMLocalization";
import { DynamicStyleSheet } from "react-native-dark-mode";
import { Colors } from "../../../../themes";

export const IMProfileSettings = (props) => {
  const { navigation, onLogout, lastScreenTitle, appStyles, appConfig } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  const onSettingsTypePress = async (
    type,
    routeName,
    form,
    screenTitle,
    phone
  ) => {
    if (type === "Log Out") {
      authManager.logout(props.user);
      onLogout();
      navigation.navigate("LoadScreen", {
        appStyles: appStyles,
        appConfig: appConfig,
      });
    } else {
      navigation.navigate(lastScreenTitle + routeName, {
        appStyles: appStyles,
        form,
        screenTitle,
        phone,
      });
    }
  };

  const renderSettingsType = ({
    type,
    routeName,
    form,
    screenTitle,
    phone,
  }) => (
    <TouchableOpacity
      style={styles.settingsTypeContainer}
      onPress={() => onSettingsTypePress(type, routeName, form, screenTitle)}
    >
      <Text style={styles.settingsType}>{type}</Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <View style={styles.settingsTitleContainer}>
        <Text style={styles.settingsTitle}>{"GENERAL"}</Text>
      </View>
      <View style={styles.settingsTypesContainer}>
        {renderSettingsType({
          type: "Account Details",
          routeName: "EditProfile",
          form: appConfig.editProfileFields,
          screenTitle: Translate("Edit Profile"),
        })}
        {renderSettingsType({
          type: "Settings",
          routeName: "AppSettings",
          form: appConfig.userSettingsFields,
          screenTitle: Translate("User Settings"),
        })}
        {renderSettingsType({
          type: "Contact Us",
          routeName: "ContactUs",
          form: appConfig.contactUsFields,
          phone: appConfig.contactUsPhoneNumber,
          screenTitle: Translate("Contact Us"),
        })}
        {renderSettingsType({ type: "Log Out" })}
      </View>
    </View>
  );
};

IMProfileSettings.propTypes = {};

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      backgroundColor: Colors.whiteSmoke,
    },
    //Profile Settings
    settingsTitleContainer: {
      width: "100%",
      height: 55,
      justifyContent: "flex-end",
    },
    settingsTitle: {
      color: Colors.subTitle,
      paddingLeft: 10,
      fontSize: 14,
      paddingBottom: 6,
      fontWeight: "500",
    },
    settingsTypesContainer: {
      backgroundColor: Colors.backgroundColor,
    },
    settingsTypeContainer: {
      borderBottomColor: Colors.whiteSmoke,
      borderBottomWidth: 1,
      justifyContent: "center",
      alignItems: "center",
      height: 50,
    },
    settingsType: {
      color: Colors.foregroundColor,
      fontSize: 14,
      fontWeight: "500",
    },

    //Edit Profile
    contentContainer: {
      width: "100%",
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderColor: Colors.hairlineColor,
      backgroundColor: Colors.backgroundColor,
    },
    divider: {
      height: 0.5,
      width: "96%",
      alignSelf: "flex-end",
      backgroundColor: Colors.hairlineColor,
    },
    text: {
      fontSize: 14,
      color: Colors.title,
    },

    //app Settings
    appSettingsTypeContainer: {
      flexDirection: "row",
      borderBottomWidth: 0,
      justifyContent: "space-between",
      paddingHorizontal: 15,
    },
    appSettingsSaveContainer: {
      marginTop: 50,
      height: 45,
      backgroundColor: Colors.backgroundColor,
    },
    placeholderTextColor: { color: Colors.hairlineColor },
  });
};
