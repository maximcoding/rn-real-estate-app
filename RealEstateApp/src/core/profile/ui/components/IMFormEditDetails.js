import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  KeyboardAvoidingView,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  changePassword,
  changeEmail,
  updateUser,
  changePhoneNumber,
  sendOtp,
} from "../../../../redux/actions/user";
import { TouchableOpacity } from "react-native-gesture-handler";
import IMprofileCodeConfirm from "./IMprofileCodeConfirm";
import { Translate } from "../../../i18n/IMLocalization";
import IntlPhoneInput from "../../../../components/phoneInput";

export default function IMFormEditDetails(props) {
  let { appStyles } = props.route.params;
  const dispatch = useDispatch();
  let { user } = useSelector((state) => state.auth);
  const [state, setState] = useState({
    newpassword: "",
    cemail: "",
    cfname: "",
    clname: "",
    cphone: "",
    password: "",
  });
  const [loader, setLoader] = useState(false);
  const [visible, setVisible] = useState(false);
  const [confirmation, setConfirmation] = useState("");
  let { data } = props.route.params;

  const closeModal = () => {
    setVisible(false);
  };

  const stopLoader = (success) => {
    setLoader(false);
    if (success) {
      resetState();
      alert("Updated Successfully");
    }
  };

  const credLoader = (cred) => {
    setLoader(false);
    if (cred) {
      setConfirmation(cred);
      setVisible(true);
      //save them
    }
  };

  const resetState = () => {
    setState({
      ...state,
      cemail: "",
      cfname: "",
      clname: "",
      cphone: "",
      password: "",
      newpassword: "",
    });
  };

  const submitForm = () => {
    let { cemail, cfname, clname, password, newpassword, cphone } = state;
    if (data.type == "password") {
      if (password == "" || newpassword == "") {
        alert("Both Passwords are required");
        return;
      }
      setLoader(true);
      dispatch(
        updateUser(
          { ...user, password, newpassword, type: data.type },
          stopLoader
        )
      );
    } else if (data.type == "email") {
      if (cemail == "") {
        alert("Email is required");
        return;
      }
      setLoader(true);
      dispatch(
        updateUser({ ...user, email: cemail, type: data.type }, stopLoader)
      );
    } else if (data.type == "name") {
      if (cfname == "" || clname == "") {
        alert("First name and Last name is required");
        return;
      }
      setLoader(true);
      dispatch(
        updateUser({ ...user, firstName: cfname, lastName: clname }, stopLoader)
      );
    } else if (data.type == "phone") {
      if (state.cphone == "") {
        alert(Translate("Please enter a valid phone number."));
        return;
      }

      console.log("in data typ main");

      setLoader(true);

      dispatch(sendOtp(state.cphone, credLoader));
    }
  };

  const resendCode = (phone, cb) => {
    dispatch(sendOtp(phone, cb));
  };

  const onChangeText = ({ dialCode, unmaskedPhoneNumber }) => {
    let number = `${dialCode}${unmaskedPhoneNumber}`;
    setState({ ...state, cphone: number });
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text style={styles.title}>Change your {data.type}</Text>
        <Text style={styles.desc}>
          if you want to change the {data.type} associated with your account you
          may do so below, be sure to click the{" "}
          <Text style={styles.span}>Save Changes</Text> button when your are
          done
        </Text>
        {data.fields.map((obj) => {
          return (
            <View style={styles.inputContainer}>
              <Text style={{ paddingBottom: 5 }}>{obj.displayName}</Text>
              {data.type == "phone" ? (
                <>
                  <IntlPhoneInput
                    onChangeText={onChangeText}
                    defaultCountry="IL"
                  />
                </>
              ) : (
                <TextInput
                  placeholder={obj.placeholder}
                  style={styles.input}
                  value={state[obj.name]}
                  onChangeText={(text) =>
                    setState({ ...state, [obj.name]: text })
                  }
                />
              )}
            </View>
          );
        })}
        <TouchableOpacity
          style={styles.btn}
          disabled={loader}
          onPress={submitForm}
        >
          {loader ? (
            <ActivityIndicator size="small" color="white" />
          ) : (
            <Text style={styles.btnText}>Save Changes</Text>
          )}
        </TouchableOpacity>
        <IMprofileCodeConfirm
          appStyles={appStyles}
          confirmation={confirmation}
          phone={state.cphone}
          user={user}
          visible={visible}
          closeModal={closeModal}
          resendCode={resendCode}
          credLoader={credLoader}
        />
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    backgroundColor: "white",
  },
  title: {
    fontSize: 20,
    fontWeight: "700",
    paddingTop: 10,
  },
  span: {
    fontWeight: "700",
  },
  desc: {
    paddingTop: 10,
    textAlign: "justify",
    lineHeight: 22,
    color: "gray",
  },
  input: {
    width: "100%",
    height: 45,
    borderRadius: 5,
    marginTop: 4,
    borderColor: "#d3d3d3",
    borderWidth: 1,
    padding: 10,
  },
  inputContainer: {
    marginTop: 20,
    alignItems: "flex-start",
  },
  btn: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    backgroundColor: "#21c064",
    borderRadius: 5,
    width: "100%",
    height: 45,
  },
  btnText: {
    fontSize: 17,
    color: "white",
  },
});
