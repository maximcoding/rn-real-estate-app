import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Switch,
  ScrollView,
  Image,
  TouchableOpacity,
} from "react-native";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import { DynamicStyleSheet } from "react-native-dark-mode";
import Ionicons from "react-native-vector-icons/Ionicons";
import NumberFormat from "react-number-format";

import moment from "moment";
import {
  renderData,
  renderChangePassword,
  renderChangeName,
  renderChangeEmailOne,
  renderChangeEmailTwo,
  renderChangePhone,
} from "./IMUserFormData";
import { Images, Colors, Fonts } from "../../../../themes";
import Icon from "react-native-vector-icons/Ionicons";
import { Translate } from "../../../i18n/IMLocalization";
import { AppStyles } from "../../../../AppStyles";

export const IMFormComponent = (props) => {
  const { appStyles, user, navigation } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));

  useEffect(() => {
    setForm({
      ...form,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email ? user.email : Translate("Enter your Email"),
      phone: user.phone ? user.phone : Translate("not specified"),
    });
  }, [user]);

  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    password: "**********",
    registration: moment(user.createdAt).format("MM/DD/YYYY"),
    accountActive: false,
  });

  const navigateTo = (type) => {
    let data;
    switch (type) {
      case "firstName":
        data = renderChangeName;
        break;
      case "lastName":
        data = renderChangeName;
        break;
      case "email":
        data =
          user.accountType == "email"
            ? renderChangeEmailOne
            : renderChangeEmailTwo;
        break;
      case "phone":
        data = renderChangePhone;
        break;
      case "password":
        data = renderChangePassword;
        break;
      default:
        data = renderChangeName;
        break;
    }
    navigation.navigate("EditDetails", { data, appStyles });
  };

  const renderSwitchField = () => {
    return (
      <View
        style={[styles.settingsTypeContainer, styles.appSettingsTypeContainer]}
      >
        <Text style={AppStyles.text.listTitle}>
          {Translate("Disable Your Account")}
        </Text>
        <Switch
          value={form.accountActive}
          onValueChange={(value) => setForm({ ...form, accountActive: value })}
          style={{ transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }] }}
        />
      </View>
    );
  };

  const renderTextField = (formTextField, index, totalLen) => {
    return (
      <View>
        <View
          style={[
            styles.settingsTypeContainer,
            styles.appSettingsTypeContainer,
          ]}
        >
          <View>
            <Text style={[AppStyles.text.listTitle]}>
              {formTextField.displayName}:
            </Text>
            {formTextField.name == "phone" ? (
              <NumberFormat
                value={form[formTextField.name]}
                renderText={(text) => (
                  <Text style={[AppStyles.text.listValue, { paddingTop: 5 }]}>
                    {text}
                  </Text>
                )}
                format="(###) ###-####"
                displayType={"text"}
                mask="_"
              />
            ) : (
              <Text style={[AppStyles.text.listValue, { paddingTop: 5 }]}>
                {form[formTextField.name]}
              </Text>
            )}
          </View>
          {formTextField.name !== "registration" && (
            <TouchableOpacity onPress={() => navigateTo(formTextField.name)}>
              <Image
                style={AppStyles.navigationIcon}
                source={Images.rightArrow}
              />
            </TouchableOpacity>
          )}
        </View>
        {index < totalLen - 1 && <View style={styles.divider} />}
      </View>
    );
  };

  const renderSection = (section) => {
    return (
      <View>
        <View style={styles.settingsTitleContainer}>
          <Text style={styles.settingsTitle}>{section.title}</Text>
        </View>
        <View style={styles.contentContainer}>
          {section.fields.map((field, index) => {
            if (field.name == "password" && user.accountType == "phone") {
              return;
            }
            return renderTextField(field, index, section.fields.length);
          })}
        </View>
      </View>
    );
  };

  return (
    <View>
      <ScrollView contentContainerStyle={styles.container}>
        {renderData.map((item) => {
          return renderSection(item);
        })}
        <View style={{ padding: 5 }} />
        {renderSwitchField()}
      </ScrollView>
    </View>
  );
};

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flexGrow: 1,
      backgroundColor: Colors.whiteSmoke,
      paddingBottom: 70,
    },
    settingsTitleContainer: {
      width: "100%",
      height: 55,
      justifyContent: "flex-end",
    },
    settingsTitle: {
      fontSize: Fonts.size.regular,
      color: Colors.text,
      paddingHorizontal: 10,
      paddingBottom: 10,
    },
    settingsTypesContainer: {
      backgroundColor: Colors.backgroundColor,
    },
    settingsTypeContainer: {
      borderBottomColor: Colors.whiteSmoke,
      borderBottomWidth: 1,
      justifyContent: "center",
      alignItems: "center",
      height: 70,
    },
    settingsTypeContainer2: {
      justifyContent: "center",
      alignItems: "center",
      height: 50,
    },
    settingsType: {
      color: Colors.foregroundColor,
      fontSize: 14,
      fontWeight: "500",
    },

    itemNavigationIcon: {
      height: 15,
      width: 15,
    },
    //Edit Profile
    contentContainer: {
      width: "100%",
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderColor: Colors.hairlineColor,
      backgroundColor: Colors.backgroundColor,
    },
    contentContainer2: {
      width: "100%",
      backgroundColor: Colors.backgroundColor,
    },
    divider: {
      height: 0.5,
      width: "96%",
      alignSelf: "flex-end",
      backgroundColor: Colors.hairlineColor,
    },
    text: {
      fontSize: 14,
      color: Colors.title,
    },
    appSettingsTypeContainer: {
      flexDirection: "row",
      borderBottomWidth: 0,
      justifyContent: "space-between",
      paddingHorizontal: 15,
      backgroundColor: Colors.backgroundColor,
    },
    appSettingsSaveContainer: {
      marginTop: 4,
      height: 45,
      backgroundColor: Colors.backgroundColor,
    },
    placeholderTextColor: { color: Colors.hairlineColor },
  });
};
