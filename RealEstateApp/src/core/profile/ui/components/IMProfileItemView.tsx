import React from "react";
import { Image, Text, View, TouchableOpacity } from "react-native";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import { DynamicStyleSheet } from "react-native-dark-mode";
import { I18nManager } from "react-native";
import { Colors, Images } from "../../../../themes";
import Icon from "react-native-vector-icons/Ionicons";
import { AppStyles } from "../../../../AppStyles";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Ionicons from "react-native-vector-icons/Ionicons";
export const IMProfileItemView = (props) => {
  const { appStyles } = props;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const rightArrowIcon = Images.rightArrow;

  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={styles.itemContainer}>
        <Image style={[styles.icon, props.iconStyle]} source={props.icon} />
        <View style={{ paddingLeft: 7 }}>
          <Text style={[AppStyles.text.listTitle]}>{props.title}</Text>
        </View>
      </View>
      <Image style={AppStyles.navigationIcon} source={rightArrowIcon} />
    </TouchableOpacity>
  );
};

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      height: 50,
      width: "100%",
      borderBottomWidth: 0.5,
      borderBottomColor: Colors.border,
      paddingHorizontal: 20,
    },
    icon: {
      width: 24,
      height: 24,
    },
    itemContainer: {
      flex: 1,
      flexDirection: "row",
      alignItems: "center",
    },
    title: {
      color: Colors.title,
      fontSize: 14,
      fontWeight: "800",
      marginTop: 3,
    },
  });
};

export default dynamicStyles;
