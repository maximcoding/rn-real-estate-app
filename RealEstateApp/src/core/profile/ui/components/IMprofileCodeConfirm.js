import React, { useState } from "react";
import { View, StyleSheet, Dimensions, Text } from "react-native";
import CodeInput from "react-native-confirmation-code-input";
import ResendButton from "../../../../components/resendButton";
import { verifyPhoneNumber } from "../../../../redux/actions/user";
import { useDispatch, useSelector } from "react-redux";
import TNActivityIndicator from "../../../truly-native/TNActivityIndicator";
import Modal from "react-native-modal";
let { height } = Dimensions.get("window");

export default function IMprofileCodeConfirm({
  appStyles,
  confirmation,
  phone,
  user,
  visible,
  closeModal,
  resendCode,
  credLoader,
}) {
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();

  const stopLoader = (success) => {
    setLoader(false);
    closeModal();
    if (success) {
      alert("Updated Successfully");
    }
  };

  const verifiyCode = (code) => {
    setLoader(true);

    let updatedUser = {
      ...user,
      phone,
    };
    dispatch(verifyPhoneNumber(code, updatedUser, stopLoader));
  };

  const onFinishCheckingCode = (text) => {
    setLoader(true);
    verifiyCode(text);
  };
  return (
    <Modal style={styles.model} isVisible={visible}>
      <View style={styles.subContainer}>
        <Text style={styles.codeText}>ENTER CODE BELOW</Text>
        <CodeInput
          autoFocus
          codeLength={6}
          activeColor="#000"
          inactiveColor="#000"
          autoFocus={true}
          size={35}
          onFulfill={(text) => onFinishCheckingCode(text)}
          codeInputStyle={{ borderWidth: 0.5 }}
          keyboardType="numeric"
        />

        <ResendButton
          resendSms={() => {
            resendCode(phone, credLoader);
          }}
        />

        {loader && <TNActivityIndicator appStyles={appStyles} />}
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  model: { flex: 1, justifyContent: "center", alignItems: "center" },
  subContainer: {
    backgroundColor: "white",
    padding: 20,
    borderRadius: 5,
    height: height / 3,
  },
  codeText: {
    alignSelf: "center",
    paddingVertical: 10,
    color: "gray",
  },
});
