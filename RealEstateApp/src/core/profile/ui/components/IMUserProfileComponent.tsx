import React, { useEffect } from "react";
import {
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import firestore from "@react-native-firebase/firestore";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import { Translate } from "../../../i18n/IMLocalization";
import { IMProfileItemView } from "./IMProfileItemView";
import { ProfileProfilePictureSelector } from "../../../truly-native";
import { firebaseStorage } from "../../../firebase/storage";
import { firebaseAuth } from "../../../firebase";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import { Dimensions } from "react-native";
import { DynamicStyleSheet } from "react-native-dark-mode";
import { AppStyles } from "../../../../AppStyles";
import moment from "moment";
import { Colors } from "../../../../themes";

const { height } = Dimensions.get("window");
const imageSize = height * 0.14;

export const IMUserProfileComponent = (props) => {
  const {
    appStyles,
    menuItems,
    onUpdateUser,
    onLogout,
    onPressSettings,
  } = props;
  const unixTime = props?.user?._user?.metadata?.creationTime;
  const date = new Date(unixTime).toLocaleString();
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const userRef = firestore().collection("users").doc(props.user?.id);

  const onUserProfileUpdate = (querySnapshot) => {
    const data = querySnapshot.data();
    if (data) {
      onUpdateUser(data);
    }
  };

  const displayEmail = () => {
    return props.user
      ? `${props.user.firstName}  ${props.user.lastName}`
      : "name not exist";
  };

  // const displayDate = () => {
  //   return props.user
  //     ? moment(props.user.createdAt).format("DD/MM/YYYY")
  //     : "date not exist";
  // };

  // useEffect(() => {
  //   const unsubscribeUserFunction = userRef.onSnapshot(onUserProfileUpdate);
  //   return () => {
  //     unsubscribeUserFunction();
  //   };
  // }, []);

  const updateProfilePictureURL = (photoURI) => {
    if (photoURI == null) {
      // Remove profile photo action
      firebaseAuth
        .updateProfilePhoto(props.user?.userID, null)
        .then((finalRes) => {
          if (finalRes.success == true) {
            onUpdateUser({ ...props.user, profilePictureURL: null });
          }
        });
      return;
    }
    // If we have a photo, we upload it to Firebase, and then update the user
    firebaseStorage.uploadImage(photoURI).then((response) => {
      if (response.error) {
        // there was an error, fail silently
      } else {
        firebaseAuth
          .updateProfilePhoto(props.user?.userID, response.downloadURL)
          .then((finalRes) => {
            if (finalRes.success == true) {
              onUpdateUser({
                ...props.user,
                profilePictureURL: response.downloadURL,
              });
            }
          });
      }
    });
  };

  const renderMenuItem = (menuItem) => {
    const { title, icon, onPress, tintColor } = menuItem;
    return (
      <IMProfileItemView
        title={title}
        icon={icon}
        iconStyle={{ tintColor: tintColor }}
        onPress={onPress}
        appStyles={appStyles}
      />
    );
  };

  const myProfileScreenContent = () => {
    return (
      <>
        <View style={styles.container}>
          <StatusBar />
          <View style={styles.imageContainer}>
            <ProfileProfilePictureSelector
              setProfilePictureURL={updateProfilePictureURL}
              appStyles={appStyles}
              profilePictureURL={props?.user?.profilePictureURL}
            />
            <View style={{ flexDirection: "column", justifyContent: "center" }}>
              <Text style={styles.userName}>{displayEmail()}</Text>
            </View>
          </View>
          <ScrollView style={{ width: "100%", marginTop: 10 }}>
            {menuItems.map((menuItem, i) => {
              if ([3, 4, 5, 6, 7].includes(i) && props.user?.phone == null) {
                return;
              } else {
                return renderMenuItem(menuItem);
              }
            })}
            <TouchableOpacity onPress={onLogout}>
              <Text style={styles.logout}>{Translate("Logout")}</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </>
    );
  };

  return <>{myProfileScreenContent()}</>;
};

export const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      alignItems: "center",
      backgroundColor: Colors.backgroundColor,
    },
    tabs: {
      width: "100%",
      flexDirection: "row",
      justifyContent: "space-between",
      borderBottomColor: Colors.grey,
      borderBottomWidth: 0.5,
    },
    tabWrapper: {
      flex: 1,
      height: 50,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
    },
    buttonContainer: {
      height: 53,
      width: "98%",
      backgroundColor: "transparent",
      justifyContent: "center",
      alignItems: "center",
    },
    imageContainer: {
      margin: 5,
      display: "flex",
      justifyContent: "center",
      flexDirection: "row",
      alignItems: "center",
      width: "70%",
    },
    closeButton: {
      alignSelf: "flex-end",
      alignItems: "center",
      justifyContent: "center",
      marginTop: 20,
      marginRight: 15,
      backgroundColor: Colors.grey0,
      width: 28,
      height: 28,
      borderRadius: 20,
      overflow: "hidden",
    },
    closeIcon: {
      width: 27,
      height: 27,
    },
    userName: {
      // marginTop: 5,
      color: Colors.title,
      fontSize: 17,
    },
    memberSince: {
      // marginTop: 5,
      color: Colors.title,
      // fontSize: 17,
    },
    logout: {
      width: "90%",
      borderWidth: 1,
      color: Colors.title,
      fontSize: 15,
      paddingVertical: 10,
      borderColor: Colors.grey3,
      borderRadius: 5,
      marginVertical: 30,
      alignSelf: "center",
      textAlign: "center",
    },
  });
};
