import React, { Component } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import { Translate } from "../../../i18n/IMLocalization";
import { firebaseUser } from "../../../firebase";
import { IMFormComponent } from "./IMFormComponent";
import { dispatchSetUserData } from "../../../../redux/reducers";

export const IMUserSettingsOptions = (navigation, route) => {
  let appStyles = route.params.appStyles;
  let screenTitle = route.params.screenTitle || Translate("Settings");
  let currentTheme = appStyles.navThemeConstants.light;
  return {
    headerTitle: screenTitle,
    headerStyle: {
      backgroundColor: currentTheme.backgroundColor,
    },
    headerTintColor: currentTheme.fontColor,
  };
};

class IMUserSettingsScreen extends Component<any, any> {
  appStyles: any;
  form: any;
  initialValuesDict: any;
  didFocusSubscription: any;
  willBlurSubscription: any;
  constructor(props) {
    super(props);

    this.appStyles = props.route.params.appStyles || props.appStyles;
    this.form = props.route.params.form || props.form;
    this.initialValuesDict = props.user.settings || {};

    this.state = {
      form: props.form,
      alteredFormDict: {},
    };

    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onFormSubmit = () => {
    const user = this.props.user;
    var newSettings = user.settings || {};
    const form = this.form;
    const alteredFormDict = this.state.alteredFormDict;

    form.sections.forEach((section) => {
      section.fields.forEach((field) => {
        const newValue = alteredFormDict[field.key];
        if (newValue != null) {
          newSettings[field.key] = alteredFormDict[field.key];
        }
      });
    });

    let newUser = { ...user, settings: newSettings };
    firebaseUser.updateUserData(user.id, newUser);
    this.props.dispatchSetUserData({ user: newUser });
    this.props.navigation.goBack();
  };

  onFormChange = (alteredFormDict) => {
    this.setState({ alteredFormDict });
  };

  onFormButtonPress = (buttonField) => {
    this.onFormSubmit();
  };

  render() {
    return (
      <IMFormComponent
        form={this.form}
        initialValuesDict={this.initialValuesDict}
        onFormChange={this.onFormChange}
        navigation={this.props.navigation}
        appStyles={this.appStyles}
        onFormButtonPress={this.onFormButtonPress}
      />
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, { dispatchSetUserData })(
  IMUserSettingsScreen
);
