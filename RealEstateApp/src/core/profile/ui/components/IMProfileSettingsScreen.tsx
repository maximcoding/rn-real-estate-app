import React, { Component } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import { dispatchLogOut } from "../../../../redux/reducers";
import { Translate } from "../../../i18n/IMLocalization";
import { IMProfileSettings } from "./IMProfileSettings";

class IMProfileSettingsScreen extends Component {
  static navigationOptions = ({ screenProps, route }) => {
    let appStyles = route.params.appStyles;
    let currentTheme = appStyles.navThemeConstants.light;

    return {
      headerTitle: Translate("Profile Settings"),
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: currentTheme.hairlineColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);
    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
    this.lastScreenTitle = this.props.route.params.lastScreenTitle;
    this.appStyles = this.props.route.params.appStyles;
    this.appConfig = this.props.route.params.appConfig;
    if (!this.lastScreenTitle) {
      this.lastScreenTitle = "Profile";
    }
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onLogout = () => {
    this.props.dispatchLogOut();
  };

  render() {
    return (
      <IMProfileSettings
        navigation={this.props.navigation}
        onLogout={this.onLogout}
        lastScreenTitle={this.lastScreenTitle}
        user={this.props.user}
        appStyles={this.appStyles}
        appConfig={this.appConfig}
      />
    );
  }
}

IMProfileSettingsScreen.propTypes = {};

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, {
  dispatchLogOut: dispatchLogOut,
})(IMProfileSettingsScreen);
