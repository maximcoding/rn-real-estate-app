import { DynamicStyleSheet } from "react-native-dark-mode";
import { AppStyles } from "../../../AppStyles";
const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    welcomeTitle: appStyles.text.title,
    welcomeCaption: {
      ...appStyles.text.subTitle,
      marginBottom: 5,
    },
    loginContainer: appStyles.buttonContainer,
    loginText: appStyles.text.buttonText,
    SignUpContainer: appStyles.buttonInverseContainer,
    SignUpText: appStyles.text.buttonInverseText,
  });
};

export default dynamicStyles;
