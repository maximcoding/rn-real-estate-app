import React, { useState, useEffect } from "react";
import Button from "react-native-button";
import { Text, View, Image, ImageBackground, SafeAreaView } from "react-native";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import TNActivityIndicator from "../../truly-native/TNActivityIndicator";
import { Translate } from "../../i18n/IMLocalization";
import dynamicStyles from "./styles";
import { connect } from "react-redux";
import authManager from "../AuthManager";
import { dispatchSetUserData } from "../../../redux/reducers";
import { AppStyles, ListStyle } from "../../../AppStyles";
import { Images } from "../../../themes";

const WelcomeScreen = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const styles = useDynamicStyleSheet(dynamicStyles(AppStyles));
  const appConfig = props.route.params.appConfig;
  const appStyles = AppStyles;
  // useEffect(() => {
  //   tryToLoginFirst();
  // }, []);

  // const tryToLoginFirst = async () => {
  //   setIsLoading(true);
  //   authManager.retrievePersistedAuthUser().then((response) => {
  //     console.log("===================Trying to login first=====================", response)
  //     setIsLoading(false);
  //     if (response) {
  //       const user = response.user;
  //       props.dispatchSetUserData(user);
  //     }
  //   });
  // };

  // if (isLoading == true) {
  //   return <TNActivityIndicator appStyles={appStyles} />;
  // }

  return (
    <ImageBackground
      style={AppStyles.screenTransparent}
      source={Images.background}
    >
      <SafeAreaView
        style={[AppStyles.screenTransparent, { alignItems: "center" }]}
      >
        <Image
          style={[AppStyles.logo, { marginTop: 80, marginBottom: 30 }]}
          source={Images.logo}
        />
        <View
          style={{
            flexDirection: "column",
            alignItems: "center",
            marginTop: 5,
          }}
        >
          <Text style={[AppStyles.text.title, styles.welcomeTitle]}>
            {Translate("welcomeTitle")}
          </Text>
          <Text style={[styles.welcomeCaption]}>
            {Translate("welcomeCaption")}
          </Text>
        </View>
        <Button
          containerStyle={styles.loginContainer}
          style={styles.loginText}
          onPress={() => {
            appConfig.isSMSAuthEnabled
              ? props.navigation.navigate("Sms", {
                  isSigningUp: false,
                  appStyles,
                  appConfig,
                })
              : props.navigation.navigate("Login", { appStyles, appConfig });
          }}
        >
          {Translate("Log In")}
        </Button>
        <Button
          containerStyle={styles.SignUpContainer}
          style={styles.SignUpText}
          onPress={() => {
            appConfig.isSMSAuthEnabled
              ? props.navigation.navigate("Sms", {
                  isSigningUp: true,
                  appStyles,
                  appConfig,
                })
              : props.navigation.navigate("SignUp", { appStyles, appConfig });
          }}
        >
          {Translate("Sign Up")}
        </Button>
      </SafeAreaView>
    </ImageBackground>
  );
};

const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
  };
};

export default connect(mapStateToProps, { dispatchSetUserData })(WelcomeScreen);
