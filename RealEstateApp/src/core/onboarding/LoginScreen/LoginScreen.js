import React, { useState, useContext } from "react";
import {
  Text,
  TextInput,
  View,
  Alert,
  TouchableOpacity,
  Image,
  ImageBackground,
  SafeAreaView,
} from "react-native";
import Button from "react-native-button";
import { connect } from "react-redux";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import TNActivityIndicator from "../../truly-native/TNActivityIndicator";
import { Translate } from "../../i18n/IMLocalization";
import dynamicStyles from "./styles";
import authManager from "../AuthManager";
import { localizedErrorMessage } from "../../../utils/ErrorCode";
import { dispatchSetUserData } from "../../../redux/reducers";
// import { AuthContext } from "../../../AppContext";
import { AppStyles, ListStyle } from "../../../AppStyles";
import Images from "../../../themes/images";
import { Colors } from "../../../themes";
import { signinWithEmail } from "../../../redux/actions/user";

const LoginScreen = (props) => {
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("zeeshankamboh82@gmail.com");
  const [password, setPassword] = useState("123456");
  // const { signIn } = useContext(AuthContext);
  const appStyles = props.route.params.appStyles;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const appConfig = props.route.params.appConfig;

  const onPressLogin = () => {
    setLoading(true);

    props.signinWithEmail({ email, password }, () => setLoading(false));
  };

  // const onFBButtonPress = () => {
  //   authManager
  //     .loginOrSignUpWithFacebook(appConfig.appIdentifier)
  //     .then(response => {
  //       if (response.user) {
  //         const user = response.user;
  //         props.dispatchSetUserData(user);
  //         props.navigation.navigate('Home', { user: user });
  //       } else {
  //         Alert.alert('', localizedErrorMessage(response.error), [{ text: Translate('OK') }], {
  //           cancelable: false,
  //         });
  //       }
  //     });
  // }

  return (
    <ImageBackground
      style={AppStyles.screenTransparent}
      source={Images.background}
    >
      <SafeAreaView style={[AppStyles.screenTransparent]}>
        <TouchableOpacity
          style={{ alignSelf: "flex-start" }}
          onPress={() => props.navigation.goBack()}
        >
          <Image
            style={[AppStyles.backArrowStyle, { marginTop: 6 }]}
            source={Images.backArrow}
          />
        </TouchableOpacity>
        <Text style={AppStyles.text.headerTitle}>{Translate("Login")}</Text>
        <Image
          style={[AppStyles.logo, { marginTop: 35, marginBottom: 45 }]}
          source={Images.logo}
        />
        <KeyboardAwareScrollView
          style={{ flex: 1, width: "100%" }}
          keyboardShouldPersistTaps="always"
        >
          <TextInput
            style={[AppStyles.inputContainer, { marginTop: 5 }]}
            placeholder={Translate("Email")}
            placeholderTextColor={Colors.placeholder}
            onChangeText={(text) => setEmail(text)}
            value={email}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
          />
          <TextInput
            style={[AppStyles.inputContainer, { marginTop: 5 }]}
            placeholderTextColor={Colors.placeholder}
            secureTextEntry
            placeholder={Translate("Password")}
            onChangeText={(text) => setPassword(text)}
            value={password}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
          />
          <Button
            containerStyle={[styles.loginContainer, { marginTop: 12 }]}
            style={styles.loginText}
            onPress={() => onPressLogin()}
          >
            {Translate("Log In")}
          </Button>
          <Text style={AppStyles.orTextStyle}> {Translate("OR")}</Text>
          {/*<Button*/}
          {/*  containerStyle={styles.facebookContainer}*/}
          {/*  style={styles.facebookText}*/}
          {/*  onPress={() => onFBButtonPress()}*/}
          {/*>*/}
          {/*  {Translate('Login With Facebook')}*/}
          {/*</Button>*/}
          {appConfig.isSMSAuthEnabled && (
            <Button
              containerStyle={styles.phoneNumberContainer}
              onPress={() =>
                props.navigation.navigate("Sms", {
                  isSigningUp: false,
                  appStyles,
                  appConfig,
                })
              }
            >
              {Translate("Login with phone number")}
            </Button>
          )}

          {loading && <TNActivityIndicator appStyles={appStyles} />}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </ImageBackground>
  );
};
const mapStateToProps = ({ auth }) => {
  return {
    user: auth.user,
    isAdmin: auth.user && auth.user?.isAdmin,
  };
};
export default connect(mapStateToProps, {
  signinWithEmail,
})(LoginScreen);
