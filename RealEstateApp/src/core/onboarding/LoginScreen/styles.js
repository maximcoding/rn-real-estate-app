import { I18nManager } from "react-native";
import { DynamicStyleSheet } from "react-native-dark-mode";
import { AppStyles } from "../../../AppStyles";
import App from "../../../App";
import Colors from "../../../themes/colors";
import Fonts from "../../../themes/fonts";
const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: Colors.backgroundColor,
    },
    title: {
      fontSize: 30,
      fontWeight: "bold",
      color: Colors.foregroundColor,
      marginTop: 25,
      marginBottom: 20,
      alignSelf: "stretch",
      textAlign: "left",
      marginLeft: 30,
    },
    loginContainer: appStyles.buttonContainer,
    loginText: {
      color: Colors.white,
    },
    placeholder: {
      color: Colors.fire,
    },
    InputContainer: appStyles.inputContainer,
    phoneNumberContainer: {
      marginTop: 6,
    },
    loginTextButton: AppStyles.text.button,
  });
};

export default dynamicStyles;
