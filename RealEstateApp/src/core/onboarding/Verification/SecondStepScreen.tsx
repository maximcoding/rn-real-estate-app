import React, { useContext, useState } from "react";
import { SafeAreaView, StyleSheet, View, ImageBackground } from "react-native";
import { Text, Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { Colors, Fonts, Images } from "../../../themes";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { LoadScreen } from "../index";
import { Translate } from "../../i18n/IMLocalization";
import { AppStyles, ListStyle } from "../../../AppStyles";

export const SecondStepScreen = () => {
  // const { t, i18n } = useContext(LocalizationContext);
  const [otp, setOpt] = useState([]);
  const navigation = useNavigation();
  const nextStep = () => {
    return navigation.navigate("Home");
  };
  return (
    <SafeAreaView>
      <ImageBackground style={styles.background} source={Images.background}>
        <View style={styles.container}>
          <View style={styles.languages}></View>
          <View style={styles.centerContainer}>
            <Text style={AppStyles.text.title}>
              {Translate("enter_the_code")}
            </Text>
            <Text style={styles.subTitle}>
              {Translate("we_will_send_you_sms")}
            </Text>
            <View style={styles.inputContainer}>
              <OTPInputView
                style={styles.input}
                pinCount={4}
                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                // onCodeChanged = {code => { this.setState({code})}}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled={(code) => {
                  console.log(`Code is ${code}, you are good to go!`);
                }}
              />
            </View>
            <Text style={styles.validation}>
              {Translate("messages.validations.verification_code")}
            </Text>
            <Button
              title="Next"
              style={{ width: 100 }}
              onPress={() => nextStep()}
            />
          </View>
        </View>
        <LoadScreen />
      </ImageBackground>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  background: { width: "100%", height: "100%" },
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
  },
  centerContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    paddingTop: "25%",
  },
  languages: {
    paddingTop: 50,
    paddingLeft: 50,
    display: "flex",
    alignItems: "flex-start",
  },
  title: {
    minHeight: 40,
    display: "flex",
    textAlign: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  subTitle: {
    paddingBottom: "5%",
  },
  inputContainer: {
    display: "flex",
    flexDirection: "row",
  },
  input: {
    height: 50,
    width: "50%",
    margin: 12,
  },
  underlineStyleBase: {
    color: "black",
    width: 35,
    height: 50,
    borderWidth: 0,
    borderBottomWidth: 1,
    fontSize: Fonts.size.large,
  },
  validation: {
    color: Colors.fire,
    paddingBottom: 30,
  },
  underlineStyleHighLighted: {
    borderColor: Colors.underlayColor,
  },
});
