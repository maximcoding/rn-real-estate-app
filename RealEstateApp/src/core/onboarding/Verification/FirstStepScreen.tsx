import React, { useContext, useState } from "react";
import { StyleSheet, View, SafeAreaView, ImageBackground } from "react-native";
import { Text, Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { Colors, Fonts, Images } from "../../../themes";
import { TextInputMask } from "react-native-masked-text";
import { LanguagesSelect } from "../../../components/LanguagesSelect";
import { LocalizationContext } from "../../i18n/Localization";
import { Translate } from "../../i18n/IMLocalization";
import { AppStyles, ListStyle } from "../../../AppStyles";

export const FirstStepScreen = () => {
  const { t, i18n } = useContext(LocalizationContext);
  const IL_CODE = "+972-5";
  const [mobile, setMobile] = useState(`${IL_CODE}`);
  const navigation = useNavigation();
  const nextStep = () => {
    return navigation.navigate("SecondStep");
  };
  const updatePhoneNumber = (value, other) => {
    setMobile(value);
  };
  return (
    <SafeAreaView>
      <ImageBackground style={styles.background} source={Images.background}>
        <View style={styles.container}>
          
          <View style={styles.centerContainer}>
            <Text style={AppStyles.text.title}>
              {Translate("verify_mobile")}
            </Text>
            <Text style={styles.subTitle}>
              {Translate("we_will_send_you_sms")}
            </Text>
            <View style={styles.inputContainer}>
              <TextInputMask
                style={styles.input}
                type={"cel-phone"}
                options={{
                  withDDD: true,
                  maskType: "INTERNATIONAL",
                }}
                value={mobile}
                onChangeText={updatePhoneNumber}
              />
            </View>
            <Button
              title="Next"
              style={{ width: 100 }}
              onPress={() => nextStep()}
            />
          </View>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  background: { width: "100%", height: "100%" },
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
  },
  centerContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    paddingTop: "25%",
  },
  languages: {
    paddingTop: 50,
    paddingRight: 50,
    display: "flex",
    alignItems: "flex-end",
  },
  title: {
    minHeight: 40,
    display: "flex",
    textAlign: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  subTitle: {
    paddingBottom: "5%",
  },
  inputContainer: {
    display: "flex",
    justifyContent: "center",
    paddingBottom: 30,
  },
  input: {
    height: 50,
    width: 240,
    margin: 12,
    paddingRight: 12,
    fontSize: Fonts.size.large,
    borderBottomWidth: 1,
    borderColor: Colors.underlayColor,
  },
});
