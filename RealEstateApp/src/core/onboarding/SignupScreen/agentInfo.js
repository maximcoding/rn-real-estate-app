import React, { memo } from "react";
import { TextInput } from "react-native";
import { AppStyles } from "../../../AppStyles";
import Colors from "../../../themes/colors";

export default memo(({ setCompanyID, setAddress, companyID, address }) => {
  console.log("companyID in child", companyID);
  return (
    <>
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5 }]}
        placeholder={"Company ID"}
        placeholderTextColor={Colors.placeholder}
        onChangeText={(text) => setCompanyID(text)}
        underlineColorAndroid="transparent"
        keyboardType="numeric"
        value={companyID}
      />
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5 }]}
        placeholder={"Office Address"}
        placeholderTextColor={Colors.placeholder}
        onChangeText={(text) => setAddress(text)}
        underlineColorAndroid="transparent"
        value={address}
      />
    </>
  );
});
