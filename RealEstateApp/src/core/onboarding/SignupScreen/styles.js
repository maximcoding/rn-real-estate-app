import { Dimensions, I18nManager } from "react-native";
import { DynamicStyleSheet } from "react-native-dark-mode";
import { AppStyles } from "../../../AppStyles";

const { height } = Dimensions.get("window");
const imageSize = height * 0.232;
const photoIconSize = imageSize * 0.27;
import Colors from "../../../themes/colors";
import Fonts from "../../../themes/fonts";

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: Colors.backgroundColor,
    },
    background: { width: "100%", height: "100%" },
    title: {
      fontSize: Fonts.size.title,
      fontWeight: "bold",
      color: Colors.foregroundColor,
      marginTop: 25,
      marginBottom: 30,
      alignSelf: "stretch",
      textAlign: "left",
      marginLeft: 35,
    },
    content: {
      paddingLeft: 50,
      paddingRight: 50,
      textAlign: "center",
      color: Colors.foregroundColor,
    },
    loginContainer: AppStyles.buttonContainer,
    signUpText: {
      color: Colors.backgroundColor,
    },
    placeholder: {
      color: "red",
    },
    InputContainer: {
      height: 42,
      borderWidth: 1,
      borderColor: Colors.grey3,
      paddingLeft: 20,
      color: Colors.title,
      width: "80%",
      alignSelf: "center",
      marginTop: 20,
      alignItems: "center",
      borderRadius: 25,
      textAlign: I18nManager.isRTL ? "right" : "left",
    },

    SignUpContainer: AppStyles.buttonContainer,
    SignUpText: {
      color: Colors.backgroundColor,
    },
    image: {
      width: "100%",
      height: "100%",
    },
    imageBlock: {
      flex: 2,
      flexDirection: "row",
      width: "100%",
      justifyContent: "center",
      alignItems: "center",
    },
    imageContainer: {
      height: imageSize,
      width: imageSize,
      borderRadius: imageSize,
      shadowColor: Colors.shadow,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.1,
      overflow: "hidden",
    },
    formContainer: {
      width: "100%",
      flex: 4,
      alignItems: "center",
    },
    photo: {
      marginTop: imageSize * 0.77,
      marginLeft: -imageSize * 0.29,
      width: photoIconSize,
      height: photoIconSize,
      borderRadius: photoIconSize,
    },

    addButton: {
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: Colors.backgroundColor,
      opacity: 0.8,
      zIndex: 2,
    },
    PhoneNumberContainer: {
      alignSelf: "center",
    },
    smsText: {
      color: Colors.primary,
    },
  });
};

export default dynamicStyles;
