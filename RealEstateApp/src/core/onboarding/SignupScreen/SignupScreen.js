import React, { useState, memo } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import Button from "react-native-button";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import dynamicStyles from "./styles";
import TNActivityIndicator from "../../truly-native/TNActivityIndicator";
import TNProfilePictureSelector from "../../truly-native/TNProfilePictureSelector/TNProfilePictureSelector";
import { Translate } from "../../i18n/IMLocalization";
import { useDispatch } from "react-redux";
import { AppStyles } from "../../../AppStyles";
import TermsOfUseView from "../components/TermsOfUseView";
import Images from "../../../themes/images";
import { AppConfig } from "../../../AppConfig";
import { signUpWithEmail } from "../../../redux/actions/user";
import { TabView } from "react-native-tab-view";
import AgentInfo from "./agentInfo";
import BesicInfo from "./besicInfo";

const SignUpScreen = (props) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [companyID, setCompanyID] = useState("");
  const [address, setAddress] = useState("");

  const [profilePictureURL, setProfilePictureURL] = useState(null);
  const [loading, setLoading] = useState(false);
  const [index, setIndex] = useState(0);
  const routes = [
    { key: "basicInfo", title: "Basic Info" },
    { key: "agentInfo", title: "Agent Info" },
  ];

  const dispatch = useDispatch();
  const appConfig = props.route.params.appConfig;
  const styles = useDynamicStyleSheet(dynamicStyles(styles));

  const onRegister = () => {
    setLoading(true);

    const userDetails = {
      firstName,
      lastName,
      email,
      password,
      companyID,
      address,
      photoURI: profilePictureURL,
      appIdentifier: appConfig.appIdentifier,
    };

    dispatch(signUpWithEmail(userDetails, () => setLoading(false)));
  };

  const renderScene = ({ route }) => {
    switch (route.key) {
      case "basicInfo":
        return (
          <BesicInfo
            email={email}
            password={password}
            firstName={firstName}
            lastName={lastName}
            setEmail={setEmail}
            setPassword={setPassword}
            setFirstName={setFirstName}
            setLastName={setLastName}
          />
        );
      case "agentInfo":
        return (
          <AgentInfo
            setCompanyID={setCompanyID}
            companyID={companyID}
            address={address}
            setAddress={setAddress}
          />
        );
    }
  };

  const renderTabBar = (props) => {
    const activeIndex = props.navigationState.index;

    return (
      <View style={AppStyles.tabStyles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const active = activeIndex === i;

          const tabItemStyle = active
            ? AppStyles.tabStyles.tabItemActive
            : AppStyles.tabStyles.tabItem;

          const tabItemTxtStyle = active
            ? AppStyles.tabStyles.tabItemTxtActive
            : AppStyles.tabStyles.tabItemTxt;

          return (
            <TouchableOpacity
              key={route.title}
              style={tabItemStyle}
              onPress={() => setIndex(i)}
            >
              <View style={{ alignItems: "center" }}>
                <Text style={tabItemTxtStyle}>{route.title}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <ImageBackground style={styles.background} source={Images.background}>
      <KeyboardAwareScrollView
        style={{ flex: 1, width: "100%" }}
        keyboardShouldPersistTaps="always"
      >
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image style={AppStyles.backArrowStyle} source={Images.backArrow} />
        </TouchableOpacity>
        <Text style={[AppStyles.text.headerTitle, { paddingBottom: 45 }]}>
          {Translate("Create new account")}
        </Text>
        <TNProfilePictureSelector
          setProfilePictureURL={setProfilePictureURL}
          appStyles={AppStyles}
        />
        <View>
          <TabView
            lazy={false}
            onIndexChange={(i) => setIndex(i)}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
          />
        </View>
        <Button
          containerStyle={AppStyles.buttonContainer}
          style={styles.signUpText}
          onPress={() => onRegister()}
        >
          {Translate("Sign Up")}
        </Button>
        {appConfig.isSMSAuthEnabled && (
          <View>
            <Text style={AppStyles.orTextStyle}>{Translate("OR")}</Text>
            <Button
              containerStyle={styles.PhoneNumberContainer}
              onPress={() =>
                props.navigation.navigate("Sms", {
                  isSigningUp: true,
                  appStyles: AppStyles,
                  appConfig: AppConfig,
                })
              }
            >
              {Translate("Sign up with phone number")}
            </Button>
          </View>
        )}
        <TermsOfUseView tosLink={appConfig.tosLink} style={AppStyles.tos} />
      </KeyboardAwareScrollView>
      {loading && <TNActivityIndicator appStyles={AppStyles} />}
    </ImageBackground>
  );
};

export default memo(SignUpScreen);
