import React, { memo } from "react";
import { TextInput } from "react-native";
import { AppStyles } from "../../../AppStyles";
import Colors from "../../../themes/colors";
import { Translate } from "../../i18n/IMLocalization";

export default memo((props) => {
  return (
    <>
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5 }]}
        placeholder={Translate("First Name")}
        placeholderTextColor={Colors.placeholder}
        onChangeText={(text) => props.setFirstName(text)}
        value={props.firstName}
        underlineColorAndroid="transparent"
      />
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5 }]}
        placeholder={Translate("Last Name")}
        placeholderTextColor={Colors.placeholder}
        onChangeText={(text) => props.setLastName(text)}
        value={props.lastName}
        underlineColorAndroid="transparent"
      />
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5 }]}
        placeholder={Translate("Email Address")}
        placeholderTextColor={Colors.placeholder}
        onChangeText={(text) => props.setEmail(text)}
        value={props.email}
        underlineColorAndroid="transparent"
        autoCapitalize="none"
      />
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5, marginBottom: 15 }]}
        placeholder={Translate("Password")}
        placeholderTextColor={Colors.placeholder}
        secureTextEntry
        onChangeText={(text) => props.setPassword(text)}
        value={props.password}
        underlineColorAndroid="transparent"
        autoCapitalize="none"
      />
    </>
  );
});
