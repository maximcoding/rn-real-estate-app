import React from "react";
import { Text, Linking, View } from "react-native";
import { Translate } from "../../i18n/IMLocalization";

const TermsOfUseView = (props) => {
  const { tosLink, style } = props;
  return (
    <View style={style}>
      <Text style={{ fontSize: 12 }}>
        {Translate("By creating an account you agree with our")}
      </Text>
      <Text
        style={{ color: "blue", fontSize: 12 }}
        onPress={() => Linking.openURL(tosLink)}
      >
        {Translate("Terms of Use")}
      </Text>
    </View>
  );
};

export default TermsOfUseView;
