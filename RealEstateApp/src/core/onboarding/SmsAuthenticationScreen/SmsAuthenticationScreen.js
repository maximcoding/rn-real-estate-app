import React, { useState, memo } from "react";
import {
  Text,
  View,
  Alert,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import Button from "react-native-button";
import CodeInput from "react-native-confirmation-code-input";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useDynamicStyleSheet } from "react-native-dark-mode";
import dynamicStyles from "./styles";
import TNActivityIndicator from "../../truly-native/TNActivityIndicator";
import TNProfilePictureSelector from "../../truly-native/TNProfilePictureSelector/TNProfilePictureSelector";
import { Translate } from "../../i18n/IMLocalization";
import { useDispatch } from "react-redux";
import TermsOfUseView from "../components/TermsOfUseView";
import ResendButton from "../../../components/resendButton";
import { Images } from "../../../themes";
import { AppStyles } from "../../../AppStyles";
import { AppConfig } from "../../../AppConfig";

import {
  sendOtp,
  signupWithPhone,
  signinWithPhone,
} from "../../../redux/actions/user";
import { TabView } from "react-native-tab-view";
import AgentInfo from "../SignupScreen/agentInfo";
import BesicInfo from "./besicInfo";

import PhoneInputRender from "./phoneInput";

const SmsAuthenticationScreen = (props) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [loading, setLoading] = useState(false);
  const [isPhoneVisible, setIsPhoneVisible] = useState(true);
  const [phoneNumber, setPhoneNumber] = useState("");
  const [profilePictureURL, setProfilePictureURL] = useState(null);
  const [companyID, setCompanyID] = useState("");
  const [address, setAddress] = useState("");

  const appStyles = props.route.params.appStyles;
  const styles = useDynamicStyleSheet(dynamicStyles(appStyles));
  const appConfig = props.route.params.appConfig;
  const { isSigningUp } = props.route.params;

  const [index, setIndex] = useState(0);
  const routes = [
    { key: "basicInfo", title: "Basic Info" },
    { key: "agentInfo", title: "Agent Info" },
  ];

  const dispatch = useDispatch();

  const onChangeText = ({ dialCode, unmaskedPhoneNumber }) => {
    let number = `${dialCode}${unmaskedPhoneNumber}`;
    setPhoneNumber(number);
  };

  const signInWithPhoneNumber = (userValidPhoneNumber) => {
    setLoading(true);
    dispatch(
      sendOtp(userValidPhoneNumber, (type) => {
        if (type === "success") {
          setIsPhoneVisible(false);
        }

        setLoading(false);
      })
    );
  };

  const SignUpWithPhoneNumber = (smsCode) => {
    const userDetails = {
      firstName,
      lastName,
      phone: phoneNumber,
      appIdentifier: appConfig.appIdentifier,
      photoURI: profilePictureURL,
    };
    setLoading(true);

    dispatch(signupWithPhone(userDetails, smsCode, () => setLoading(false)));
  };

  const onPressSend = () => {
    if (phoneNumber !== "") {
      setPhoneNumber(phoneNumber);

      signInWithPhoneNumber(phoneNumber);
    } else {
      Alert.alert(
        "",
        Translate("Please enter a valid phone number."),
        [{ text: Translate("OK") }],
        {
          cancelable: false,
        }
      );
    }
  };

  const renderScene = ({ route }) => {
    switch (route.key) {
      case "basicInfo":
        return (
          <BesicInfo
            firstName={firstName}
            lastName={lastName}
            setFirstName={setFirstName}
            setLastName={setLastName}
            onPressSend={onPressSend}
            onChangeText={onChangeText}
          />
        );
      case "agentInfo":
        return (
          <AgentInfo
            setCompanyID={setCompanyID}
            companyID={companyID}
            address={address}
            setAddress={setAddress}
          />
        );
    }
  };

  const renderTabBar = (props) => {
    const activeIndex = props.navigationState.index;

    return (
      <View style={AppStyles.tabStyles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const active = activeIndex === i;

          const tabItemStyle = active
            ? AppStyles.tabStyles.tabItemActive
            : AppStyles.tabStyles.tabItem;

          const tabItemTxtStyle = active
            ? AppStyles.tabStyles.tabItemTxtActive
            : AppStyles.tabStyles.tabItemTxt;

          return (
            <TouchableOpacity
              key={route.title}
              style={tabItemStyle}
              onPress={() => setIndex(i)}
            >
              <View style={{ alignItems: "center" }}>
                <Text style={tabItemTxtStyle}>{route.title}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  const onFinishCheckingCode = (newCode) => {
    setLoading(true);
    if (isSigningUp) {
      SignUpWithPhoneNumber(newCode);
    } else {
      // login
      let data = {
        phone: phoneNumber,
      };

      dispatch(signinWithPhone(data, newCode, () => setLoading(false)));
    }
  };

  const codeInputRender = () => {
    return (
      <View style={styles.codeInput}>
        <CodeInput
          autoFocus
          codeLength={6}
          activeColor="#000"
          inactiveColor="#000"
          autoFocus={true}
          size={35}
          onFulfill={(text) => onFinishCheckingCode(text)}
          codeInputStyle={{ borderWidth: 0.5 }}
          keyboardType="numeric"
        />
        <ResendButton resendSms={() => signInWithPhoneNumber(phoneNumber)} />
      </View>
    );
  };

  const renderAsSignUpState = () => {
    return (
      <>
        <Text style={[AppStyles.text.headerTitle, { paddingBottom: 45 }]}>
          {Translate("Create new Account")}
        </Text>
        <TNProfilePictureSelector
          setProfilePictureURL={setProfilePictureURL}
          appStyles={AppStyles}
        />
        <View>
          {isPhoneVisible ? (
            <TabView
              lazy={false}
              onIndexChange={(i) => setIndex(i)}
              renderScene={renderScene}
              renderTabBar={renderTabBar}
              navigationState={{ index, routes }}
            />
          ) : (
            codeInputRender()
          )}
        </View>

        <Text style={AppStyles.orTextStyle}>{Translate("OR")}</Text>
        <Button
          containerStyle={styles.signWithEmailContainer}
          onPress={() =>
            props.navigation.navigate("SignUp", {
              appStyles: AppStyles,
              appConfig: AppConfig,
            })
          }
        >
          {Translate("Sign up with Email")}
        </Button>
      </>
    );
  };

  const renderAsLoginState = () => {
    return (
      <View style={[AppStyles.screenTransparent, { alignItems: "center" }]}>
        <Text style={[AppStyles.text.headerTitle]}>{Translate("Log In")}</Text>
        <Image
          style={[AppStyles.logo, { marginVertical: 35 }]}
          source={Images.logo}
        />
        {isPhoneVisible ? (
          <PhoneInputRender
            onPressSend={onPressSend}
            onChangeText={onChangeText}
          />
        ) : (
          codeInputRender()
        )}
        <Text style={AppStyles.orTextStyle}>{Translate("OR")}</Text>

        <Button
          containerStyle={[styles.signWithEmailContainer, { paddingTop: 10 }]}
          onPress={() =>
            props.navigation.navigate("Login", {
              appStyles: AppStyles,
              appConfig: AppConfig,
            })
          }
        >
          {Translate("Sign in with Email")}
        </Button>
      </View>
    );
  };

  return (
    <ImageBackground
      style={AppStyles.screenTransparent}
      source={Images.background}
    >
      <KeyboardAwareScrollView
        style={{ flex: 1, width: "100%" }}
        keyboardShouldPersistTaps="always"
      >
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image style={[AppStyles.backArrowStyle]} source={Images.backArrow} />
        </TouchableOpacity>
        {isSigningUp ? renderAsSignUpState() : renderAsLoginState()}
        {isSigningUp && (
          <TermsOfUseView tosLink={appConfig.tosLink} style={AppStyles.tos} />
        )}
      </KeyboardAwareScrollView>
      {loading && <TNActivityIndicator appStyles={AppStyles} />}
    </ImageBackground>
  );
};

export default memo(SmsAuthenticationScreen);
