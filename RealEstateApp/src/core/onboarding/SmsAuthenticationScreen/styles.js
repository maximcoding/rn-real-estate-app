import { DynamicStyleSheet } from "react-native-dark-mode";
import { I18nManager } from "react-native";
import Colors from "../../../themes/colors";
import Fonts from "../../../themes/fonts";

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    background: { width: "100%", height: "100%" },
    container: appStyles.screenContainer,
    codeInput: {
      display: "flex",
    },
    flagStyle: {
      width: 35,
      height: 25,
      borderColor: Colors.title,
      borderBottomLeftRadius: 25,
      borderTopLeftRadius: 25,
      transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
    },
    input: {
      flex: 1,
      borderLeftWidth: 1,
      borderRadius: 3,
      borderColor: Colors.grey3,
      color: Colors.title,
      fontSize: 17,
      fontWeight: "700",
      backgroundColor: Colors.backgroundColor,
    },
    codeFieldContainer: {
      borderWidth: 1,
      borderColor: Colors.grey3,
      width: "80%",
      height: 42,
      marginTop: 30,
      alignSelf: "center",
      borderRadius: 25,
      alignItems: "center",
      overflow: "hidden",
      backgroundColor: Colors.backgroundColor,
    },
    signWithEmailContainer: {
      // color: Colors.primary,
    },
    tos: {
      alignItems: "center",
      justifyContent: "center",
      height: 40,
    },
    inputContainer: {
      display: "flex",
      justifyContent: "center",
      paddingBottom: 30,
    },
    phoneInput: {
      height: 50,
      width: 240,
      margin: 12,
      paddingRight: 12,
      fontSize: Fonts.size.large,
      borderBottomWidth: 1,
      borderColor: Colors.underlayColor,
    },
  });
};

export default dynamicStyles;
