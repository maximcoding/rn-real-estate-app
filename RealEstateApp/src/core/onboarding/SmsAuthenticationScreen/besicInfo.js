import React, { memo } from "react";
import { TextInput } from "react-native";
import { AppStyles } from "../../../AppStyles";
import Colors from "../../../themes/colors";
import { Translate } from "../../i18n/IMLocalization";
import PhoneInputRender from "./phoneInput";

export default memo((props) => {
  return (
    <>
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5 }]}
        placeholder={Translate("First Name")}
        placeholderTextColor={Colors.placeholder}
        onChangeText={(text) => props.setFirstName(text)}
        value={props.firstName}
        underlineColorAndroid="transparent"
      />
      <TextInput
        style={[AppStyles.inputContainer, { marginTop: 5 }]}
        placeholder={Translate("Last Name")}
        placeholderTextColor={Colors.placeholder}
        onChangeText={(text) => props.setLastName(text)}
        value={props.lastName}
        underlineColorAndroid="transparent"
      />
      <PhoneInputRender
        onPressSend={props.onPressSend}
        onChangeText={props.onChangeText}
      />
    </>
  );
});
