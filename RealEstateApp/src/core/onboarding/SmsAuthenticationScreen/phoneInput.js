import React from "react";
import Button from "react-native-button";
import { AppStyles } from "../../../AppStyles";
import { Translate } from "../../i18n/IMLocalization";
import IntlPhoneInput from "../../../components/phoneInput";

const phoneInputRender = ({ onPressSend, onChangeText }) => {
  return (
    <>
      <IntlPhoneInput onChangeText={onChangeText} defaultCountry="IL" />
      <Button
        containerStyle={[AppStyles.buttonContainer, { marginTop: 5 }]}
        style={[AppStyles.text.buttonText]}
        onPress={() => onPressSend()}
      >
        {Translate("Send code")}
      </Button>
    </>
  );
};

export default phoneInputRender;
