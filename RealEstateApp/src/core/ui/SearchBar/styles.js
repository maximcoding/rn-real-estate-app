import { DynamicStyleSheet } from "react-native-dark-mode";
import { Colors } from "../../../themes";
const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      // width: Platform.OS === 'ios' ? '120%' : '100%',
      width: "95%",
      alignSelf: "center",
      marginBottom: 4,
    },
    cancelButtonText: {
      color: Colors.foregroundColor,
      fontSize: 16,
      marginBottom: 5,
    },
    searchInput: {
      fontSize: 16,
      color: Colors.title,
      backgroundColor: Colors.whiteSmoke,
    },
  });
};

export default dynamicStyles;
