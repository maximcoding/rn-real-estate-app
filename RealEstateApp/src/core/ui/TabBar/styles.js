import { DynamicStyleSheet } from "react-native-dark-mode";
import { ifIphoneX } from "react-native-iphone-x-helper";

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    tabBarContainer: {
      ...ifIphoneX(
        {
          height: 80,
        },
        {
          height: 45,
        }
      ),
      backgroundColor: Colors.backgroundColor,
      flexDirection: "row",
      borderTopWidth: 0.5,
      borderTopColor: Colors.hairlineColor,
    },
    tabContainer: {
      backgroundColor: Colors.backgroundColor,
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    tabIcon: {
      ...ifIphoneX(
        {
          width: 25,
          height: 25,
        },
        {
          width: 22,
          height: 22,
        }
      ),
    },
    focusTintColor: {
      tintColor: Colors.foregroundColor,
    },
    unFocusTintColor: {
      tintColor: Colors.bottomTintColor,
    },
  });
};

export default dynamicStyles;
