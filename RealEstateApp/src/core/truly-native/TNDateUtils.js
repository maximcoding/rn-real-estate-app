import moment from "moment";
import { Translate } from "../i18n/IMLocalization";

const monthNames = [
  Translate("Jan"),
  Translate("Feb"),
  Translate("Mar"),
  Translate("Apr"),
  Translate("May"),
  Translate("Jun"),
  Translate("Jul"),
  Translate("Aug"),
  Translate("Sep"),
  Translate("Oct"),
  Translate("Nov"),
  Translate("Dec"),
];

const TNDateFormattedTimestamp = (timestamp) => {
  if (timestamp) {
    const date = new Date(timestamp);
    let time = moment(date);
    if (moment().diff(time, "days") == 0) {
      return time.format("H:mm");
    } else if (moment().diff(time, "week") == 0) {
      return time.fromNow();
    } else {
      return `${monthNames[date.getMonth()]} ${time.format("D, Y")}`;
    }
  }
  return "";
};

export default TNDateFormattedTimestamp;
