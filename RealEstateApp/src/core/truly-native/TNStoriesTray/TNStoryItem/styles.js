import { DynamicStyleSheet } from "react-native-dark-mode";

const imageContainerWidth = 66;
const imageWidth = imageContainerWidth - 6;

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      margin: 8,
    },
    imageContainer: {
      width: imageContainerWidth,
      height: imageContainerWidth,
      borderRadius: Math.floor(imageContainerWidth / 2),
      borderColor: Colors.foregroundColor,
      borderWidth: 2,
      justifyContent: "center",
      alignItems: "center",
    },
    image: {
      width: imageWidth,
      height: imageWidth,
      borderRadius: Math.floor(imageWidth / 2),
      borderColor: Colors.backgroundColor,
      borderWidth: 1,
      overflow: "hidden",
    },
    text: {
      fontSize: 12,
      textAlign: "center",
      color: Colors.subTitle,
      paddingTop: 5,
    },
    isOnlineIndicator: {
      position: "absolute",
      backgroundColor: "#4acd1d",
      height: 16,
      width: 16,
      borderRadius: 16 / 2,
      borderWidth: 3,
      borderColor: Colors.backgroundColor,
      right: 5,
      bottom: 0,
    },
  });
};

export default dynamicStyles;
