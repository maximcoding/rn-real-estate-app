import { DynamicStyleSheet } from "react-native-dark-mode";
import { Colors } from "../../../themes";
const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    storiesContainer: {
      backgroundColor: Colors.backgroundColor,
      marginBottom: 5,
      flexDirection: "row",
    },
    seenStyle: {
      borderColor: Colors.grey,
      borderWidth: 1,
    },
  });
};

export default dynamicStyles;
