import { DynamicStyleSheet } from "react-native-dark-mode";

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    title: {
      fontSize: 30,
      fontWeight: "bold",
      alignSelf: "center",
      color: Colors.title,
      marginBottom: 15,
    },
    description: {
      alignSelf: "center",
      color: Colors.title,
      textAlign: "center",
      width: "85%",
      lineHeight: 20,
    },
    buttonContainer: {
      backgroundColor: Colors.foregroundColor,
      width: "75%",
      height: 45,
      alignSelf: "center",
      borderRadius: 10,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 30,
    },
    buttonName: {
      color: "#ffffff",
      fontSize: 16,
      fontWeight: "600",
    },
  });
};

export default dynamicStyles;
