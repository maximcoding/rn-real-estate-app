import React, { Component } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import { firebaseNotification } from "..";
import { setNotifications } from "../redux";
import { AppStyles } from "../../../AppStyles";
import { IMNotification } from "../Notification/IMNotification";
import Colors from "../../../themes/colors";

class IMNotificationScreen extends Component {
  static navigationOptions = ({ screenProps }) => {
    let currentTheme = AppStyles.navThemeConstants.light;

    return {
      headerTitle: "Notifications",
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
        borderBottomColor: Colors.borderBottomColor,
      },
      headerTintColor: currentTheme.fontColor,
    };
  };

  constructor(props) {
    super(props);

    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );

    this.lastScreenTitle = this.props.route.params.lastScreenTitle;
    if (!this.lastScreenTitle) {
      this.lastScreenTitle = "Profile";
    }
    this.appStyles = this.props.route.params.appStyles;
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );

    this.notificationUnsubscribe = firebaseNotification.subscribeNotifications(
      this.props.user.id,
      this.onNotificationCollection
    );
  }

  componentWillUnmount() {
    this.notificationUnsubscribe();
    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();
    return true;
  };

  onNotificationCollection = (notifications) => {
    this.props.setNotifications(notifications);
  };

  onNotificationPress = async (notification) => {};

  render() {
    return (
      <IMNotification
        onNotificationPress={this.onNotificationPress}
        notifications={this.props.notifications}
        appStyles={this.appStyles}
      />
    );
  }
}

IMNotificationScreen.propTypes = {};

const mapStateToProps = ({ notifications, auth }) => {
  return {
    user: auth.user,
    notifications: notifications.notifications,
  };
};

export default connect(mapStateToProps, { setNotifications })(
  IMNotificationScreen
);
