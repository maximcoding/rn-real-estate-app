import { DynamicStyleSheet } from "react-native-dark-mode";

const imageSize = 40;

import Colors from "../../../themes/colors";

const dynamicStyles = (appStyles) => {
  return new DynamicStyleSheet({
    container: {
      flex: 1,
      flexDirection: "row",
      backgroundColor: Colors.backgroundColor,
    },
    userImageMainContainer: {
      flex: 1,
      margin: 0,
      justifyContent: "center",
      alignItems: "center",
      marginBottom: 7,
    },
    userImageContainer: {
      width: imageSize,
      height: imageSize,
      borderWidth: 0,
      alignItems: "flex-end",
    },
    userImage: {
      width: imageSize,
      height: imageSize,
    },
    notificationItemBackground: {
      flex: 1,
    },
    notificationItemContainer: {
      flexDirection: "row",
      width: "95%",
      height: 82,
      alignSelf: "center",
      borderBottomColor: Colors.hairlineColor,
      borderBottomWidth: 0.3,
    },
    notificationLabelContainer: {
      flex: 5.4,
      justifyContent: "center",
    },
    description: {
      color: Colors.title,
      fontSize: 12,
      paddingVertical: 6,
    },
    name: {
      fontWeight: "700",
    },
    moment: {
      fontSize: 10,
    },
    seenNotificationBackground: {
      backgroundColor: Colors.backgroundColor,
    },
    unseenNotificationBackground: {
      backgroundColor: Colors.mainButtonColor,
    },
  });
};

export default dynamicStyles;
