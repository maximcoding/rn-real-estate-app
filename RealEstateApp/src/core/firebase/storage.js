import storage from "@react-native-firebase/storage";
import { Platform } from "react-native";
import RNFS from "react-native-fs";
import { ErrorCode } from "../../utils/ErrorCode";

const uploadFileWithProgressTracking = async (
  filename,
  uploadUri,
  callbackSuccess,
  callbackError
) => {
  // Success handler with SUCCESS is called multiple times on Android. We need work around that to ensure we only call it once
  var finished = false;
  storage()
    .ref(filename)
    .putFile(uploadUri)
    .on(
      storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        if (snapshot.state == storage.TaskState.SUCCESS) {
          if (finished == true) {
            return;
          }
          finished = true;
        }
        callbackSuccess(snapshot, storage.TaskState.SUCCESS);
      },
      callbackError
    );
};

const uploadImage = (uri) => {
  return new Promise(async (resolve, _reject) => {
    console.log("uri ==", uri);
    const filename = uri.substring(uri.lastIndexOf("/") + 1);
    console.log("filename ==", filename);
    const uploadUri = Platform.OS === "ios" ? uri.replace("file://", "") : uri;
    const data = await RNFS.readFile(uploadUri, "base64");
    storage()
      .ref(filename)
      .putString(data, "base64")
      .then(async (snapshot) => {
        const url = await storage().ref(filename).getDownloadURL();
        console.log("snapshot: ", url);
        resolve({ downloadURL: url });
      })
      .catch((_error) => {
        console.log("_error: ", _error);
        resolve({ error: ErrorCode.photoUploadFailed });
      });
  });
};

const firebaseStorage = {
  uploadImage,
  uploadFileWithProgressTracking,
};

export { firebaseStorage };
