import firestore from "@react-native-firebase/firestore";
import auth from "@react-native-firebase/auth";
import { ErrorCode } from "../../utils/ErrorCode";

const usersRef = firestore().collection("users");

export const retrievePersistedAuthUser = () => {
  return new Promise((resolve) => {
    resolve(null);
    auth().onAuthStateChanged((user) => {
      if (user) {
        // resolve(user);
        usersRef
          .doc(user.uid)
          .get()
          .then((document) => {
            resolve(document.data());
          })
          .catch((errror) => {
            resolve(null);
          });
      } else {
        resolve(null);
      }
    });
  });
};

const signInWithCredential = (credential, appIdentifier) => {
  return new Promise((resolve, _reject) => {
    auth()
      .signInWithCredential(credential)
      .then((response) => {
        const isNewUser = response.additionalUserInfo.isNewUser;
        const { first_name, last_name } = response.additionalUserInfo.profile;
        const { uid, email, phoneNumber, photoURL } = response.user._user;
        if (isNewUser) {
          const timestamp = firestore.FieldValue.serverTimestamp();
          const userData = {
            id: uid,
            email: email,
            firstName: first_name,
            lastName: last_name,
            phone: phoneNumber,
            profilePictureURL: photoURL,
            userID: uid,
            appIdentifier,
            created_at: timestamp,
            createdAt: timestamp,
          };
          usersRef
            .doc(uid)
            .set(userData)
            .then(() => {
              resolve({ user: userData, accountCreated: true });
            });
        }
        usersRef
          .doc(uid)
          .get()
          .then((document) => {
            resolve({ user: document.data(), accountCreated: false });
          });
      })
      .catch((_error) => {
        resolve({ error: ErrorCode.serverError });
      });
  });
};

export const register = (userDetails, appIdentifier) => {
  const {
    email,
    firstName,
    lastName,
    password,
    phone,
    profilePictureURL,
    location,
    SignUpLocation,
  } = userDetails;
  return new Promise(function (resolve, _reject) {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        const timestamp = firestore.FieldValue.serverTimestamp();
        const uid = response.user.uid;
        const data = {
          id: uid,
          userID: uid, // legacy reasons
          email,
          firstName,
          lastName,
          phone,
          profilePictureURL,
          location,
          SignUpLocation,
          appIdentifier,
          createdAt: timestamp,
          created_at: timestamp,
          unit: "meter",
          accountType: "email",
          currency: "dollar",
        };
        usersRef
          .doc(uid)
          .set(data)
          .then(() => {
            resolve({ user: data });
          })
          .catch((error) => {
            alert(error);
            resolve({ error: ErrorCode.serverError });
          });
      })
      .catch((error) => {
        var errorCode = ErrorCode.serverError;
        if (error.code === "auth/email-already-in-use") {
          errorCode = ErrorCode.emailInUse;
        }
        resolve({ error: errorCode });
      });
  });
};

export const loginWithEmailAndPassword = async (email, password) => {
  return new Promise(function (resolve, reject) {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then((response) => {
        const uid = response.user._user.uid;
        const userData = {
          email,
          password,
          id: uid,
        };
        console.log("FIREBASE AUTH  ", JSON.stringify(userData, null, 2));
        usersRef
          .doc(uid)
          .get()
          .then(function (firestoreDocument) {
            const user = firestoreDocument.data();
            const newUserData = {
              ...userData,
              ...user,
            };

            resolve({ user: newUserData });
          })
          .catch(function (_error) {
            resolve({ error: ErrorCode.serverError });
          });
      })
      .catch((error) => {
        var errorCode = ErrorCode.serverError;
        switch (error.code) {
          case "auth/wrong-password":
            errorCode = ErrorCode.invalidPassword;
            break;
          case "auth/network-request-failed":
            errorCode = ErrorCode.serverError;
            break;
          case "auth/user-not-found":
            errorCode = ErrorCode.noUser;
            break;
          default:
            errorCode = ErrorCode.serverError;
        }
        resolve({ error: errorCode });
      });
  });
};

export const loginWithFacebook = (accessToken, appIdentifier) => {
  const credential = auth.FacebookAuthProvider.credential(accessToken);
  return new Promise((resolve, _reject) => {
    signInWithCredential(credential, appIdentifier).then((response) => {
      resolve(response);
    });
  });
};

export const logout = () => {
  auth().signOut();
};

export const retrieveUserByPhone = (phone) => {
  return new Promise((resolve) => {
    usersRef.where("phone", "==", phone).onSnapshot((querySnapshot) => {
      console.log("querySnapshot----->1111 ", querySnapshot);
      if (querySnapshot.docs.length <= 0) {
        resolve({ error: true });
      } else {
        resolve({ success: true });
      }
    });
  });
};

export const sendSMSToPhoneNumber = (phoneNumber) => {
  return new Promise(function (resolve, _reject) {
    auth()
      .signInWithPhoneNumber(phoneNumber, true)
      .then(function (confirmationResult) {
        // console.log('confirmationResult-----> ', confirmationResult._auth._user)
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        resolve({ confirmationResult });
      })
      .catch(function (_error) {
        console.log("confirmationResult----->ERROR ", _error);
        resolve({ error: ErrorCode.smsNotSent });
      });
  });
};

export const loginWithSMSCode = (smsCode, verificationID) => {
  const credential = auth.PhoneAuthProvider.credential(verificationID, smsCode);
  let user = auth().currentUser;
  return new Promise(function (resolve, _reject) {
    if (user) {
      usersRef
        .doc(user._user.uid)
        .get()
        .then(function (firestoreDocument) {
          if (!firestoreDocument.exists) {
            resolve({ errorCode: ErrorCode.noUser });
            return;
          }
          const userData = firestoreDocument.data();
          resolve({ user: userData });
        });
    } else {
      auth()
        .signInWithCredential(credential)
        .then((result) => {
          const { user } = result;
          usersRef
            .doc(user.uid)
            .get()
            .then(function (firestoreDocument) {
              if (!firestoreDocument.exists) {
                resolve({ errorCode: ErrorCode.noUser });
                return;
              }
              const userData = firestoreDocument.data();
              resolve({ user: userData });
            })
            .catch(function (_error) {
              resolve({ error: ErrorCode.serverError });
            });
        })
        .catch((_error) => {
          resolve({ error: ErrorCode.invalidSMSCode });
        });
    }
  });
};

export const registerWithPhoneNumber = (
  userDetails,
  smsCode,
  verificationID,
  appIdentifier
) => {
  const {
    firstName,
    lastName,
    phone,
    profilePictureURL,
    location,
    SignUpLocation,
  } = userDetails;

  const credential = auth.PhoneAuthProvider.credential(verificationID, smsCode);
  let user = auth().currentUser;
  return new Promise(function (resolve, _reject) {
    if (user) {
      const timestamp = firestore.FieldValue.serverTimestamp();
      const data = {
        id: user._user.uid,
        userID: user._user.uid,
        firstName,
        lastName,
        phone,
        profilePictureURL,
        location,
        SignUpLocation,
        appIdentifier,
        created_at: timestamp,
        createdAt: timestamp,
        unit: "meter",
        accountType: "phone",
        currency: "dollar",
      };
      usersRef
        .doc(user._user.uid)
        .set(data)
        .then(() => {
          resolve({ user: data });
        });
    } else {
      auth()
        .signInWithCredential(credential)
        .then((response) => {
          const timestamp = firestore.FieldValue.serverTimestamp();
          const uid = response.user.uid;
          const data = {
            id: uid,
            userID: uid, // legacy reasons
            firstName,
            lastName,
            phone,
            profilePictureURL,
            location,
            SignUpLocation,
            appIdentifier,
            created_at: timestamp,
            createdAt: timestamp,
            unit: "feet",
            currency: "dollar",
            accountType: "phone",
          };
          usersRef
            .doc(uid)
            .set(data)
            .then(() => {
              resolve({ user: data });
            });
        })
        .catch((error) => {
          var errorCode = ErrorCode.serverError;
          if (error.code === "auth/email-already-in-use") {
            errorCode = ErrorCode.emailInUse;
          }
          resolve({ error: errorCode });
        });
    }
  });
};

export const updateProfilePhoto = (userID, profilePictureURL) => {
  return new Promise((resolve, _reject) => {
    usersRef
      .doc(userID)
      .update({ profilePictureURL: profilePictureURL })
      .then(() => {
        resolve({ success: true });
      })
      .catch((error) => {
        resolve({ error: error });
      });
  });
};

export const fetchPushTokenIfPossible = async () => {
  const messaging = messaging();
  const hasPushPermissions = await messaging.hasPermission();
  if (hasPushPermissions) {
    return await messaging.getToken();
  }
  await messaging.requestPermission();
  return await messaging.getToken();
};

export const updateUser = async (userID, newData) => {
  // const dataWithOnlineStatus = {
  //   ...newData,
  //   lastOnlineTimestamp: firestore.FieldValue.serverTimestamp(),
  // };
  // await usersRef.doc(userID).update(dataWithOnlineStatus);
};
