const type = {
  base: "Avenir-Book",
  bold: "Avenir-Black",
  emphasis: "HelveticaNeue-Italic",
};

const family = {
  main: "Avenir-Book", // iCielVAGRoundedNext-Regular
  bold: "bold", // iCielVAGRoundedNext-Bold
  emphasis: "HelveticaNeue-Italic",
  medium: "medium", // iCielVAGRoundedNext-Medium
  light: "light", // iCielVAGRoundedNext-Light
  demiBold: "demiBold", // iCielVAGRoundedNext-DemiBold
  lightItalic: "lightItalic", // iCielVAGRoundedNext-LightItalic
};

const size = {
  xxxlarge: 26,
  xxlarge: 22,
  xlarge: 20,
  large: 18,
  normal: 16,
  medium: 14,
  regular: 14,
  small: 12,
  tiny: 8.5,
};

const style = {
  header: size.xlarge,
  title: size.xlarge,
  input: size.normal,
  placeholder: size.normal,
  label: size.normal,
  button: size.normal,
  bigButton: size.large,
  smallButton: size.small,
  subTitle: 12,
  description: size.normal,
  key: size.regular,
  value: size.small,
};
const Fonts = { family, size, type, style };
export default Fonts;
