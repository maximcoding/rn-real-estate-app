import { Dimensions, Platform, StyleSheet } from "react-native";
import Colors from "./colors";

const Styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  toolbar: {
    flexDirection: "row",
    width: "100%",
    height: Platform.OS === "android" ? 48 : isIphoneX() ? 88 : 78,
    paddingTop: Platform.OS === "android" ? 0 : isIphoneX() ? 40 : 30,
    alignItems: "center",
  },
});
export default Styles;

export function isIphoneX() {
  const dim = Dimensions.get("window");

  return (
    // This has to be iOS
    Platform.OS === "ios" &&
    // Check either, iPhone X or XR
    (isIPhoneXSize(dim) || isIPhoneXrSize(dim))
  );
}

export function isIPhoneXSize(dim) {
  return dim.height === 812 || dim.width === 812;
}

export function isIPhoneXrSize(dim) {
  return dim.height === 896 || dim.width === 896;
}
