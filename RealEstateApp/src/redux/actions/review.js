import {
  postReviewApi,
  getReviewApi,
  getPostedReviewApi,
  replyReviewApi,
  updateListingApi,
} from "../../api";

export const USER_MESSAGES = "USER_MESSAGES";

export const subscribeReviews = (listingId, callback) => async (dispatch) => {
  try {
    const res = await getReviewApi(listingId);

    console.log("res.data in subscribe review", res.data);

    callback(res.data);

    // reviewsRef
    //   .where("listingID", "==", listingId)
    //   .onSnapshot((querySnapshot) => {
    //     let data = [];
    //     querySnapshot.forEach((doc) => {
    //       const Review = doc.data();
    //       data.push({
    //         ...Review,
    //         id: doc.id,
    //       });
    //     });

    //     ;
    //   });
  } catch (error) {
    callback();
  }
};

export const postReview = (user, data, starCount, content, callback) => async (
  dispatch
) => {
  try {
    let reviewObj = {
      authorID: user._id,
      listingID: data._id,
      starCount,
      content: content,
      firstName: user.firstName,
      lastName: user.lastName,
      postUser: data.authorID,
      image: user.profilePictureURL,
      replies: [],
    };

    const res = await postReviewApi(reviewObj);

    // await reviewsRef.add();

    // let reviewQuerySnapshot = await reviewsRef
    //   .where("listingID", "==", data.id)
    //   .get();

    let totalStarCount = 0;
    let count = 0;
    res.data.forEach((review) => {
      totalStarCount += review.starCount;
      count++;
    });

    if (count > 0) {
      data.starCount = totalStarCount / count;
    } else {
      data.starCount = 0;
    }

    await updateListingApi(data._id, data);

    // await listingsRef.doc(data.id).set(data);
    callback();
  } catch (error) {
    console.log("");
    callback();
  }
};

export const getPostReviews = (user, cb) => async (dispatch) => {
  try {
    const res = await getPostedReviewApi(user._id);
    dispatch({
      type: USER_MESSAGES,
      payload: res.data,
    });

    cb();
    // reviewsRef.where("postUser", "==", user.id).onSnapshot((querySnapshot) => {
    //   let data = [];
    //   querySnapshot.forEach((doc) => {
    //     const Review = doc.data();
    //     data.push({
    //       ...Review,
    //       id: doc.id,
    //     });
    //   });

    // });
  } catch (error) {
    cb();
  }
};

export const replyToReview = (review, content, cb) => async (dispatch) => {
  try {
    let updateReview = review;
    updateReview.replies.push({
      text: content,
      createdAt: new Date(),
    });

    // await reviewsRef.doc(review.id).update({ ...updateReview });

    await replyReviewApi(review._id, { ...updateReview });

    cb();
  } catch (error) {
    cb();
    alert(error);
  }
};
