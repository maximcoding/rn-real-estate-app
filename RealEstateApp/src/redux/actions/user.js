import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
const usersRef = firestore().collection("users");

import axios from "axios";
import {
  signUpWithEmailApi,
  loginWithEmailApi,
  getUserApi,
  logoutApi,
  sendVerificationCodeApi,
  loginWithPhoneApi,
  updateUserApi,
  addComplainApi,
} from "../../api";
import {
  dispatchSetUserData,
  dispatchLogOut,
  updateUserData,
} from "../reducers";
import { Alert } from "react-native";
import { INSTANCE } from "../../api/instance";

export const SAVE_CURRENCY = "SAVE_CURRENCY";
export const CHANGE_GRID_VIEW = "CHANGE_GRID_VIEW";
export const CHANGE_SORT_VIEW = "CHANGE_SORT_VIEW";
export const SAVE_CODE = "SAVE_CODE";

export const updateUser = (user, cb) => async (dispatch) => {
  try {
    const res = await updateUserApi(user, user._id);
    dispatch(updateUserData(user));
    cb(true);
  } catch (error) {
    cb();
    Alert.alert(
      "",
      error.response ? error.response.data : error,
      [{ text: "OK" }],
      {
        cancelable: false,
      }
    );
  }
};

export const changeEmail = (currentPassword, newEmail, myuser, cb) => async (
  dispatch
) => {
  try {
    var user = auth().currentUser;
    var cred = auth.EmailAuthProvider.credential(user.email, currentPassword);
    await user.reauthenticateWithCredential(cred);
    await user.updateEmail(newEmail);
    await usersRef.doc(myuser.id).update({ ...myuser, email: newEmail });

    cb(true);
  } catch (error) {
    cb();
    alert(error);
  }
};

export const changePhoneNumber = (newPhone, cb) => async () => {
  try {
    // const confirmation = await auth().signInWithPhoneNumber(newPhone);

    const snapshot = await auth()
      .verifyPhoneNumber(newPhone)
      .on("state_changed", (phoneAuthSnapshot) => {
        console.log("Snapshot state: ", phoneAuthSnapshot.state);
      });
    cb(snapshot);
  } catch (error) {
    cb();
    alert(error);
  }
};

export const verifyPhoneNumber = (code, updatedUser, cb) => async (
  dispatch,
  getState
) => {
  try {
    let oldCode = getState().auth.otp;
    console.log("otp saved", oldCode, ":new:", code);
    if (+code === +oldCode) {
      const res = await updateUserApi(updatedUser, updatedUser._id);
      console.log("user in action", updatedUser);
      dispatch(updateUserData(updatedUser));
    } else {
      Alert.alert(
        "Incorrect OTP",
        "The OTP you typed is not correct",
        [{ text: "OK" }],
        {
          cancelable: false,
        }
      );
    }

    cb(true);
  } catch (error) {
    console.log("error while update phone", error);

    Alert.alert(
      "",
      error.response ? error.response.data : error,
      [{ text: "OK" }],
      {
        cancelable: false,
      }
    );
    cb();
  }
};

export const complainUs = (complain, user, cb) => async (dispatch) => {
  try {
    let data = {
      from: `${user.firstName} ${user.lastName}`,
      userId: user._id,
      text: complain,
    };

    const res = await addComplainApi(data);
    console.log("res from complain", res);

    cb(true);
  } catch (error) {
    cb();
    alert(error);
  }
};

export const getCurrencyPrices = () => async (dispatch) => {
  try {
    let res = await axios.get(
      "https://api.exchangeratesapi.io/latest?base=USD"
    );
    dispatch({
      type: SAVE_CURRENCY,
      payload: res.data.rates,
    });
  } catch (error) {}
};

export const changeView = (value) => {
  return {
    type: CHANGE_GRID_VIEW,
    payload: value,
  };
};

export const changeSort = (value) => {
  return {
    type: CHANGE_SORT_VIEW,
    payload: value,
  };
};

// signup with email and password

export const signUpWithEmail = (data, cb) => async (dispatch) => {
  try {
    console.log("data in action", data);
    const user = await signUpWithEmailApi(data);

    if (user) {
      dispatch(
        dispatchSetUserData({
          user: user.data,
          authorization: user.headers.authorization,
        })
      );

      INSTANCE.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${user.headers.authorization}`;
    } else {
      throw Error("something went wrong");
    }
  } catch (error) {
    console.log("err while signup", error.response);

    Alert.alert(
      "",
      error.response ? error.response.data : error,
      [{ text: "OK" }],
      {
        cancelable: false,
      }
    );
  } finally {
    cb();
  }
};
// signin with email and password

export const signinWithEmail = (data, cb) => async (dispatch) => {
  try {
    console.log("data in action", data);
    const user = await loginWithEmailApi(data);

    if (user) {
      console.log("res in api", user.headers.authorization);
      dispatch(
        dispatchSetUserData({
          user: user.data,
          authorization: user.headers.authorization,
        })
      );
      INSTANCE.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${user.headers.authorization}`;
    } else {
      throw Error("something went wrong");
    }
  } catch (error) {
    console.log("err while signin", error);

    Alert.alert(
      "",
      error.response ? error.response.data : error,
      [{ text: "OK" }],
      {
        cancelable: false,
      }
    );
  } finally {
    cb();
  }
};

// Get user action
export const getUser = () => {
  return async (dispatch, getState) => {
    let token = getState().auth.authorization;
    console.log("token", getState().auth);
    try {
      INSTANCE.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      const res = await getUserApi(token);

      console.log("res while user ", res);
      dispatch(dispatchSetUserData({ user: res.data, authorization: token }));
    } catch (error) {
      console.log("userNotLogIn", error);
      dispatch(dispatchSetUserData({ user: null, authorization: token }));
    }
  };
};

// Logout user action
export const logoutUser = () => {
  return async (dispatch) => {
    try {
      let res = await logoutApi();
      console.log("res in logout", res.data);

      if (res.data === "success") {
        dispatch(dispatchLogOut());
      } else {
        throw Error("something went wrong");
      }
    } catch (error) {
      console.log("error logout", error);
      Alert.alert("", "something went wrong", [{ text: "OK" }], {
        cancelable: false,
      });
    }
  };
};

const saveCode = (code) => ({
  type: SAVE_CODE,
  payload: code,
});

export const sendOtp = (phone, cb) => async (dispatch) => {
  try {
    let randomNumber = Math.random() * 1000000;
    let code = +randomNumber.toFixed();
    console.log("code =========>", code);
    dispatch(saveCode(code));
    let res = await sendVerificationCodeApi({ phone, code });
    console.log("res while send otp", res);
    cb("success");
  } catch (error) {
    console.log("error while send", error);
    cb("fail");
    Alert.alert(
      "",
      error.response ? error.response.data : "something went wrong",
      [{ text: "OK" }],
      {
        cancelable: false,
      }
    );
  }
};

export const signupWithPhone = (data, newCode, cb) => async (
  dispatch,
  getState
) => {
  try {
    let code = getState().auth.otp;
    console.log("otp saved", code, ":new:", newCode);
    if (+code === +newCode) {
      const user = await signUpWithEmailApi(data);

      if (user) {
        dispatch(
          dispatchSetUserData({
            user: user.data,
            authorization: user.headers.authorization,
          })
        );

        INSTANCE.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${user.headers.authorization}`;
      } else {
        throw Error("something went wrong");
      }
    } else {
      Alert.alert(
        "Incorrect OTP",
        "The OTP you typed is not correct",
        [{ text: "OK" }],
        {
          cancelable: false,
        }
      );
    }
  } catch (error) {
    Alert.alert(
      "",
      error.response ? error.response.data : "something went wrong",
      [{ text: "OK" }],
      {
        cancelable: false,
      }
    );
  } finally {
    cb();
  }
};

// signin with phone

export const signinWithPhone = (data, newCode, cb) => async (
  dispatch,
  getState
) => {
  try {
    let code = getState().auth.otp;
    console.log("otp saved", code, ":new:", newCode);
    if (+code === +newCode) {
      const user = await loginWithPhoneApi(data);

      if (user) {
        console.log("res in api", user.headers.authorization);
        dispatch(
          dispatchSetUserData({
            user: user.data,
            authorization: user.headers.authorization,
          })
        );
        INSTANCE.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${user.headers.authorization}`;
      } else {
        throw Error("something went wrong");
      }
    } else {
      Alert.alert(
        "Incorrect OTP",
        "The OTP you typed is not correct",
        [{ text: "OK" }],
        {
          cancelable: false,
        }
      );
    }
  } catch (error) {
    console.log("err while signin", error);

    Alert.alert(
      "",
      error.response ? error.response.data : error,
      [{ text: "OK" }],
      {
        cancelable: false,
      }
    );
  } finally {
    cb();
  }
};
