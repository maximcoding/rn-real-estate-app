import firestore from "@react-native-firebase/firestore";
import ServerConfig from "../../ServerConfig";
import { filterListing } from "../../api";
import _ from "lodash";

export const FILTER_RESULT = "FILTER_RESULT";
export const RESET_FILTER = "RESET_FILTER";
export const ONCHANGE_FILTER = "ONCHANGE_FILTER";
export const CLEAR_FILTER = "CLEAR_FILTER";

export const filterPosts = (data, cb) => async (dispatch) => {
  try {
    const res = await filterListing(data);
    console.log("filter result", res.data);

    if (res.data.length > 0) {
      dispatch({
        type: FILTER_RESULT,
        payload: res.data,
      });
      cb(true);
    } else {
      cb();
      alert("No result Found!");
    }
  } catch (error) {
    console.log("error is here", error);
    cb();
  }
};

export const onChangeFilter = (payload) => ({
  type: ONCHANGE_FILTER,
  payload,
});

export const onClearFilter = () => ({
  type: CLEAR_FILTER,
});

export const resetFilter = () => {
  return {
    type: RESET_FILTER,
  };
};

const verifyFilter = (result, data) => {
  let filterResult = result.filter((obj) => {
    if (
      obj.type == data.rent &&
      obj.categoryID == data.category &&
      obj.newConstruction == data.newConstruction &&
      obj.built == data.built &&
      obj.bedroom >= data.bedrooms &&
      obj.bathroom >= data.bathrooms &&
      obj.price >= data.price[0] &&
      obj.square >= data.square[0] &&
      obj.deposit >= data.deposit[0]
    ) {
      return obj;
    }
  });

  return filterResult;
};
