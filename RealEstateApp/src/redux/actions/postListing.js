import firestore from "@react-native-firebase/firestore";
import ServerConfig from "../../ServerConfig";
import storage from "@react-native-firebase/storage";
import { Platform, Alert } from "react-native";
import RNFS from "react-native-fs";
import geohash from "ngeohash";
import {
  addStateApi,
  filterCategoriesApi,
  getListingApi,
  getCategories,
  getListingByID,
  saveRecentListingApi,
  getRecentListingApi,
  updateListingApi,
  getListingRSApi,
  deleteListingApi,
  changeStatusApi,
} from "../../api";
import { FILTER_RESULT } from "./filters";
import { updateCategories } from "./category";
import { cloudinaryImageUrl } from "../../api/cloudinaryInstance";

export const POST_LISTING = "POST_LISTING";
export const SAVED_POSTS = "SAVED_POSTS";
export const RENTING_POSTS = "RENTING_POSTS";
export const BUYING_POSTS = "BUYING_POSTS";
export const FAVOURITE_POSTS = "FAVOURITE_POSTS";
export const RECENT_POSTS = "RECENT_POSTS";
export const RECENT_POSTS_LIST = "RECENT_POSTS_LIST";
export const SAVE_RECENT_LISTING = "SAVE_RECENT_LISTING";
export const DELETE_LISTING = "DELETE_LISTING";
export const ADD_TO_SAVED = "ADD_TO_SAVED";
export const CHANGE_STATUS = "CHANGE_STATUS";

const savedListingsRef = firestore().collection(
  ServerConfig.database.collection.SAVED_LISTINGS
);

const listingsRef = firestore().collection(
  ServerConfig.database.collection.LISTINGS
);

const recentListingRef = firestore().collection(
  ServerConfig.database.collection.RECENT_LISTING
);

const ListingCategoriesRef = firestore()
  .collection(ServerConfig.database.collection.CATEGORIES)
  .orderBy("order");

const getGeohashRange = (latitude, longitude, distance) => {
  const lat = 0.0144927536231884;
  const lon = 0.0181818181818182;

  const lowerLat = latitude - lat * distance;
  const lowerLon = longitude - lon * distance;

  const upperLat = latitude + lat * distance;
  const upperLon = longitude + lon * distance;

  const lower = geohash.encode(lowerLat, lowerLon);
  const upper = geohash.encode(greaterLat, greaterLon);

  return {
    lower,
    upper,
  };
};

export const getListing = (callback, sort, location = {}) => async (
  dispatch
) => {
  try {
    let range;
    if (sort == "near") {
      range = getGeohashRange(location.latitude, location.longitude, 10);
    }
    const res = await getListingApi({ sort, range });

    callback();
    dispatch({
      type: POST_LISTING,
      payload: res.data,
    });
  } catch (error) {
    console.log("error while get ", error);
    callback();
  }
};

export const getDocById = (id, cb) => async (dispatch) => {
  try {
    console.log("id in get by id", id);

    let result = await getListingByID(id);
    console.log("reslt in get ", result.data);

    cb(result.data);
  } catch (error) {
    alert(error);
  }
};

export const subscribeListingCategories = (callback) => async (dispatch) => {
  try {
    let res = await getCategories();

    callback();
    dispatch(updateCategories(res.data));
  } catch (error) {}
};

export const filterCategories = (
  categoryId,
  isApproved = true,
  callback
) => async (dispatch) => {
  try {
    if (categoryId) {
      let res = await filterCategoriesApi({ categoryId, isApproved });

      dispatch({
        type: FILTER_RESULT,
        payload: res.data,
      });
      callback();
    }
  } catch (error) {
    console.log("error while filter categories", error);
  }
};

export const subscribeSavedListings = (userId, callback) => (dispatch) => {
  try {
    if (userId) {
      // savedListingsRef
      //   .where("userID", "==", userId)
      //   .onSnapshot((querySnapshot) => {
      //     const savedListingdata = [];
      //     querySnapshot.forEach((doc) => {
      //       const savedListing = doc.data();
      //       savedListingdata.push(savedListing.listingID);
      //     });
      //     callback();
      //     dispatch({
      //       type: SAVED_POSTS,
      //       payload: savedListingdata,
      //     });
      //   });
    }
  } catch (error) {
    callback();
  }
};

export const subscribeRecentListings = (userId, callback) => async (
  dispatch
) => {
  try {
    if (userId) {
      const res = await getRecentListingApi(userId);

      let recentListingdata = [];

      res.data.forEach((doc) => {
        recentListingdata.push(doc.listingID);
      });

      console.log("recentListingdata", recentListingdata);

      dispatch({
        type: RECENT_POSTS,
        payload: recentListingdata,
      });
      callback();
    }
  } catch (error) {
    console.log("error is ehre", error);
    callback();
  }
};

export const saveRecentListing = (item, userId, recentSaved) => async (
  dispatch
) => {
  try {
    if (recentSaved.findIndex((k) => k == item._id) !== -1) {
      return;
    } else {
      // await recentListingRef.add({
      //   userID: userId,
      //   listingID: item._id,
      // });

      const res = await saveRecentListingApi({
        userID: userId,
        listingID: item._id,
      });

      dispatch({
        type: SAVE_RECENT_LISTING,
        payload: res.data,
      });
    }
  } catch (error) {
    console.log("error is here", error);
  }
};

export const saveUnsaveListing = (item, userId, savedPosts) => async (
  dispatch
) => {
  try {
    if (savedPosts.findIndex((k) => k == item._id) >= 0) {
      // let querySnapshot = await savedListingsRef
      //   .where("listingID", "==", item.id)
      //   .where("userID", "==", userId)
      //   .get();

      // querySnapshot.forEach((doc) => {
      //   doc.ref.delete();
      // });
      dispatch({
        type: DELETE_LISTING,
        payload: item._id,
      });
    } else {
      // await savedListingsRef.add({
      //   userID: userId,
      //   listingID: item.id,
      // });
      dispatch({
        type: ADD_TO_SAVED,
        payload: item._id,
      });
    }
  } catch (error) {
    alert(error);
  }
};

export const removeListing = (listing, callback) => async (dispatch) => {
  try {
    // let one = listingsRef.doc(listing.id).update({
    //   ...listing,
    //   delete: true,
    // });

    await deleteListingApi(listing._id);

    // let two = savedListingsRef.where("listingID", "==", listing.id).get();
    // let result = await Promise.all([one, two]);
    // result[1].forEach(function (doc) {
    //   doc.ref.delete();
    // });

    dispatch({
      type: DELETE_LISTING,
      payload: listing._id,
    });

    callback(true);
  } catch (error) {
    callback(false);
  }
};

// export const approveListing = (listingId, callback) => {
//   listingsRef
//     .doc(listingId)
//     .update({ isApproved: true })
//     .then(() => {
//       callback({ success: true });
//     })
//     .catch((error) => {
//       callback({ success: false });
//     });
// };

export const postListing = async (selectedItem, uploadObject, callback) => {
  try {
    const updatedUploadObjects = {
      ...uploadObject,
      coordinate: [uploadObject.latitude, uploadObject.longitude],
    };
    if (selectedItem) {
      await updateListingApi(selectedItem._id, { ...updatedUploadObjects });

      callback(true);

      // await listingsRef
      //   .doc(selectedItem.id)
      //   .update({ ...updatedUploadObjects });
    } else {
      console.log("updatedUploadObjects ==== >", updatedUploadObjects);

      const res = await addStateApi(updatedUploadObjects);
      console.log("res while add state", res.data);
      callback(true);

      // let docRef = await listingsRef.add(updatedUploadObjects);
      // if (docRef.id) {
      //   await listingsRef.doc(docRef.id).update({ id: docRef.id });
      // }
    }
  } catch (error) {
    callback(false);
  }
};

export const uploadImage = async (source) => {
  await cloudinaryImageUrl(source);
  // return new Promise(async (resolve, reject) => {
  //   const filename = uri.substring(uri.lastIndexOf("/") + 1);
  //   const uploadUri = Platform.OS === "ios" ? uri.replace("file://", "") : uri;
  //   const data = await RNFS.readFile(uploadUri, "base64");
  //   try {
  //     await storage().ref(filename).putString(data, "base64");
  //     const url = await storage().ref(filename).getDownloadURL();
  //     resolve({ downloadURL: url });
  //   } catch (error) {
  //     reject(error);
  //   }
  // });
};

export const removeUpdateListing = (item, cb) => async (dispatch) => {
  try {
    // await listingsRef.doc(item.id).update(item);

    await updateListingApi(item._id, { item });

    cb();
  } catch (error) {
    cb();
    alert(error);
  }
};

export const getListingRS = (type, user, cb) => async (dispatch) => {
  try {
    const res = await getListingRSApi(type);

    if (type == "buy") {
      dispatch({
        type: BUYING_POSTS,
        payload: res.data,
      });
    } else {
      dispatch({
        type: RENTING_POSTS,
        payload: res.data,
      });
    }
    cb();
  } catch (error) {
    console.log("erro is here guyts", error);
    cb();
  }
};

function transform2d(srcArray, modulus) {
  var result = undefined;
  if (modulus > 0) {
    result = [];
    for (var i = 0; i < srcArray.length; i += modulus) {
      result.push(srcArray.slice(i, i + modulus));
    }
  }
  return result;
}

export const getFavouriteListing = (savedPosts, cb) => async (dispatch) => {
  try {
    let arrays = transform2d(savedPosts, 10);
    let promises = [];
    arrays.map((ar) => {
      promises.push(listingsRef.where("id", "in", ar).get());
    });

    let resultOne = await Promise.all(promises);
    let docsArr = [];
    resultOne.forEach((item) => {
      docsArr.push(item.docs);
    });
    let result = [];

    docsArr.flat(1).forEach(function (doc) {
      result.push({ ...doc.data(), id: doc.id });
    });

    dispatch({
      type: FAVOURITE_POSTS,
      payload: result,
    });
    cb();
  } catch (error) {
    cb();
    alert(error);
  }
};

export const getRecentListings = (recentSaved, cb) => async (dispatch) => {
  try {
    let arrays = transform2d(recentSaved, 10);
    let promises = [];
    arrays.map((ar) => {
      promises.push(listingsRef.where("id", "in", ar).get());
    });

    let resultOne = await Promise.all(promises);
    let docsArr = [];
    resultOne.forEach((item) => {
      docsArr.push(item.docs);
    });
    let result = [];

    docsArr.flat(1).forEach(function (doc) {
      result.push({ ...doc.data(), id: doc.id });
    });

    dispatch({
      type: RECENT_POSTS_LIST,
      payload: result,
    });
    cb();
  } catch (error) {
    cb();
    alert(error);
  }
};

export const updateStatus = (data, cb) => async (dispatch) => {
  try {
    let res = await changeStatusApi(data);
    console.log("res while update status", res);
    dispatch({
      type: CHANGE_STATUS,
      payload: data,
    });
    cb("success", data.status);
  } catch (er) {
    console.log("err while update status", er);
    cb("error");
  }
};
