import { UPATE_CATEGORIES } from "../reducers/index";

export const updateCategories = (data) => {
  return {
    type: UPATE_CATEGORIES,
    payload: data,
  };
};
