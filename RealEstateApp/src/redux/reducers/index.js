import { combineReducers } from "redux";
import { app } from "./app";
import { chat } from "./chat";
import { userReports } from "../../core/user-reporting/redux";
import {
  FILTER_RESULT,
  RESET_FILTER,
  ONCHANGE_FILTER,
  CLEAR_FILTER,
} from "../actions/filters";
import {
  SAVE_CURRENCY,
  CHANGE_GRID_VIEW,
  CHANGE_SORT_VIEW,
  SAVE_CODE,
} from "../actions/user";
import {
  POST_LISTING,
  SAVED_POSTS,
  DELETE_LISTING,
  ADD_TO_SAVED,
  RENTING_POSTS,
  BUYING_POSTS,
  FAVOURITE_POSTS,
  RECENT_POSTS,
  RECENT_POSTS_LIST,
  SAVE_RECENT_LISTING,
  CHANGE_STATUS,
} from "../actions/postListing";
import { USER_MESSAGES } from "../actions/review";
import { UPDATE_LANGUAGE } from "../actions/language";

export const LOG_IN_ACTION = "LOG_IN";
export const LOG_OUT_ACTION = "LOG_OUT";
export const RESTORE_TOKEN_ACTION = "RESTORE_TOKEN";
export const UPDATE_USER_ACTION = "UPDATE_USER";
export const SET_USERS_ACTION = "SET_USERS";
export const UPATE_CATEGORIES = "UPATE_CATEGORIES";
export const UPDATE_USER = "UPDATE_USER_DATA";

export const dispatchSetUsers = (data) => ({
  type: SET_USERS_ACTION,
  data,
});

export const dispatchSetUserData = (user) => ({
  type: UPDATE_USER_ACTION,
  data: user,
});
export const updateUserData = (user) => ({
  type: UPDATE_USER,
  data: user,
});

export const dispatchLogOut = () => ({
  type: LOG_OUT_ACTION,
});

export const INITIAL_STATE = {
  user: null,
  users: [],
  userToken: null,
  isLoading: false,
  updator: new Date(),
  currencyRates: "",
  grid: "images",
  sortBy: "new",
  authorization: "",
  otp: "",
};

export const auth = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_USER_ACTION: {
      console.log("UPDATE_USER_ACTION");
      return {
        ...state,
        user: action.data.user,
        authorization: action.data.authorization,
        updator: new Date(),
      };
    }
    case UPDATE_USER:
      let updatedUser = {
        ...state.user,
        ...action.data,
      };
      console.log("updatedUser", updatedUser);
      return {
        ...state,
        user: updatedUser,
        updator: new Date(),
      };
    case SAVE_CURRENCY:
      return {
        ...state,
        currencyRates: action.payload,
      };
    case SET_USERS_ACTION:
      return { ...state, users: [...action.data] };
    case LOG_OUT_ACTION: {
      return INITIAL_STATE;
    }
    case CHANGE_GRID_VIEW:
      return {
        ...state,
        grid: action.payload,
      };
    case CHANGE_SORT_VIEW:
      return {
        ...state,
        sortBy: action.payload,
      };
    case SAVE_CODE:
      return {
        ...state,
        otp: action.payload,
      };
    default:
      return state;
  }
};

export const categories = (state = { categories: [] }, action) => {
  switch (action.type) {
    case UPATE_CATEGORIES:
      return { ...state, categories: action.payload };

    default:
      return state;
  }
};

export const filterReducer = (
  state = {
    filters: [],
    showFilter: false,
    filterValues: {
      price: [0, 0],
      square: [0, 0],
      bathrooms: 0,
      bedrooms: 0,
      rent: "",
      newConstruction: "",
      built: "",
      loading: false,
      deposit: [0, 0],
      category: "",
      useExact: false,
      status: "",
      persistState: false,
    },
  },
  action
) => {
  switch (action.type) {
    case FILTER_RESULT:
      return { ...state, filters: action.payload, showFilter: true };
    case RESET_FILTER:
      return {
        ...state,
        filters: [],
        showFilter: false,
      };

    case ONCHANGE_FILTER:
      console.log("onChange in reducer", action.payload);
      return {
        ...state,
        filterValues: {
          ...state.filterValues,
          [action.payload.name]: action.payload.value,
        },
      };
    case CLEAR_FILTER:
      return {
        ...state,
        filterValues: {
          price: [0, 0],
          square: [0, 0],
          bathrooms: 0,
          bedrooms: 0,
          rent: "",
          newConstruction: "",
          built: "",
          deposit: [0, 0],
          category: "",
          useExact: false,
          status: "",
          persistState: false,
        },
      };
    default:
      return state;
  }
};

export const postReducer = (
  state = {
    posts: [],
    savedPosts: [],
    rentingPosts: [],
    buyingPosts: [],
    favouritePosts: [],
    recentPosts: [],
    recentSaved: [],
    userMessages: [],
  },
  action
) => {
  switch (action.type) {
    case POST_LISTING:
      return { ...state, posts: action.payload };
    case SAVED_POSTS:
      return {
        ...state,
        savedPosts: action.payload,
      };

    case DELETE_LISTING:
      let posts = state.savedPosts.filter((id) => id !== action.payload);
      return {
        ...state,
        savedPosts: posts,
      };

    case ADD_TO_SAVED:
      let existingPost = state.savedPosts;
      existingPost.push(action.payload);
      return {
        ...state,
        savedPosts: existingPost,
      };

    case RECENT_POSTS:
      return {
        ...state,
        recentSaved: action.payload,
      };
    case RENTING_POSTS:
      return {
        ...state,
        rentingPosts: action.payload,
      };
    case BUYING_POSTS:
      return {
        ...state,
        buyingPosts: action.payload,
      };
    case FAVOURITE_POSTS:
      return {
        ...state,
        favouritePosts: action.payload,
      };
    case RECENT_POSTS_LIST:
      return {
        ...state,
        recentPosts: action.payload,
      };

    case SAVE_RECENT_LISTING:
      let savedRecent = action.payload;
      savedRecent.push(action.payload);
      return {
        ...state,
        recentPosts: savedRecent,
      };
    case CHANGE_STATUS:
      let { payload } = action;
      if (payload.type === "type") {
        let targetPost = state.buyingPosts.filter(
          (item) => item._id === payload.id
        );
        targetPost.status = payload.status;
        let filterPost = state.buyingPosts.filter(
          (item) => item._id !== payload.id
        );

        filterPost.push(targetPost);
        console.log("buyingPosts in reducer", filterPost);
        return {
          ...state,
          buyingPosts: [...filterPost],
        };
      } else {
        let targetPost = state.rentingPosts.filter(
          (item) => item._id === payload.id
        );
        targetPost.status = payload.status;
        let filterPost = state.rentingPosts.filter(
          (item) => item._id !== payload.id
        );

        filterPost.push(targetPost);
        return {
          ...state,
          rentingPosts: [...filterPost],
        };
      }
    case USER_MESSAGES:
      return {
        ...state,
        userMessages: action.payload,
      };

    default:
      return state;
  }
};

export const languageReducer = (state = { updator: new Date() }, action) => {
  switch (action.type) {
    case UPDATE_LANGUAGE:
      return { ...state, updator: new Date() };
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  auth,
  app,
  chat,
  userReports,
  categories,
  filterReducer,
  postReducer,
  languageReducer,
});
