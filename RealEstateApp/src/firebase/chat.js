import firestore from '@react-native-firebase/firestore';
import ServerConfig from "../ServerConfig";

const usersRef = firestore().collection("users");

const channelPaticipationRef = firestore()
  .collection(ServerConfig.database.collection.CHANNEL_PARTICIPATION);

const channelsRef = firestore()
  .collection(ServerConfig.database.collection.CHANNELS)
  .orderBy("lastMessageDate", "desc");

export const subscribeChannels = callback => {
  return channelsRef.onSnapshot(querySnapshot =>
    callback(querySnapshot, channelPaticipationRef, usersRef)
  );
};

export const subscribeChannelPaticipation = (userId, callback) => {
  return channelPaticipationRef
    .where("user", "==", userId)
    .onSnapshot(querySnapshot => callback(querySnapshot));
};
