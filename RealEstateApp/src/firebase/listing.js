import firestore from "@react-native-firebase/firestore";
import ServerConfig from "../ServerConfig";

const savedListingsRef = firestore().collection(
  ServerConfig.database.collection.SAVED_LISTINGS
);
const listingsRef = firestore().collection(
  ServerConfig.database.collection.LISTINGS
);
const ListingCategoriesRef = firestore()
  .collection(ServerConfig.database.collection.CATEGORIES)
  .orderBy("order");
// console.log('listingsRef:-> ',subscribeListingCategories)
export const getListing = (callback) => {
  return listingsRef.onSnapshot((querySnapshot) => callback(querySnapshot));
};

export const subscribeListingCategories = (callback) => {
  return ListingCategoriesRef.onSnapshot((querySnapshot) =>
    callback(querySnapshot)
  );
};

export const subscribeListings = (
  {  categoryId, isApproved = true },
  callback
) => {

  try {
    if (categoryId) {
       listingsRef
        .where("categoryID", "==", categoryId)
        .where("isApproved", "==", isApproved)
        .onSnapshot(callback);
    }
    
  } catch (error) {
    
  }
 

 

  

  
};

export const subscribeSavedListings = (userId, callback, listingId) => {
  if (listingId) {
    return savedListingsRef
      .where("userID", "==", userId)
      .where("listingID", "==", listingId)
      .onSnapshot((querySnapshot) => callback(querySnapshot));
  }
  if (userId) {
    return savedListingsRef
      .where("userID", "==", userId)
      .onSnapshot((querySnapshot) => callback(querySnapshot));
  }

  return savedListingsRef.onSnapshot((querySnapshot) =>
    callback(querySnapshot)
  );
};

export const saveUnsaveListing = (item, userId) => {
  if (item.saved) {
    savedListingsRef
      .where("listingID", "==", item.id)
      .where("userID", "==", userId)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          doc.ref.delete();
        });
      });
  } else {
    savedListingsRef
      .add({
        userID: userId,
        listingID: item.id,
      })
      .then((docRef) => {})
      .catch((error) => {
        alert(error);
      });
  }
};

export const removeListing = async (listingId, callback) => {
  listingsRef
    .doc(listingId)
    .delete()
    .then(() => {
      savedListingsRef
        .where("listingID", "==", listingId)
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach(function (doc) {
            doc.ref.delete();
          });
        });
      callback({ success: true });
    })
    .catch((error) => {
      callback({ success: false });
      console.log("Error deleting listing: ", error);
    });
};

export const approveListing = (listingId, callback) => {
  listingsRef
    .doc(listingId)
    .update({ isApproved: true })
    .then(() => {
      callback({ success: true });
    })
    .catch((error) => {
      callback({ success: false });
      console.log("Error approving listing: ", error);
    });
};

export const postListing = (uploadObject, callback) => {
  const updatedUploadObjects = {
    ...uploadObject,
    createdAt: firestore.FieldValue.serverTimestamp(),
    coordinate: new firestore.GeoPoint(
      uploadObject.latitude,
      uploadObject.longitude
    ),
  };
   listingsRef
    .add(updatedUploadObjects)
    .then((docRef) => {
      callback(true);
    })
    .catch((error) => {
      console.log(error);
      callback(false);
    });
};
