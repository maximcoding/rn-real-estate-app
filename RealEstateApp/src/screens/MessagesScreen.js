import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getPostReviews } from "../redux/actions/review";
import moment from "moment";

export default function MessagesScreen({ navigation }) {
  const dispatch = useDispatch();
  const { userMessages } = useSelector((state) => state.postReducer);
  const { user } = useSelector((state) => state.auth);
  const [loader, setLoader] = useState(false);

  const stopLoader = () => {
    setLoader(false);
  };

  useEffect(() => {
    setLoader(true);
    dispatch(getPostReviews(user, stopLoader));
  }, []);

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={styles.row}
        key={index}
        onPress={() =>
          navigation.navigate("Detail", {
            item: { id: item.listingID },
            reviewId: item.id,
          })
        }
      >
        <View style={styles.subRow}>
          <Image source={{ uri: item.image }} style={styles.image} />
          <View style={{ alignItems: "flex-start", paddingLeft: 10 }}>
            <Text>
              {item.firstName}
              {item.lastName}
            </Text>
            <Text style={styles.content} numberOfLines={3}>
              {item.content}
            </Text>
          </View>
        </View>
        <Text style={styles.time}>
          {moment(item.createdAt).fromNow(true)} ago
        </Text>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.mainContainer}>
      {loader ? (
        <View style={styles.loaderC}>
          <ActivityIndicator size="small" color="green" />
        </View>
      ) : (
        <FlatList
          data={userMessages}
          renderItem={renderItem}
          contentContainerStyle={styles.container}
          ListEmptyComponent={() => {
            return (
              <View style={styles.empty}>
                <Text style={{ color: "gray" }}>no reviews yet</Text>
              </View>
            );
          }}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: "white",
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  mainContainer: {
    flex: 1,
  },
  loaderC: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomColor: "#d3d3d3",
    borderBottomWidth: 0.7,
  },
  image: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
  },
  subRow: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 10,
  },
  content: {
    fontSize: 12,
    color: "gray",
  },
  time: {
    fontSize: 12,
    color: "#d3d3d3",
    position: "absolute",
    bottom: 5,
    right: 10,
  },
});
