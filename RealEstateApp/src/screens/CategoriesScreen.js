import React from "react";
import {
  View,
  Text,
  ImageBackground,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { filterCategories } from "../redux/actions/postListing";

export default function CategoriesScreen({ navigation }) {
  const dispatch = useDispatch();
  let { categories } = useSelector((state) => state.categories);

  const onPressCategoryItem = (item) => {
    dispatch(
      filterCategories(item._id, true, () => {
        navigation.navigate("Home");
      })
    );
  };

  const renderListingItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={() => onPressCategoryItem(item)}
        style={{ paddingHorizontal: 10 }}
      >
        <ImageBackground source={{ uri: item.photo }} style={styles.image}>
          <Text style={styles.title}>{item.name}</Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView>
      <View style={styles.header}>
        <Text style={styles.headTitle}>Categories</Text>
      </View>
      <FlatList
        vertical
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ flexGrow: 1 }}
        data={categories}
        renderItem={renderListingItem}
        keyExtractor={(item, index) => index}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    height: 150,
  },
  title: {
    fontSize: 28,
    color: "white",
    fontWeight: "bold",
  },
  header: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    elevation: 2,
  },
  headTitle: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
