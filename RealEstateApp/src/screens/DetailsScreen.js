import React, { useState, useEffect, useRef } from "react";
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ActivityIndicator,
  Image,
} from "react-native";
import { AppStyles, ListStyle } from "../AppStyles";
import MapView, { Marker } from "react-native-maps";
import ReviewModal from "../components/ReviewModal";
import { Translate } from "../core/i18n/IMLocalization";
import { LineChart } from "react-native-chart-kit";
import Icon from "react-native-vector-icons/MaterialIcons";
import Fonts from "../themes/fonts";
import { useSelector, useDispatch } from "react-redux";
const { width, height } = Dimensions.get("window");
import {
  saveRecentListing,
  getDocById,
  updateStatus,
} from "../redux/actions/postListing";
import { subscribeReviews } from "../redux/actions/review";
import Review from "../components/review";
import { TouchableOpacity } from "react-native-gesture-handler";
import VerifyModal from "../components/verificationModal";
import { unitConverter } from "../utils/currencyConverter";
import Colors from "../themes/colors";

const chartData = {
  labels: ["January", "February", "March", "April", "May", "June"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
      color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`,
      strokeWidth: 2,
    },
  ],
};
const LATITUDEDELTA = 0.0422;
const LONGITUDEDELTA = 0.0223;

export default function DetailsScreen({ route, navigation }) {
  let item = route.params.item;
  let reviewId = route.params?.reviewId ?? false;
  let { user } = useSelector((state) => state.auth);
  let { updator } = useSelector((state) => state.languageReducer);
  let { recentSaved } = useSelector((state) => state.postReducer);
  const scrollRef = useRef();
  const dispatch = useDispatch();
  let [state, setState] = useState({
    data: "",
    reviewModalVisible: false,
  });
  const [reviews, setReviews] = useState([]);
  const [revLoader, setRevLoader] = useState(false);
  const [loader, setLoader] = useState(false);
  const [index, setIndex] = useState(false);
  const [showWarning, setShowWarning] = useState(user.phone == null);
  const [statusLoader, setStatusLoader] = useState(false);
  let { data } = state;

  const reviewLoader = (reviews) => {
    setRevLoader(false);
    if (reviews) {
      setReviews(reviews);
    }
    let index = reviews.findIndex((item) => item.id == reviewId);
    setIndex(index);
  };

  const stopLoader = (result) => {
    setState({ ...state, data: result });
    setLoader(false);
    if (reviewId) {
      scrollView();
    }
  };

  const scrollView = () => {
    let index = reviews.findIndex((item) => item.id == reviewId);
    setTimeout(() => {
      scrollRef.current.scrollTo({
        y: height + index * 88,
        animated: true,
      });
    }, 200);
  };

  useEffect(() => {
    console.log("detail screen useef", item);
    setLoader(true);

    dispatch(getDocById(item._id, stopLoader));
    dispatch(saveRecentListing(item, user._id, recentSaved));
    dispatch(subscribeReviews(item._id, reviewLoader));
    const unsubscribe = navigation.addListener("focus", () => {
      if (user.phone == null) {
        setShowWarning(true);
      }
    });
    return unsubscribe;
  }, [navigation]);

  const onReviewCancel = () => {
    setState({ ...state, reviewModalVisible: false });
  };

  const onReviewDone = () => {
    setState({ ...state, reviewModalVisible: false });

    // save review
  };

  const renderReviewItem = (item, i) => (
    <Review
      item={item}
      index={index}
      i={i}
      showReply={user._id == data.authorID}
    />
  );

  const changeStatusHandler = () => {
    setStatusLoader(true);
    let statusObj = {
      status: data.status === "open" ? "close" : "open",
      id: data._id,
      type: data.type,
    };
    console.log("data", statusObj);

    dispatch(updateStatus(statusObj, changeStatusFinally));
  };

  const changeStatusFinally = (type, status) => {
    if (type === "success") {
      setState({ ...state, data: { ...state.data, status } });
    }
    setStatusLoader(false);
  };

  console.log("data in detail screen", data);

  return (
    <>
      {loader ? (
        <SafeAreaView style={styles.mContainer}>
          <ActivityIndicator size="small" color="green" />
        </SafeAreaView>
      ) : (
        <ScrollView
          contentContainerStyle={styles.container}
          showsVerticalScrollIndicator={false}
          ref={scrollRef}
        >
          <Text style={AppStyles.text.sectionTitle}>
            {Translate("Location")}
          </Text>
          <MapView
            style={styles.mapView}
            initialRegion={{
              latitude: data.latitude,
              longitude: data.longitude,
              latitudeDelta: LATITUDEDELTA,
              longitudeDelta: LONGITUDEDELTA,
            }}
          >
            <Marker
              coordinate={{
                latitude: data.latitude,
                longitude: data.longitude,
              }}
            />
          </MapView>
          <Text style={AppStyles.text.sectionTitle}>
            {Translate("Address")}
          </Text>
          <Text numberOfLines={3} style={styles.longTextValue}>
            {data.place}
          </Text>
          <View style={styles.horizontalLine} />
          <Text style={AppStyles.text.sectionTitle}>
            {Translate("Details")}
          </Text>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>
              {Translate("Rent or Buy")}
            </Text>
            <Text style={AppStyles.text.listValue}>{data.type}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>
              {Translate("Bedrooms")}
            </Text>
            <Text style={AppStyles.text.listValue}>{data.bedroom}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>
              {Translate("Bathrooms")}
            </Text>
            <Text style={AppStyles.text.listValue}>{data.bathroom}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>{Translate("Floors")}</Text>
            <Text style={AppStyles.text.listValue}>{data.floors}</Text>
          </View>
          <View style={styles.details}>
            <Text style={AppStyles.text.listTitle}>{Translate("Square")}</Text>
            <Text style={AppStyles.text.listValue}>
              {unitConverter(Number(data.square), user)} {user.unit}
            </Text>
          </View>
          <View style={styles.horizontalLine} />
          <View style={styles.statusCont}>
            <Text style={AppStyles.text.listTitle}>
              Current Status:{" "}
              <Text
                style={[
                  AppStyles.text.listValue,
                  {
                    textTransform: "uppercase",
                    color: data.status === "open" ? Colors.green : Colors.fire,
                  },
                ]}
              >
                {data.status}
              </Text>
            </Text>
            {user._id === data?.authorID?._id && (
              <TouchableOpacity
                onPress={changeStatusHandler}
                style={[
                  styles.statusAction,
                  data.status === "open"
                    ? { backgroundColor: Colors.fire }
                    : { backgroundColor: Colors.green },
                ]}
              >
                {statusLoader ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  <Text style={{ color: "#fff" }}>
                    {data.status === "open" ? "close" : "open"}
                  </Text>
                )}
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.horizontalLine} />
          <Text style={[AppStyles.text.sectionTitle]}>
            {Translate("Additional Details")}
          </Text>
          <Text numberOfLines={3} style={styles.longTextValue}>
            {data.description}
          </Text>
          <View style={styles.horizontalLine} />

          {/*agent detials  */}
          <View>
            <Text style={[AppStyles.text.sectionTitle]}>Agent Details</Text>
            <View style={AppStyles.text.agentCont}>
              <Image
                style={AppStyles.text.avatar}
                source={{
                  uri: data?.authorID?.photoURI,
                }}
              />
              <View style={AppStyles.text.agentDetail}>
                <Text>
                  Name:{" "}
                  {`${data?.authorID?.firstName} ${data?.authorID?.lastName}`}
                </Text>
                <Text>Phone: {data?.authorID?.phone}</Text>
                <Text>Address: {data?.authorID?.address}</Text>
              </View>
            </View>
          </View>
          <View style={styles.horizontalLine} />

          <Text style={AppStyles.text.sectionTitle}>{"Views"}</Text>
          <LineChart
            data={chartData}
            width={width}
            height={220}
            chartConfig={{
              backgroundColor: "gray",
              backgroundGradientFrom: "#DDDD",
              backgroundGradientTo: "gray",
              color: (opacity = 1) => `rgba(${0}, ${0}, ${0}, ${opacity})`,
            }}
          />
          <Text style={[AppStyles.text.sectionTitle, { paddingTop: 10 }]}>
            {Translate("Reviews")}
          </Text>
          {user._id !== data?.authorID?._id && (
            <View style={[styles.details, { paddingBottom: 20 }]}>
              <TextInput
                placeholder={Translate("Write...")}
                style={styles.placeholder}
                onFocus={() => setState({ ...state, reviewModalVisible: true })}
              />
              <TouchableOpacity
                style={{ display: "flex" }}
                onPress={() => setState({ ...state, reviewModalVisible: true })}
              >
                <Icon name="rate-review" size={20} color="#00b33c" />
              </TouchableOpacity>
            </View>
          )}
          {reviews.length > 0 && (
            <>
              {reviews.map((item, index) => {
                return renderReviewItem(item, index);
              })}
            </>
          )}

          {state.reviewModalVisible && (
            <ReviewModal
              listing={data}
              onCancel={onReviewCancel}
              onDone={onReviewDone}
              dispatch={dispatch}
              user={user}
            />
          )}
        </ScrollView>
      )}
      <VerifyModal
        navigation={navigation}
        showWarning={showWarning}
        setShowWarning={setShowWarning}
      />
    </>
  );
}

const styles = StyleSheet.create({
  headerIconContainer: {
    marginRight: 10,
  },
  details: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "85%",
    alignSelf: "center",
    paddingVertical: 1,
  },
  horizontalLine: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    minHeight: 10,
    alignSelf: "center",
    width: width / 1.1,
  },
  statusCont: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 15,
    width: "85%",
    alignSelf: "center",
    alignItems: "center",
  },
  statusAction: {
    paddingVertical: 7,
    paddingHorizontal: 20,
    borderRadius: 20,
  },
  longTextValue: {
    ...AppStyles.text.listValue,
    paddingLeft: 10,
    flexDirection: "column",
    alignSelf: "center",
    justifyContent: "space-between",
    width: "90%",
  },
  headerIcon: {
    tintColor: "#21c064",
    height: 20,
    width: 20,
  },
  container: {
    backgroundColor: "white",
    flexGrow: 1,
  },
  mContainer: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  reviewTitle: {
    paddingTop: 0,
    paddingBottom: 10,
  },
  description: {
    fontFamily: Fonts.family.main,
    padding: 10,
    color: Colors.description,
  },
  photoItem: {
    backgroundColor: Colors.grey,
    height: 250,
    width: "100%",
  },
  paginationContainer: {
    flex: 1,
    position: "absolute",
    alignSelf: "center",
    paddingVertical: 8,
    marginTop: 220,
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
  },
  mapView: {
    width: "100%",
    height: 200,
    // backgroundColor: Colors.grey
  },
  loadingMap: {
    justifyContent: "center",
    alignItems: "center",
  },
  extra: {
    padding: 30,
    paddingTop: 10,
    paddingBottom: 0,
    marginBottom: 30,
  },
  extraRow: {
    flexDirection: "row",
    paddingBottom: 10,
  },
  extraKey: {
    flex: 2,
    color: Colors.title,
    fontWeight: "bold",
  },
  extraValue: {
    flex: 1,
    color: Colors.title,
  },

  reviewInput: {
    marginHorizontal: 10,
    padding: 5,
  },
});
