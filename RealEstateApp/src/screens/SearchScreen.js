import React, { useState, useEffect, memo } from "react";
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  Image,
} from "react-native";
import FastImage from "react-native-fast-image";
import { useDispatch, useSelector } from "react-redux";
import {
  AppIcon,
  AppStyles,
  HeaderButtonStyle,
  ListStyle,
  TwoColumnListStyle,
} from "../AppStyles";
import { Configuration } from "../Configuration";
import { Translate } from "../core/i18n/IMLocalization";
import Ionicons from "react-native-vector-icons/Ionicons";
import Fonts from "../themes/fonts";
import { filterCategories } from "../redux/actions/postListing";
import { Images } from "../themes";
import Colors from "../themes/colors";

export const SearchScreenOptions = (navigation, route) => ({
  title: Translate("Filter"),
  headerTitleStyle: AppStyles.text.headerTitleStyle,
  headerRight: () => (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate("Filters")}>
        <Image
          style={[AppIcon.style, { marginRight: 20 }]}
          source={Images.filter}
        />
      </TouchableOpacity>
    </View>
  ),
});

function SearchScreen({ navigation }) {
  let { posts } = useSelector((state) => state.postReducer);
  let { categories } = useSelector((state) => state.categories);
  let { updator } = useSelector((state) => state.languageReducer);
  const dispatch = useDispatch();
  let [state, setState] = useState({
    renting_data: [],
    selling_data: [],
  });

  useEffect(() => {
    getRentsSales(posts);
  }, []);

  const getRentsSales = (posts) => {
    let postsListing = posts.slice(0, 20);
    let one = postsListing.filter((p) => p.type == "rent");
    let two = postsListing.filter((p) => p.type == "buy");
    setState({
      ...state,
      renting_data: one,
      selling_data: two,
    });
  };

  const renderCategoryItem = ({ item }) => (
    <TouchableOpacity
      onPress={() => onPressCategoryItem(item)}
      style={styles.categoryItem}
    >
      <FastImage
        style={styles.categoryItemPhoto}
        source={{ uri: item.photo }}
      />
      <Text style={styles.categoryItemTitle}>{item.name}</Text>
    </TouchableOpacity>
  );

  const renderListing = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => onPressListingItem(item)}>
        <View style={TwoColumnListStyle.listingItemContainer}>
          <FastImage
            style={TwoColumnListStyle.listingPhoto}
            source={{ uri: item.photo }}
          />
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={[
              AppStyles.text.listTitle,
              { maxHeight: 40, paddingHorizontal: 5, paddingVertical: 1 },
            ]}
          >
            {item.title}
          </Text>
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            style={[
              AppStyles.text.description,
              { paddingHorizontal: 5, paddingVertical: 1 },
            ]}
          >
            {item.place}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const onPressListingItem = (item) => {
    navigation.navigate("Detail", {
      item: item,
      customLeft: true,
      routeName: "Home",
    });
  };

  const onPressCategoryItem = (item) => {
    console.log("item ", item);
    dispatch(
      filterCategories(item._id, true, () => {
        navigation.navigate("Home");
      })
    );
  };

  return (
    <View style={styles.container}>
      <View
        style={{
          height: 35,
          display: "flex",
          marginBottom: 30,
          backgroundColor: "#EEEE",
          borderRadius: 10,
          flexDirection: "row",
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Ionicons
            name="ios-search"
            style={{
              fontSize: Fonts.size.input,
              color: "gray",
              paddingLeft: 10,
              paddingRight: 10,
            }}
          />
          <TextInput placeholder="Free Text ..." />
        </View>
      </View>
      <ScrollView>
        <Text style={AppStyles.text.sectionTitle}>
          {Translate("Categories")}
        </Text>
        <View style={styles.categories}>
          <FlatList
            horizontal={true}
            initialNumToRender={5}
            data={categories}
            showsHorizontalScrollIndicator={false}
            renderItem={(item) => renderCategoryItem(item)}
            keyExtractor={(item) => `${item.id}`}
          />
        </View>
        {state.selling_data.length > 0 && (
          <>
            <Text style={[AppStyles.text.sectionTitle]}>
              {Translate("Selling")}
            </Text>
            <View>
              <FlatList
                horizontal
                data={state.selling_data}
                renderItem={renderListing}
                keyExtractor={(item) => `${item.id}`}
                showsHorizontalScrollIndicator={false}
              />
            </View>
          </>
        )}

        {state.renting_data.length > 0 && (
          <>
            <Text style={[AppStyles.text.sectionTitle]}>
              {Translate("Renting")}
            </Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={state.renting_data}
                renderItem={renderListing}
                keyExtractor={(item) => `${item.id}`}
              />
            </View>
          </>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Configuration.home.listing_item.offset,
    backgroundColor: Colors.backgroundColor,
  },
  categoryItem: {
    marginHorizontal: 5,
    marginVertical: 10,
    backgroundColor: "white",
    shadowColor: Colors.primary,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.border,
  },

  categoryItemPhoto: {
    height: 60,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    width: 110,
  },
  categoryItemTitle: {
    ...AppStyles.text.listTitle,
    margin: 10,
  },
  userPhoto: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginLeft: 10,
  },
  mapButton: {
    marginRight: 13,
    marginLeft: 7,
  },
  starStyle: {
    tintColor: Colors.foregroundColor,
  },
  starRatingContainer: {
    width: 90,
    marginTop: 10,
  },
});

export default memo(SearchScreen);
