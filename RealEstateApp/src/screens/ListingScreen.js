import React, { useEffect, useState, useRef } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  SafeAreaView,
} from "react-native";

import { useSelector, useDispatch } from "react-redux";

import {
  getListingRS,
  removeUpdateListing,
  saveUnsaveListing,
  getFavouriteListing,
  getRecentListings,
} from "../redux/actions/postListing";

import IIcon from "react-native-vector-icons/Ionicons";

import RentingSellingItem from "../components/rentingSellingItem";

export default function ListingScreen(props) {
  let { type } = props.route.params;
  const {
    buyingPosts,
    rentingPosts,
    favouritePosts,
    savedPosts,
    recentPosts,
    recentSaved,
  } = useSelector((state) => state.postReducer);
  const { user } = useSelector((state) => state.auth);
  const [loader, setLoader] = useState(false);
  const dispatch = useDispatch();

  const stopLoader = () => {
    setLoader(false);
  };

  useEffect(() => {
    // let unsubscribe = () => {};
    setLoader(true);
    if (type == "fav") {
      dispatch(getFavouriteListing(savedPosts, stopLoader));
    } else if (type == "recent") {
      dispatch(getRecentListings(recentSaved, stopLoader));
    } else {
      dispatch(getListingRS(type, user, stopLoader));
    }
    // return () => unsubscribe();
  }, []);

  const navigateTo = () => {
    props.navigation.goBack();
  };

  const renderTitle = () => {
    switch (type) {
      case "buy":
        return "Selling";
      case "rent":
        return "Renting";
      case "fav":
        return "Favorites";
      case "recent":
        return "Recent Viewed";
      default:
        break;
    }
  };

  const renderData = () => {
    switch (type) {
      case "buy":
        return buyingPosts;
      case "rent":
        return rentingPosts;
      case "fav":
        return favouritePosts;
      case "recent":
        return recentPosts;
      default:
        break;
    }
  };

  const renderItem = ({ item, index }) => (
    <RentingSellingItem
      item={item}
      index={index}
      dispatch={dispatch}
      removeUpdateListing={removeUpdateListing}
      saveUnsaveListing={saveUnsaveListing}
      type={type}
    />
  );
  console.log("buyingPosts in component", buyingPosts);
  return (
    <SafeAreaView style={styles.main}>
      <View style={styles.header}>
        <IIcon name="arrow-back" color="black" size={24} onPress={navigateTo} />
        <Text style={styles.hTitle}>{renderTitle()}</Text>
        <Text />
      </View>
      {loader ? (
        <View style={styles.container}>
          <ActivityIndicator size="small" color="green" />
        </View>
      ) : (
        <FlatList
          contentContainerStyle={{ flexGrow: 1 }}
          data={renderData()}
          renderItem={renderItem}
          keyExtractor={(item) => `${item.id}`}
          initialNumToRender={20}
          ListEmptyComponent={() => {
            return (
              <View style={styles.noItem}>
                <Text style={styles.text}>No items yet</Text>
              </View>
            );
          }}
        />
      )}
    </SafeAreaView>
  );
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
  noItem: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: "gray",
  },
  main: {
    flex: 1,
  },
  pri: { position: "absolute", bottom: 10, right: 10 },
  rightSwipeItem: {
    height: 120,
    width: 80,
    justifyContent: "center",
    marginTop: 20,
    alignItems: "center",
  },
  rowM: {
    flexDirection: "row",
    marginTop: 20,
    height: 120,
    width: "100%",
    backgroundColor: "white",
  },
  header: {
    height: 50,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    backgroundColor: "white",
    paddingHorizontal: 20,
  },
  hTitle: {
    fontSize: 20,
  },
});
