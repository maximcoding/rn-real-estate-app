import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  KeyboardAvoidingView,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { complainUs } from "../redux/actions/user";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Translate } from "../core/i18n/IMLocalization";
import { AppStyles } from "../AppStyles";
import { Fonts } from "../themes";

export default function Contact(props) {
  const dispatch = useDispatch();
  let { user } = useSelector((state) => state.auth);
  const [state, setState] = useState({
    complain: "",
  });
  const [loader, setLoader] = useState(false);

  const stopLoader = (success) => {
    setLoader(false);
    if (success) {
      resetState();
      alert("Complain Submitted");
    }
  };

  const resetState = () => {
    setState({
      ...state,
      complain: "",
    });
  };

  const submitForm = () => {
    let { complain } = state;
    if (complain == "") {
      alert("Must write something");
      return;
    }
    setLoader(true);
    dispatch(complainUs(complain, user, stopLoader));
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text style={styles.title}>{Translate("Contact Us")}</Text>
        <Text style={styles.desc}>
          {Translate(
            "If you have any problem regarding using this application you can contact us here."
          )}
          <Text style={styles.span}>{Translate("Press Send")} </Text>
          {Translate("button when your are done.")}
        </Text>

        <View style={styles.inputContainer}>
          <Text style={AppStyles.text.listTitle}>
            {Translate("Complain Here")}
          </Text>

          <TextInput
            placeholder={"Write your message..."}
            style={styles.input}
            value={state.complain}
            onChangeText={(text) => setState({ ...state, complain: text })}
            multiline={true}
            numberOfLines={6}
            textAlignVertical={"top"}
          />
        </View>

        <TouchableOpacity
          style={styles.btn}
          disabled={loader}
          onPress={submitForm}
        >
          {loader ? (
            <ActivityIndicator size="small" color="white" />
          ) : (
            <Text style={styles.btnText}>{Translate("Send")}</Text>
          )}
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    backgroundColor: "white",
  },
  title: {
    ...AppStyles.text.listTitle,
    fontWeight: "500",
    textAlign: "center",
    alignItems: "center",
    paddingTop: 20,
  },
  span: {
    fontWeight: "700",
  },
  desc: {
    paddingTop: 10,
    flexDirection: "column",
    textAlign: "justify",
    lineHeight: 22,
    fontFamily: "Arial",
    fontSize: Fonts.size.regular,
    opacity: 0.6,
  },
  input: {
    width: "100%",
    height: 150,
    borderRadius: 5,
    marginTop: 4,
    borderColor: "#d3d3d3",
    borderWidth: 1,
    fontSize: Fonts.size.regular,
    padding: 10,
  },
  inputContainer: {
    marginTop: 20,
    alignItems: "flex-start",
  },
  btn: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    backgroundColor: "#21c064",
    borderRadius: 5,
    width: "100%",
    height: 45,
  },
  btnText: {
    fontSize: 17,
    color: "white",
  },
});
