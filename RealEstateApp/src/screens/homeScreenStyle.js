import { StyleSheet } from "react-native";
import { AppStyles } from "../AppStyles";
import { Configuration } from "../Configuration";
import Colors from "../themes/colors";
import Fonts from "../themes/fonts";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Configuration.home.listing_item.offset,
  },
  shareCompareButtons: { alignSelf: "flex-end", position: "absolute" },
  imageBackground: {
    flex: 1,
    borderRadius: 7,
    backgroundColor: "#222",
    resizeMode: "center",
  },
  itemContainer: {
    flex: 1,
    width: "100%",
  },
  addressWrapper: {
    borderBottomWidth: 1,
    borderColor: Colors.description,
    paddingLeft: "1%",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    paddingVertical: 5,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  addressText: {
    marginLeft: "1%",
    padding: 3,
    paddingLeft: "1%",
    fontSize: Fonts.size.normal,
    color: Colors.primary,
  },
  detailsWrapper: {
    paddingTop: "1%",
  },
  rowDetails: {
    width: "87%",
    marginLeft: "1%",
    marginTop: 4,
    opacity: 0.9,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  leftColumn: {
    flexDirection: "row",
    alignItems: "center",
    paddingRight: 5,
    paddingLeft: 5,
    borderRadius: 2,
    backgroundColor: Colors.backgroundText,
  },
  rightColumn: {
    flexDirection: "row",
    padding: 2,
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 2,
    alignItems: "center",
    backgroundColor: Colors.backgroundText,
  },
  starRatingWrapper: {
    position: "absolute",
    right: 50,
    bottom: 10,
    width: 110,
    opacity: 0.8,
    padding: 2,
    borderRadius: 2,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.backgroundText,
  },
  starRating: {
    width: 90,
  },
  textWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  priceValue: {
    color: Colors.primary,
    fontSize: Fonts.size.small,
    paddingRight: 1,
  },
  value: {
    color: Colors.primary,
    fontWeight: "bold",
    fontSize: Fonts.size.small,
    opacity: 0.8,
    paddingRight: 5,
  },
  valueDate: {
    color: Colors.primary,
    fontSize: Fonts.size.small,
    opacity: 0.8,
    paddingRight: 5,
  },
  label: {
    color: Colors.primary,
    fontFamily: Fonts.family.main,
    fontSize: Fonts.size.small,
    paddingRight: 5,
  },
  icon: {
    paddingLeft: 2,
    paddingRight: 5,
    fontSize: Fonts.size.small,
    color: Colors.primary,
  },
  symbol: {
    paddingLeft: 2,
    paddingRight: 5,
    fontSize: Fonts.size.small,
    color: Colors.primary,
  },
  listingTitle: {
    marginTop: 10,
    marginBottom: 10,
  },
  categories: {
    marginBottom: 7,
  },
  categoryItemContainer: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#e0e0e0",
    paddingBottom: 10,
  },
  categoryItemPhoto: {
    height: 60,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    width: 110,
  },
  categoryItemTitle: {
    fontFamily: Fonts.family.main,
    fontWeight: "bold",
    color: Colors.title,
    margin: 10,
  },
  userPhoto: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginLeft: 10,
  },
  headerButton: {
    marginRight: 4,
    fontFamily: "Arial",
  },
  starStyle: {
    tintColor: Colors.foregroundColor,
  },
  badge: {
    height: 16,
    width: 16,
    borderRadius: 16 / 2,
    backgroundColor: Colors.primary,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    top: -7,
    right: 7,
  },
  paddingH: {
    paddingHorizontal: 20,
  },
  backImage: {
    color: "gray",
  },
  blankView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
