import React, { useState, useEffect } from "react";
import {
  Switch,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  TextInput,
  LayoutAnimation,
  Platform,
  UIManager,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import RNPickerSelect from "react-native-picker-select";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import QuantityPicker from "../components/quantityPicker";
import TextButton from "react-native-button";
import { useSelector, useDispatch } from "react-redux";
import { filterPosts } from "../redux/actions/filters";
import { selectorData } from "../utils/filterHelper";
import Icon from "react-native-vector-icons/Ionicons";
import { Translate } from "../core/i18n/IMLocalization";
import SliderCustomLabel from "../components/SliderCustomLabel";
import { Colors } from "../themes";
import { AppStyles } from "../AppStyles";
import { getSymbol } from "../utils/currencyConverter";
import { onClearFilter, onChangeFilter } from "../redux/actions/filters";

export default function SelectFilters({ navigation }) {
  const dispatch = useDispatch();
  const { categories } = useSelector((state) => state.categories);
  const { user } = useSelector((state) => state.auth);
  const { filterValues = {} } = useSelector((state) => {
    console.log("stat in selector", state.filterReducer);
    return state.filterReducer;
  });

  const [loading, setLoading] = useState(false);

  const clearFilter = () => {
    console.log("clearFilter Callded");
    dispatch(onClearFilter());
  };

  const stopLoader = (success) => {
    setLoading(false);
    if (success) {
      navigation.navigate("Home");
    }
  };

  const onChangeFilterHandler = (name, value) => {
    dispatch(onChangeFilter({ name, value }));
  };

  const filterPostsAction = () => {
    setLoading(true);
    dispatch(filterPosts(filterValues, stopLoader));
  };
  console.log("filter values in component", filterValues);
  useEffect(() => {
    if (!filterValues.persistState) {
      clearFilter();
    }
  }, []);

  return (
    <SafeAreaView>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon name="arrow-back" size={25} />
        </TouchableOpacity>
        <Text
          style={[
            AppStyles.text.headerTitle,
            { alignItems: "center", textAlign: "center" },
          ]}
        >
          {Translate("Filters")}
        </Text>
        <TouchableOpacity onPress={clearFilter}>
          <Text style={AppStyles.text.headerButton}>{Translate("Clear")}</Text>
        </TouchableOpacity>
      </View>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          backgroundColor: Colors.backgroundColor,
          paddingHorizontal: 10,
        }}
      >
        <View style={{ alignItems: "center", paddingVertical: 20 }}>
          <Text
            style={[
              AppStyles.text.listTitle,
              { alignSelf: "center", paddingBottom: 10 },
            ]}
          >
            {Translate("Price")} {getSymbol(user)}
          </Text>
          <MultiSlider
            selectedStyle={{ backgroundColor: "#0073eb" }}
            trackStyle={{ height: 2 }}
            onValuesChange={(v) => {
              onChangeFilterHandler("price", v);
            }}
            customLabel={SliderCustomLabel}
            isMarkersSeparated={true}
            enableLabel
            min={1000}
            max={10000000}
            snapped
            allowOverlap={false}
            values={[filterValues.price[0], filterValues.price[1]]}
          />
        </View>
        <View style={{ alignItems: "center", paddingVertical: 10 }}>
          <Text
            style={[
              AppStyles.text.listTitle,
              { alignSelf: "center", paddingBottom: 10 },
            ]}
          >
            {Translate("Square")} {user.unit ? user.unit : "feet"}
          </Text>
          <MultiSlider
            selectedStyle={{ backgroundColor: "#0073eb" }}
            customLabel={SliderCustomLabel}
            trackStyle={{ height: 2 }}
            onValuesChange={(v) => {
              onChangeFilterHandler("square", v);
            }}
            isMarkersSeparated={true}
            enableLabel
            min={2}
            max={20}
            snapped
            allowOverlap={false}
            values={[filterValues.square[0], filterValues.square[1]]}
          />
        </View>
        {filterValues.rent == "buy" && (
          <View style={{ alignItems: "center", paddingVertical: 10 }}>
            <Text style={[styles.title, { alignSelf: "center" }]}>
              {Translate("Deposit")} %
            </Text>
            <MultiSlider
              sliderLength={280}
              customLabel={SliderCustomLabel}
              selectedStyle={{ backgroundColor: "#0073eb" }}
              trackStyle={{ height: 2 }}
              onValuesChange={(v) => {
                onChangeFilterHandler("deposit", v);
              }}
              isMarkersSeparated={true}
              enableLabel
              min={1}
              max={100}
              snapped
              allowOverlap={false}
              values={[filterValues.deposit[0], filterValues.deposit[1]]}
            />
          </View>
        )}

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginVertical: 15,
          }}
        >
          <Text style={styles.title}>Use exact match for bedrooms</Text>
          <Switch
            onValueChange={() => {
              LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
              onChangeFilterHandler("useExact", !filterValues.useExact);
            }}
            thumbColor={"#ffff"}
            trackColor={{ false: "#DDDD", true: "#0073eb" }}
            value={filterValues.useExact}
          />
        </View>
        {filterValues.useExact ? (
          <View>
            {["bathrooms", "bedrooms"].map((o) => {
              return (
                <>
                  <View style={[styles.appSettingsTypeContainer]}>
                    <Text style={styles.title}>{o}</Text>
                    <TextInput
                      underlineColorAndroid="transparent"
                      style={[styles.text, { textAlign: "right" }]}
                      onChangeText={(text) => {
                        onChangeFilterHandler(o, text);
                      }}
                      placeholderTextColor={"#d3d3d3"}
                      keyboardType={"phone-pad"}
                      placeholder={`Enter number of ${o}`}
                      value={filterValues[o]}
                      maxLength={3}
                    />
                  </View>
                  <View style={styles.divider} />
                </>
              );
            })}
          </View>
        ) : (
          <>
            <QuantityPicker
              title="Bathrooms"
              name="bathrooms"
              getValue={(value) => onChangeFilterHandler("bathrooms", value)}
              value={filterValues.bathrooms}
            />
            <QuantityPicker
              title="Bedrooms"
              name="bedrooms"
              getValue={(value) => onChangeFilterHandler("bedrooms", value)}
              value={filterValues.bedrooms}
            />
          </>
        )}

        {selectorData(categories).map((obj) => {
          console.log("obj in cate", obj.data);
          return (
            <>
              <View style={styles.row}>
                <Text style={styles.title}>{obj.title}</Text>
                <RNPickerSelect
                  placeholder={{
                    label: obj.title,
                    value: obj.data[0],
                    color: "black",
                  }}
                  onValueChange={(value) => {
                    console.log("value on change", value);
                    onChangeFilterHandler(obj.name, value);
                  }}
                  value={filterValues[obj.name]}
                  items={obj.data}
                  style={AppStyles.pickerSelectStyles}
                  useNativeAndroidPickerStyle={false}
                />
              </View>
              <View style={styles.divider} />
            </>
          );
        })}

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginVertical: 15,
          }}
        >
          <Text style={styles.title}>Save Filter Values</Text>
          <Switch
            onValueChange={() => {
              onChangeFilterHandler("persistState", !filterValues.persistState);
            }}
            thumbColor={"#ffff"}
            trackColor={{ false: "#DDDD", true: "#0073eb" }}
            value={filterValues.persistState}
          />
        </View>

        <TextButton
          containerStyle={[
            styles.addButtonContainer,
            { backgroundColor: "#21c064" },
          ]}
          onPress={filterPostsAction}
          style={[styles.addButtonText]}
          disabled={loading}
        >
          {loading ? (
            <ActivityIndicator size="small" color={"white"} />
          ) : (
            "Filter"
          )}
        </TextButton>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  mapView: {
    width: "100%",
    height: "100%",
    backgroundColor: Colors.grey,
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
  header: {
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 15,
    elevation: 2,
    backgroundColor: "white",
  },
  headerTitle: {
    fontSize: 17,
  },
  clearBtn: {
    fontSize: 17,
    color: "#00cc66",
  },
  addButtonContainer: {
    backgroundColor: Colors.foregroundColor,
    borderRadius: 5,
    padding: 15,
    margin: 10,

    marginTop: 27,
    marginBottom: 55,
  },
  addButtonText: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 15,
  },
  selectList: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: Colors.border,
  },
  title: {
    fontSize: 16,
    fontWeight: "700",
    color: "rgba(0,0,0,0.5)",
    paddingHorizontal: 10,
    textTransform: "capitalize",
  },

  appSettingsTypeContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 60,
  },
  text: {
    fontSize: 17,
    color: "gray",
    paddingRight: Platform.OS == "ios" ? 5 : 15,
  },
  divider: {
    height: 0.5,
    width: "100%",
    alignSelf: "flex-end",
    backgroundColor: "#d3d3d3",
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 60,
  },
});

if (Platform.OS === "android") {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}
