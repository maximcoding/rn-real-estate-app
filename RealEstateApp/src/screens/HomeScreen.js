import React, { useState, useEffect, memo, useRef } from "react";
import { FlatList, View, Alert, Image, Text, SafeAreaView } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import ActionSheet from "react-native-actionsheet";
import PostModal from "../components/PostModal";
import Geolocation from "@react-native-community/geolocation";
import { Translate } from "../core/i18n/IMLocalization";
import { styles } from "./homeScreenStyle";
import ListItem from "../components/listItem";
import RowView from "../components/rowView";
import ListView from "../components/listView";
import SortHomeModal from "../components/sortHomeModal";
import GridModal from "../components/girdViews";
import {
  subscribeListingCategories,
  getListing,
  subscribeSavedListings,
  removeListing,
  saveUnsaveListing,
  subscribeRecentListings,
} from "../redux/actions/postListing";
import { resetFilter } from "../redux/actions/filters";
import { changeView, changeSort } from "../redux/actions/user";
import {
  HomeScreenOptions,
  FilterHeader,
} from "../components/homeScreenHeaders";
import { AppStyles } from "../AppStyles";

function HomeScreen(props) {
  let { user, currencyRates, grid, sortBy } = useSelector(
    (state) => state.auth
  );
  let { posts, savedPosts } = useSelector((state) => state.postReducer);
  let { categories } = useSelector((state) => state.categories);
  let { showFilter, filters } = useSelector((state) => state.filterReducer);
  let { updator } = useSelector((state) => state.languageReducer);
  const dispatch = useDispatch();
  const refAction = useRef();
  let [state, setState] = useState({
    selectedItem: null,
    postModalVisible: false,
    location: "",
  });
  let [isOpen, setIsOpen] = useState(false);
  let [isGrid, setIsGrid] = useState(false);

  useEffect(() => {
    dispatch(subscribeListingCategories(onCategoriesCollectionUpdate));
    dispatch(getListing(getListingDetails, sortBy));
    dispatch(
      subscribeSavedListings(user?._id, onSavedListingsCollectionUpdate)
    );
    dispatch(subscribeRecentListings(user?._id, recentLoader));

    getCurrentPosition();
  }, []);

  const getCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        setState({ ...state, location: position.coords });
      },
      (error) => console.log(error.message),
      { enableHighAccuracy: false, timeout: 1000 }
    );
  };

  const getListingDetails = () => {
    setState({ ...state, loading: false });
  };

  const closeSort = () => {
    setIsOpen(false);
  };

  const closeGrid = () => {
    setIsGrid(false);
  };

  const recentLoader = () => {
    setState({ ...state, loading: false });
  };

  const resetFilterHelper = () => {
    dispatch(resetFilter());
  };

  const openSort = () => {
    setIsOpen(true);
  };

  const openGrid = () => {
    setIsGrid(true);
  };

  const onCategoriesCollectionUpdate = () => {
    setState({ ...state, loading: false });
  };

  const onSavedListingsCollectionUpdate = () => {
    setState({
      ...state,
      loading: false,
    });
  };

  const onPressPost = () => {
    setState({
      ...state,
      selectedItem: null,
      postModalVisible: true,
    });
  };

  const onPostCancel = () => {
    setState({ ...state, postModalVisible: false });
  };

  const onPressListingItem = (item) => {
    props.navigation.navigate("Detail", {
      item: item,
      customLeft: true,
      routeName: "Home",
    });
  };

  const onLongPressListingItem = (item) => {
    if (item.authorID === user.id) {
      setState({ ...state, selectedItem: item });
      refAction.current.show();
    }
  };

  const onPressSavedIcon = (item) => {
    dispatch(saveUnsaveListing(item, user.id, savedPosts));
  };

  const sortPosts = (value) => {
    dispatch(changeSort(value));
    dispatch(getListing(getListingDetails, value, state.location));
  };

  const setGrid = (value) => {
    dispatch(changeView(value));
  };

  const onListingItemActionDone = (index) => {
    if (index == 0) {
      setState({
        ...state,
        postModalVisible: true,
      });
    }
    if (index == 1) {
      Alert.alert(
        Translate("Delete Listing"),
        Translate("Are you sure you want to remove this listing?"),
        [
          {
            text: Translate("Yes"),
            onPress: removeListingFunc,
            style: "destructive",
          },
          { text: Translate("No") },
        ],
        { cancelable: false }
      );
    }
  };
  const deleteCallback = (success) => {
    if (!success) {
      alert(
        Translate("There was an error deleting the listing. Please try again")
      );
    }
  };
  const removeListingFunc = () => {
    dispatch(removeListing(state.selectedItem, deleteCallback));
  };

  const renderListingItem = ({ item, index }) => {
    switch (grid) {
      case "images":
        return (
          <ListItem
            item={item}
            index={index}
            onPressListingItem={onPressListingItem}
            onLongPressListingItem={onLongPressListingItem}
            onPressSavedIcon={onPressSavedIcon}
            savedPosts={savedPosts}
            currencyRates={currencyRates}
            user={user}
          />
        );

      case "lists":
        return (
          <ListView
            item={item}
            index={index}
            onPressListingItem={onPressListingItem}
            onLongPressListingItem={onLongPressListingItem}
            onPressSavedIcon={onPressSavedIcon}
            savedPosts={savedPosts}
            currencyRates={currencyRates}
            user={user}
          />
        );
      case "text":
        return (
          <RowView
            item={item}
            index={index}
            onPressListingItem={onPressListingItem}
            onLongPressListingItem={onLongPressListingItem}
            onPressSavedIcon={onPressSavedIcon}
            savedPosts={savedPosts}
            currencyRates={currencyRates}
            user={user}
          />
        );
    }
  };

  return (
    <SafeAreaView style={AppStyles.screenContainer}>
      {showFilter ? (
        <FilterHeader resetFilter={resetFilterHelper} length={filters.length} />
      ) : (
        <HomeScreenOptions
          navigation={props.navigation}
          user={user}
          onPressPost={onPressPost}
          openSort={openSort}
          openGrid={openGrid}
          isOpen={isOpen}
          isGrid={isGrid}
        />
      )}

      <View style={styles.container}>
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1 }}
          data={showFilter ? filters : posts}
          renderItem={renderListingItem}
          keyExtractor={(item, index) => item.id}
          ListEmptyComponent={() => {
            return (
              <View style={styles.blankView}>
                <Text style={styles.backImage}>No result found !</Text>
              </View>
            );
          }}
        />
        {state.postModalVisible && (
          <PostModal
            categories={categories}
            onCancel={onPostCancel}
            selectedItem={state.selectedItem}
          />
        )}
        <ActionSheet
          ref={refAction}
          title={Translate("Confirm")}
          options={[
            Translate("Edit Listing"),
            Translate("Remove Listing"),
            Translate("Cancel"),
          ]}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={(index) => {
            onListingItemActionDone(index);
          }}
        />

        <SortHomeModal
          isOpen={isOpen}
          closeSort={closeSort}
          callBack={sortPosts}
          sortBy={sortBy}
        />
        <GridModal
          isOpen={isGrid}
          closeSort={closeGrid}
          callBack={setGrid}
          grid={grid}
        />
      </View>
    </SafeAreaView>
  );
}

export default memo(HomeScreen);
