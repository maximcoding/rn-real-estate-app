import React from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  Image,
  View,
  BackHandler,
  TextInput,
} from "react-native";
import { firebaseListing } from "../firebase";
import { AppIcon, AppStyles, ListStyle, HeaderButtonStyle } from "../AppStyles";
import { Marker } from "react-native-maps";
import { Translate } from "../core/i18n/IMLocalization";
import Ionicons from "react-native-vector-icons/Ionicons";
import Colors from "../themes/colors";
class FilterScreenListing extends React.Component {
  static navigationOptions = ({ screenProps, navigation, route }) => {
    return {
      title: "Listing",
    };
  };
  constructor(props) {
    super(props);

    const { route } = props;
    const item = route.params.item;
    const selectedCategoryId = route.params.selectedCategoryId;

    this.state = {
      category: item,
      filter: {},
      selectedCategoryId: selectedCategoryId,
      data: [],
      mapMode: false,
      isSearch: false,
      filterModalVisible: false,
      latitudeDelta: 0,
      longitudeDelta: 0,
      shouldUseOwnLocation: true, // Set this to false to hide the user's location
    };
    this.arrayholder = [];
    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );

    this.unsubscribe = null;
  }
  getListingDetails = (querySnapshot) => {
    const data = [];
    querySnapshot.forEach((doc) => {
      console.log(
        "FilterScreenListing -> getListingDetails",
        JSON.stringify(doc.data(), null, 2)
      );
      const Listing = doc.data();
      if (Listing.categoryID == this.state.selectedCategoryId) {
        data.push({
          ...Listing,
        });
      }
    });
    this.setState({
      filterListing: data,
      loading: false,
    });
    this.arrayholder = data;
  };
  componentDidMount() {
    this.getListing = firebaseListing.getListing(this.getListingDetails);
    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {}

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onChangeLocation = (location) => {
    this.setState({
      latitude: location.latitude,
      longitude: location.longitude,
    });
  };

  onSelectFilter = () => {
    this.setState({ filterModalVisible: true });
  };

  onSelectFilterCancel = () => {
    this.setState({ filterModalVisible: false });
  };

  onSelectFilterDone = (filter) => {
    this.setState({ filter: filter });
    this.setState({ filterModalVisible: false });
    this.unsubscribe = firebaseListing.subscribeListings(
      { categoryId: this.state.category.id },
      this.onCollectionUpdate
    );
  };

  onChangeMode = () => {
    const newMode = !this.state.mapMode;
    this.setState({ mapMode: newMode });
    this.props.navigation.setParams({
      mapMode: newMode,
    });
  };

  onCollectionUpdate = (querySnapshot) => {
    const data = [];
    let max_latitude = -400,
      min_latitude = 400,
      max_longitude = -400,
      min_logitude = 400;

    const filter = this.state.filter;
    querySnapshot.forEach((doc) => {
      const listing = doc.data();
      console.log(listing.filters);
      let matched = true;
      Object.keys(filter).forEach(function (key) {
        if (
          filter[key] != "Any" &&
          filter[key] != "All" &&
          listing.filters[key] != filter[key]
        ) {
          matched = false;
        }
      });

      console.log("matched=" + matched);

      if (!matched) return;

      if (max_latitude < listing.latitude) max_latitude = listing.latitude;
      if (min_latitude > listing.latitude) min_latitude = listing.latitude;
      if (max_longitude < listing.longitude) max_longitude = listing.longitude;
      if (min_logitude > listing.longitude) min_logitude = listing.longitude;
      data.push({ ...listing, id: doc.id });
    });

    if (!this.state.shouldUseOwnLocation || !this.state.latitude) {
      this.setState({
        latitude: (max_latitude + min_latitude) / 2,
        longitude: (max_longitude + min_logitude) / 2,
        latitudeDelta: Math.abs(
          (max_latitude - (max_latitude + min_latitude) / 2) * 3
        ),
        longitudeDelta: Math.abs(
          (max_longitude - (max_longitude + min_logitude) / 2) * 3
        ),
        data,
      });
    } else {
      this.setState({
        data,
      });
    }
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%",
        }}
      />
    );
  };

  onPress = (item) => {
    this.props.navigation.navigate("Detail", {
      item: item,
      customLeft: true,
      routeName: "Listing",
    });
  };
  searchFilter = (text) => {
    const newData = this.arrayholder.filter((item) => {
      const itemData = `${item.title.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      filterListing: newData,
    });
  };
  renderItem = ({ item }) => {
    return (
      <View
        style={{
          height: 120,
          width: "100%",
          marginTop: 20,
          paddingLeft: 10,
          paddingRight: 10,
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View style={{ flexDirection: "row", width: "75%" }}>
          <Image
            source={{ uri: item.photo }}
            style={{ height: "100%", width: 120, resizeMode: "contain" }}
          />
          <View
            style={{
              paddingLeft: 10,
              width: "60%",
              paddingVertical: 10,
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{ fontSize: 18, fontWeight: "600", paddingBottom: 10 }}
            >
              {item.title}
            </Text>
            <Text style={[ListStyle.place, { paddingBottom: 10 }]}>
              14 days ago
            </Text>
            <Text
              style={[ListStyle.place, { fontSize: 16, fontWeight: "600" }]}
            >
              {item.place}
            </Text>
          </View>
        </View>
        <Text
          numberOfLines={1}
          style={[ListStyle.price, { paddingBottom: 10, fontSize: 16 }]}
        >
          ${item.price[0]}
        </Text>
      </View>
    );
  };

  onPress = (item) => {
    this.props.navigation.navigate("Detail", {
      item: item,
      customLeft: true,
      routeName: "Listing",
    });
  };

  render() {
    const markerArr = this.state.data.map((listing) => (
      <Marker
        title={listing.title}
        description={listing.description}
        onCalloutPress={() => {
          this.onPress(listing);
        }}
        coordinate={{
          latitude: listing.latitude,
          longitude: listing.longitude,
        }}
      />
    ));

    return (
      <View style={{ flex: 1, borderBottomColor: Colors.border }}>
        <View
          style={{
            flexDirection: "row",
            width: "95%",
            justifyContent: "space-between",
            alignItems: "center",
            margin: 10,
            marginTop: 0,
            marginBottom: 0,
          }}
        >
          <View
            style={{
              height: 35,
              width: this.state.isSearch ? "85%" : "100%",
              backgroundColor: "#EEEE",
              borderRadius: 10,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingLeft: 5,
              paddingRight: 5,
            }}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Ionicons
                name="ios-search"
                style={{ fontSize: 18, color: "gray", paddingRight: 5 }}
              />
              <TextInput
                onFocus={() => this.setState({ isSearch: true })}
                onChangeText={(text) => this.searchFilter(text)}
                placeholder="Search"
                style={{ fontSize: 16, fontWeight: "800" }}
              />
            </View>
            <Ionicons
              name="ios-close"
              style={{ fontSize: 24, color: "gray", paddingRight: 5 }}
            />
          </View>
          {this.state.isSearch ? (
            <Text
              onPress={() => this.setState({ isSearch: false })}
              style={{ fontWeight: "800", color: "gray" }}
            >
              {Translate("Cancel")}
            </Text>
          ) : null}
        </View>
        <FlatList
          data={this.state.filterListing}
          renderItem={this.renderItem}
          keyExtractor={(item) => `${item.id}`}
          initialNumToRender={5}
          refreshing={this.state.refreshing}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mapView: {
    width: "100%",
    height: "100%",
    backgroundColor: Colors.grey,
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
});

export default FilterScreenListing;
