import axios from "axios";

export const cloudinaryImageUrl = (source) =>
  new Promise((resolve, reject) => {
    const data = new FormData();
    data.append("file", source);
    data.append("upload_preset", "gagotapp");
    data.append("cloud_name", "ds70n3ejp");
    axios
      .post("https://api.cloudinary.com/v1_1/pigui/upload", data)
      .then(resolve)
      .catch(reject);
  });
