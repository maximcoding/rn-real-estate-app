import { INSTANCE } from "./instance";

// signup api
export const signUpWithEmailApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: "/user/signup",
      data: data,
    })
      .then(resolve)
      .catch(reject);
  });

// login api
export const loginWithEmailApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: "/user/login",
      data: data,
    })
      .then(resolve)
      .catch(reject);
  });
// login api
export const loginWithPhoneApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: "/user/loginWithPhone",
      data: data,
    })
      .then(resolve)
      .catch(reject);
  });

// Get current user
export const getUserApi = () =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "GET",
      url: "/user/authenticate",
    })
      .then(resolve)
      .catch(reject);
  });

// logout api

export const logoutApi = () =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: "/user/logout",
    })
      .then(resolve)
      .catch(reject);
  });

// send otp
export const sendVerificationCodeApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: "/user/sendCode",
      data,
    })
      .then(resolve)
      .catch(reject);
  });
// add new real state
export const addStateApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: "/states/addListing",
      data,
    })
      .then(resolve)
      .catch(reject);
  });

// update User

export const updateUserApi = (data, uid) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "PUT",
      url: `/user/updateUser/${uid}`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

// add Complain

export const addComplainApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: `/complain/addComplain`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

// filter categories api
export const filterCategoriesApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: `/states/filterCategories`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

// get listing api
export const getListingApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: `/states/getListing`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

// get categories api
export const getCategories = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "GET",
      url: `/categories/getCategories`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

// filter listing api
export const filterListing = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: `/states/filter`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

export const getListingByID = (id) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "GET",
      url: `/states/getByID/${id}`,
    })
      .then(resolve)
      .catch(reject);
  });

export const saveRecentListingApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: `/recent/addRecent`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

export const getRecentListingApi = (uid) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "GET",
      url: `/recent/getRecent/${uid}`,
    })
      .then(resolve)
      .catch(reject);
  });

export const postReviewApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: `/reviews/postReview`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

export const getReviewApi = (listingID) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "GET",
      url: `/reviews/getReview/${listingID}`,
    })
      .then(resolve)
      .catch(reject);
  });

export const getPostedReviewApi = (uid) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "GET",
      url: `/reviews/getPostReviews/${uid}`,
    })
      .then(resolve)
      .catch(reject);
  });

export const replyReviewApi = (reviewID, data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "PUT",
      url: `/reviews/replyReview/${reviewID}`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

export const updateListingApi = (id, data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "PUT",
      url: `/states/updateListing/${id}`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });

export const getListingRSApi = (type) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "GET",
      url: `/states/getListingRS/${type}`,
    })
      .then(resolve)
      .catch(reject);
  });

export const deleteListingApi = (id) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "DELETE",
      url: `/states/deleteListing/${id}`,
    })
      .then(resolve)
      .catch(reject);
  });
export const changeStatusApi = (data) =>
  new Promise((resolve, reject) => {
    INSTANCE({
      method: "POST",
      url: `/states/changeStatus`,
      data,
    })
      .then(resolve)
      .catch(reject);
  });
