import {PopularityEnum, PropertyStatusEnum, SquareUnitsEnum} from './enums';

export interface IPropertyPreview {
    uuid: string;
    pic: string;
    price: string;
    agent: string;
    deposit: string;
    hasAlert: boolean;
    rating: number;
    floor: number;
    square: number;
    bathrooms: number;
    bedrooms: number;
    popularity: PopularityEnum;
    squareUnits: SquareUnitsEnum;
    address: string;
    status: PropertyStatusEnum;
    updatedAt: string;
}


export interface IPropertyDetails extends IPropertyPreview {
    contactAgent: any; // picture of agent -> link to agent profile
    parking: boolean;
    airCondition: boolean;
    status: PropertyStatusEnum,
    photos: any[];
    video: any;
    virtualTour: any;
    yearBuilt: number;
    securityCameras: boolean;
    securityGuard: boolean;
    fitnessCenter: boolean;
    garage: boolean;
    swimmingPool: boolean;
    balcony: any;
    chart: any; // area in demand
    emergencyExit: boolean;
    nextTo: string[]; //(checkboxes)  Next To : Busway / School / Gargen / Shop / Mall / Theater / Sea / Dog park / Tennis Cours
}
