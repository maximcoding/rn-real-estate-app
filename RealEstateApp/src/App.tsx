import React, { useEffect } from "react";
import { Text } from "react-native";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { AppRegistry, PermissionsAndroid, Platform } from "react-native";
import configureStore from "./redux/store";
import { RootNavigator } from "./navigation/AppNavigator";
import { SafeAreaProvider } from "react-native-safe-area-context";
import AsyncStorage from "@react-native-community/async-storage";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import Ionicons from "react-native-vector-icons/Ionicons";
import { setI18nConfig } from "./core/i18n/IMLocalization";

let { store, persistor } = configureStore();

FontAwesome5.getStyledIconSet("brand").loadFont();
FontAwesome5.getStyledIconSet("light").loadFont();
FontAwesome5.getStyledIconSet("regular").loadFont();
FontAwesome5.getStyledIconSet("solid").loadFont();

Ionicons.loadFont();
FontAwesome.loadFont();

const App = () => {
  const getStarter = async () => {
    let code = await AsyncStorage.getItem("language");
    let languageCode = code ? code : "en";
    setI18nConfig(languageCode);
  };

  const getPermissions = async () => {
    try {
      if (Platform.OS == "ios") {
        return true;
      } else {
        const permission = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );
        if (permission === PermissionsAndroid.RESULTS.GRANTED) {
          return true;
        } else {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
          );
        }
      }
    } catch (error) {
      getPermissions();
    }
  };

  const getImagePermission = async () => {
    try {
      if (Platform.OS == "ios") {
      } else {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          return true;
        } else {
          await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA
          );
        }
      }
    } catch (err) {
      getImagePermission();
    }
  };

  useEffect(() => {
    getStarter();
    getPermissions();
  }, []);

  console.disableYellowBox = true;
  return (
    <Provider store={store}>
      <PersistGate loading={<Text></Text>} persistor={persistor}>
        <SafeAreaProvider>
          <RootNavigator />
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  );
};

App.propTypes = {};
App.defaultProps = {};
AppRegistry.registerComponent("App", () => App);

export default App;
