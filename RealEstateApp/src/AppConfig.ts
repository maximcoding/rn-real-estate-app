import { Translate, setI18nConfig } from "./core/i18n/IMLocalization";
import { Images } from "./themes";

//setI18nConfig();

const regexForNames = /^[a-zA-Z]{2,25}$/;
const regexForPhoneNumber = /\d{9}$/;

export const AppConfig = {
  isSMSAuthEnabled: true,
  appIdentifier: "rn-real-estate-android",
  onboardingConfig: {
    welcomeTitle: Translate("welcomeTitle"),
    welcomeCaption: Translate("welcomeCaption"),
    walkthroughScreens: [
      {
        icon: Images.logo,
        title: Translate("Build your perfect app"),
        description: Translate("Launch your own real estate app in minutes."),
      },
      {
        icon: Images.map,
        title: Translate("Map View"),
        description: Translate(
          "Visualize houses on the map to make your search easier."
        ),
      },
      {
        icon: Images.heart,
        title: Translate("Saved Places"),
        description: Translate(
          "Save your favorite places to come back to them later."
        ),
      },
      {
        icon: Images.filters,
        title: Translate("Advanced Custom Filters"),
        description: Translate(
          "Custom dynamic filters to accommodate all markets and all customer needs."
        ),
      },
      {
        icon: Images.instagram,
        title: Translate("Add New Listings"),
        description: Translate(
          "Add new listings directly from the app, including photo gallery and filters."
        ),
      },
      {
        icon: Images.chat,
        title: Translate("Chat"),
        description: Translate(
          "Communicate with your customers and real estate agents in real-time."
        ),
      },
      {
        icon: Images.notification,
        title: Translate("Get Notified"),
        description: Translate(
          "Stay on top of your game with real-time push notifications."
        ),
      },
    ],
  },

  tabIcons: {
    Home: {
      focus: Images.homefilled,
      unFocus: Images.homeUnfilled,
    },
    Categories: {
      focus: Images.collections,
      unFocus: Images.collections,
    },
    Messages: {
      focus: Images.commentFilled,
      unFocus: Images.commentUnfilled,
    },
    Search: {
      focus: Images.search,
      unFocus: Images.search,
    },
  },
  reverseGeoCodingAPIKey: "AIzaSyCDeWXVrJxUCRQlpcWK2JJQSB-kFVjCqlM",
  tosLink: "https://www.instamobile.io/eula-instachatty/",
  editProfileFields: {
    sections: [
      {
        title: Translate("PUBLIC PROFILE"),
        fields: [
          {
            displayName: Translate("First Name"),
            type: "text",
            editable: true,
            regex: regexForNames,
            key: "firstName",
            placeholder: Translate("Your first name"),
          },
          {
            displayName: Translate("Last Name"),
            type: "text",
            editable: true,
            regex: regexForNames,
            key: "lastName",
            placeholder: Translate("Your last name"),
          },
        ],
      },
      {
        title: Translate("PRIVATE DETAILS"),
        fields: [
          {
            displayName: Translate("E-mail Address"),
            type: "text",
            editable: false,
            key: "email",
            placeholder: Translate("Your email address"),
          },
          {
            displayName: Translate("Phone Number"),
            type: "text",
            editable: true,
            regex: regexForPhoneNumber,
            key: "phone",
            placeholder: Translate("Your phone number"),
          },
        ],
      },
    ],
  },
  userSettingsFields: {
    sections: [
      {
        title: Translate("GENERAL"),
        fields: [
          {
            displayName: Translate("Allow Push Notifications"),
            type: "switch",
            editable: true,
            key: "push_notifications_enabled",
            value: false,
          },
          {
            displayName: Translate("Enable Face ID / Touch ID"),
            type: "switch",
            editable: true,
            key: "face_id_enabled",
            value: false,
          },
        ],
      },
      {
        title: "",
        fields: [
          {
            displayName: Translate("Save"),
            type: "button",
            key: "savebutton",
          },
        ],
      },
    ],
  },
  contactUsFields: {
    sections: [
      {
        title: Translate("CONTACT"),
        fields: [
          {
            displayName: Translate("Address"),
            type: "text",
            editable: false,
            key: "contacus",
            value: "142 Steiner Street, San Francisco, CA, 94115",
          },
          {
            displayName: Translate("E-mail us"),
            value: "florian@instamobile.io",
            type: "text",
            editable: false,
            key: "email",
            placeholder: "Your email address",
          },
        ],
      },
      {
        title: "",
        fields: [
          {
            displayName: Translate("Call Us"),
            type: "button",
            key: "savebutton",
          },
        ],
      },
    ],
  },
  contactUsPhoneNumber: "+16504859694",
  homeConfig: {
    mainCategoryID: "aaPdF5Dskd3DrHMWV3SwLs",
    mainCategoryName: "Houses",
  },
};
