import React, { useState } from "react";
import { View, Text, StyleSheet, SafeAreaView } from "react-native";
import Modal from "../utils/bottomModal";
import RadioButton from "react-native-radio-button";
import { Translate } from "../core/i18n/IMLocalization";
import Colors from "../themes/colors";
import { AppStyles, ModalHeaderStyle } from "../AppStyles";

export default function SortHomeModal({ isOpen, closeSort, callBack, sortBy }) {
  let [active, setActive] = useState(sortBy);

  let data = [
    {
      title: Translate("Date"),
      secondTitle: Translate("Newest Posts"),
      value: "new",
    },
    {
      title: Translate("Date"),
      secondTitle: Translate("Oldest Posts"),
      value: "old",
    },
    {
      title: Translate("Price"),
      secondTitle: Translate("Lowest Price"),
      value: "lowest",
    },
    {
      title: Translate("Price"),
      secondTitle: Translate("Highest Price"),
      value: "highest",
    },
    {
      title: Translate("Top Posts"),
      value: "top",
    },
    {
      title: Translate("Nearest"),
      value: "near",
    },
  ];

  const changeRadio = (value) => {
    setActive(value);
    callBack(value);
    closeSort();
  };

  return (
    <Modal isOpen={isOpen} closeModal={closeSort}>
      <SafeAreaView style={styles.container}>
        <Text style={ModalHeaderStyle.text.title}>{Translate("Sort")}</Text>
        {data.map((obj) => {
          return (
            <View style={styles.item}>
              <RadioButton
                animation={"bounceIn"}
                isSelected={active == obj.value}
                onPress={() => changeRadio(obj.value)}
                size={14}
              />
              <View
                style={{
                  paddingHorizontal: 10,
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Text style={[AppStyles.text.listValue]}>
                  {obj?.title}
                  {obj.secondTitle && ":"}
                </Text>
                <Text style={[AppStyles.text.listValue, { paddingLeft: 5 }]}>
                  {obj?.secondTitle}
                </Text>
              </View>
            </View>
          );
        })}
      </SafeAreaView>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  item: {
    width: "100%",
    height: 60,
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 20,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    minHeight: 10,
  },
});
