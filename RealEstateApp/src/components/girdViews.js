import React, { useState } from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import Modal from "../utils/bottomModal";
import RadioButton from "react-native-radio-button";
import Colors from "../themes/colors";
import { AppStyles, ModalHeaderStyle } from "../AppStyles";
import { Translate } from "../core/i18n/IMLocalization";
const { width } = Dimensions.get("window");

export default function GridModal({ isOpen, closeSort, callBack, grid }) {
  let [active, setActive] = useState(grid);

  let data = [
    {
      title: "Images",
      value: "images",
    },
    {
      title: "Lists",
      value: "lists",
    },
    {
      title: "Simple Text",
      value: "text",
    },
  ];

  const changeRadio = (value) => {
    setActive(value);
    callBack(value);
    closeSort();
  };

  return (
    <Modal isOpen={isOpen} closeModal={closeSort}>
      <View style={styles.container}>
        <Text style={ModalHeaderStyle.text.title}>
          {Translate("Grid View")}
        </Text>
        {data.map((obj) => {
          return (
            <View style={styles.item}>
              <RadioButton
                color={Colors.green}
                animation={"bounceIn"}
                isSelected={active == obj.value}
                onPress={() => changeRadio(obj.value)}
                size={14}
              />
              <Text
                style={[AppStyles.text.listValue, { paddingHorizontal: 10 }]}
              >
                {obj.title}
              </Text>
            </View>
          );
        })}
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  item: {
    width: "100%",
    height: 60,
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 20,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    minHeight: 10,
  },
});
