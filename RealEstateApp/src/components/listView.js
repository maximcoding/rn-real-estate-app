import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";
import moment from "moment";
import { ListStyle } from "../AppStyles";
import { getSymbol, getPrice } from "../utils/currencyConverter";
import { Colors } from "../themes";
import NumberFormat from "react-number-format";

export default function ListView({
  item,
  onPressListingItem,
  currencyRates,
  user,
}) {
  return (
    <TouchableOpacity
      onPress={() => onPressListingItem(item)}
      onLongPress={() => {
        onLongPressListingItem(item);
      }}
    >
      <View style={styles.rowM}>
        <Image source={{ uri: item.photo }} style={ListStyle.avatarStyle} />
        <View
          style={{
            alignItems: "flex-start",
            justifyContent: "space-between",
            paddingHorizontal: 15,
            paddingVertical: 15,
          }}
        >
          <Text style={ListStyle.title}>{item.title}</Text>
          <Text style={ListStyle.time}>
            {moment(item.createdAt).fromNow(true)} ago
          </Text>
          <Text style={ListStyle.place}>{item.place.slice(0, 20)}</Text>
        </View>
        <NumberFormat
          renderText={(text) => (
            <Text style={[ListStyle.price, styles.pri]}>{text}</Text>
          )}
          value={getPrice(Number(item.price), user, currencyRates)}
          displayType={"text"}
          thousandSeparator={true}
          prefix={getSymbol(user)}
        />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  pri: { position: "absolute", bottom: 0, right: 5 },

  rowM: {
    flexDirection: "row",
    marginTop: 20,
    height: 120,
    width: "100%",
    backgroundColor: Colors.backgroundColor,
  },
});
