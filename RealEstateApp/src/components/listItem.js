import React from "react";
import { View, Text, TouchableOpacity, ImageBackground } from "react-native";
import { styles } from "../screens/homeScreenStyle";
import { AppIcon, AppStyles } from "../AppStyles";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { Translate } from "../core/i18n/IMLocalization";
import moment from "moment";
import StarRating from "react-native-star-rating";
import { getSymbol, getPrice, unitConverter } from "../utils/currencyConverter";
import Colors from "../themes/colors";
import Fonts from "../themes/fonts";
import Images from "../themes/images";
import NumberFormat from "react-number-format";

export default function ListItem({
  item,
  onPressListingItem,
  onLongPressListingItem,
  onPressSavedIcon,
  savedPosts,
  currencyRates,
  user,
}) {
  return (
    <TouchableOpacity
      onPress={() => {
        onPressListingItem(item);
      }}
      onLongPress={() => {
        onLongPressListingItem(item);
      }}
      activeOpacity={1}
      style={{
        height: 240,
        borderRadius: 10,
        width: "100%",
        marginBottom: 20,
        backgroundColor: "white",
        shadowColor: Colors.primary,
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
        borderWidth: 1,
        borderColor: Colors.border,
      }}
    >
      <ImageBackground
        borderRadius={7}
        source={{ uri: item.photo }}
        style={styles.imageBackground}
      >
        <View style={styles.itemContainer}>
          <View style={{ ...styles.addressWrapper }}>
            <FontAwesome5
              style={{ ...styles.icon, fontSize: Fonts.size.normal }}
              name={"map-marker-alt"}
            />
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={AppStyles.text.description}
            >
              {item.place}
            </Text>
          </View>
          <View style={styles.detailsWrapper}>
            <View style={{ ...styles.rowDetails }}>
              <View style={styles.leftColumn}></View>
              <View style={styles.rightColumn}>
                <View style={styles.textWrapper}>
                  <Text style={styles.label}>{Translate("Price")}:</Text>
                  {/* <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={styles.priceValue}
                  > */}
                  <NumberFormat
                    renderText={(text) => (
                      <Text
                        ellipsizeMode="tail"
                        numberOfLines={1}
                        style={styles.priceValue}
                      >
                        {text}
                      </Text>
                    )}
                    value={getPrice(Number(item.price), user, currencyRates)}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={getSymbol(user)}
                  />
                  {/* </Text> */}
                </View>
              </View>
            </View>
            <View style={{ ...styles.rowDetails, paddingBottom: "2%" }}>
              <View style={styles.leftColumn}></View>
              <View style={styles.rightColumn}>
                <View style={styles.textWrapper}>
                  <Text style={styles.label}>{Translate("Deposit")}:</Text>
                  <Text style={styles.value}>{item.deposit} %</Text>
                </View>
              </View>
            </View>
            <View style={{ ...styles.rowDetails }}>
              <View style={styles.leftColumn}>
                <FontAwesome5 style={styles.icon} name={"signal"} />
                <View style={styles.textWrapper}>
                  <Text style={styles.value}>{item.floors}</Text>
                  <Text style={styles.label}>{Translate("Floor")}</Text>
                </View>
              </View>
            </View>
            <View style={{ ...styles.rowDetails }}>
              <View style={styles.leftColumn}>
                <FontAwesome5 style={styles.icon} name={"bed"} />
                <View style={styles.textWrapper}>
                  <Text style={styles.value}>{item.bedroom}</Text>
                  <Text style={styles.label}>{Translate("Bedrooms")}</Text>
                </View>
              </View>
              <View style={styles.rightColumn}>
                <View style={styles.textWrapper}>
                  <Text style={styles.valueDate}>
                    {moment(item.createdAt).format("MM/DD/YYYY")}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                ...styles.rowDetails,
                justifyContent: "flex-start",
                width: "35%",
                paddingLeft: 5,
                backgroundColor: Colors.backgroundText,
              }}
            >
              <FontAwesome5 style={styles.icon} name={"bath"} />
              <View style={styles.textWrapper}>
                <Text style={styles.value}>{item.bathroom}</Text>
                <Text style={styles.label}>{Translate("Bathrooms")}</Text>
              </View>
            </View>
            <View style={{ ...styles.rowDetails }}>
              <View style={styles.leftColumn}>
                <FontAwesome5 style={styles.icon} name={"sign"} />
                <View style={styles.textWrapper}>
                  <Text style={styles.value}>
                    {unitConverter(Number(item.square), user)}
                  </Text>
                  <Text style={styles.label}>{user.unit}</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.shareCompareButtons}>
            <FontAwesome5
              name={"plus"}
              size={25}
              color={"black"}
              style={{ paddingTop: 50, color: "white" }}
            />
            <FontAwesome5
              name={"share-alt"}
              size={25}
              color={"black"}
              style={{
                paddingRight: 10,
                paddingTop: 30,
                color: "white",
              }}
            />
            <TouchableOpacity onPress={() => onPressSavedIcon(item)}>
              <FontAwesome
                name={"heart"}
                size={25}
                style={{
                  paddingRight: 10,
                  paddingTop: 30,
                  color:
                    savedPosts.findIndex((k) => k == item.id) >= 0
                      ? "red"
                      : "white",
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.starRatingWrapper}>
            <StarRating
              containerStyle={styles.starRating}
              maxStars={5}
              starSize={15}
              disabled={true}
              starStyle={styles.starStyle}
              emptyStar={Images.starNoFilled}
              fullStar={Images.starFilled}
              halfStarColor={Colors.foregroundColor}
              rating={item.starCount ? item.starCount : 1}
            />
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
}
