import React, { useState, memo } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import TextButton from "react-native-button";
import { AppStyles, ModalHeaderStyle } from "../AppStyles";
import Button from "react-native-button";
import { Translate } from "../core/i18n/IMLocalization";
import { replyToReview } from "../redux/actions/review";
import { useDispatch } from "react-redux";
import Modal from "react-native-modal";
let { width } = Dimensions.get("window");
import Fonts from "../themes/fonts";
import Colors from "../themes/colors";

export default memo(function ReplyModal(props) {
  const dispatch = useDispatch();
  let [state, setState] = useState({
    content: "",
    review: props.review,
  });
  let [loader, setLoader] = useState(false);
  const onCancel = () => {
    props.onCancel();
  };

  const stopLoader = () => {
    setLoader(false);
    props.onCancel();
  };

  const onPostReply = () => {
    if (!state.content) {
      alert(Translate("Please enter reply"));
      return;
    }
    setLoader(true);
    const { review, content } = state;
    dispatch(replyToReview(review, content, stopLoader));
  };

  return (
    <Modal
      animationType="slide"
      onRequestClose={onCancel}
      isVisible={true}
      style={{ margin: 0 }}
    >
      <View style={styles.body}>
        <>
          <View style={styles.bodyContainer}>
            <View style={ModalHeaderStyle.bar}>
              <Text style={ModalHeaderStyle.title}>Add Reply</Text>
              <TextButton
                style={{ ...ModalHeaderStyle.rightButton, paddingRight: 10 }}
                onPress={onCancel}
              >
                {Translate("Cancel")}
              </TextButton>
            </View>

            <TextInput
              multiline={true}
              style={styles.input}
              onChangeText={(text) => setState({ ...state, content: text })}
              value={state.content}
              placeholder={Translate("Start typing")}
              placeholderTextColor={Colors.grey}
              underlineColorAndroid="transparent"
            />
            <Button
              containerStyle={styles.btnContainer}
              style={styles.btnText}
              onPress={() => onPostReply()}
            >
              {loader ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                "POST"
              )}
            </Button>
          </View>
        </>
      </View>
    </Modal>
  );
});

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: "flex-end",
  },
  bodyContainer: {
    alignSelf: "center",
    width: width,
    backgroundColor: "white",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  input: {
    width: "90%",
    fontSize: 19,
    height: 200,
    borderColor: "#d3d3d3",
    borderWidth: 0.7,
    textAlignVertical: "top",
    lineHeight: 26,
    fontFamily: Fonts.family.main,
    color: Colors.text,
    alignSelf: "center",
    padding: 10,
    borderRadius: 10,
    marginBottom: 20,
  },
  starRatingContainer: {
    width: 90,
    marginVertical: 12,
    marginHorizontal: 20,
  },
  starStyle: {
    tintColor: Colors.foregroundColor,
  },
  btnContainer: {
    width: width,
    height: 48,
    justifyContent: "center",
    backgroundColor: Colors.foregroundColor,
  },
  btnText: {
    color: Colors.white,
  },
});
