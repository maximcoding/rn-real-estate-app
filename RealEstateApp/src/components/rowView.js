import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import moment from "moment";
import EIcon from "react-native-vector-icons/Entypo";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { getSymbol, getPrice } from "../utils/currencyConverter";
import { Colors, Fonts } from "../themes";
import NumberFormat from "react-number-format";

export default function RowView({
  item,
  onPressSavedIcon,
  savedPosts,
  onPressListingItem,
  onLongPressListingItem,
  currencyRates,
  user,
}) {
  return (
    <TouchableOpacity
      onPress={() => onPressListingItem(item)}
      onLongPress={() => {
        onLongPressListingItem(item);
      }}
      style={styles.container}
    >
      <Text style={styles.address} numberOfLines={1} ellipsizeMode="tail">
        <EIcon name="address" color="gray" /> {item.place}
      </Text>
      <View style={styles.row}>
        <TouchableOpacity onPress={() => onPressSavedIcon(item)}>
          <FontAwesome
            name={"heart"}
            size={15}
            style={{
              color:
                savedPosts.findIndex((k) => k == item.id) >= 0
                  ? "red"
                  : "#d3d3d3",
            }}
          />
        </TouchableOpacity>

        <NumberFormat
          renderText={(text) => <Text style={styles.price}>{text}</Text>}
          value={getPrice(Number(item.price), user, currencyRates)}
          displayType={"text"}
          thousandSeparator={true}
          prefix={getSymbol(user)}
        />

        <Text style={styles.date}>
          <EIcon name="clock" color="gray" />
          {moment(item.createdAt).fromNow(true)} ago
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: Colors.backgroundColor,
    marginVertical: 5,
    padding: 10,
  },
  address: {
    fontSize: Fonts.style.normal,
    color: Colors.title,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: 20,
  },
  price: {
    color: "gray",
    fontSize: Fonts.size.small,
  },
  date: {
    color: "gray",
    fontSize: Fonts.size.small,
  },
});
