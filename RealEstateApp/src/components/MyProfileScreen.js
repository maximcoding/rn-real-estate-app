import React, { Component } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import { AppIcon, AppStyles } from "../AppStyles";
import authManager from "../core/onboarding/AuthManager";
import { AppConfig } from "../AppConfig";
import { Translate } from "../core/i18n/IMLocalization";
import { dispatchSetUserData, dispatchLogOut } from "../redux/reducers";
import { IMUserProfileComponent } from "../core/profile/ui/components/IMUserProfileComponent";
import Images from "../themes/images";
import { logoutUser } from "../redux/actions/user";

export const MyProfileScreenOptions = () => {
  let currentTheme = AppStyles.navThemeConstants.light;
  return {
    title: Translate("Profile"),
    headerTitleStyle: AppStyles.text.headerTitleStyle,
  };
};

class MyProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription && this.didFocusSubscription();
    this.willBlurSubscription && this.willBlurSubscription();
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  onLogout = async () => {
    this.props.logoutUser();
  };

  onUpdateUser = (newUser) => {
    this.props.dispatchSetUserData({ user: newUser });
  };

  render() {
    const menuItems = [
      {
        title: Translate("Account Details"),
        subTitle: "Account",
        icon: Images.accountDetail,
        tintColor: "#6b7be8",
        onPress: () =>
          this.props.navigation.navigate("AccountDetail", {
            appStyles: AppStyles,
          }),
      },

      {
        title: Translate("Settings"),
        icon: Images.settings,
        tintColor: "#a6a4b1",
        onPress: () => {
          this.props.navigation.navigate("Settings");
        },
      },

      {
        title: Translate("Messages"),
        icon: Images.communication,
        tintColor: "#968cbf",
        onPress: () => {
          this.props.navigation.navigate("Messages");
        },
      },
      {
        title: Translate("My Favorites"),
        tintColor: "#df9292",
        icon: Images.wishlistFilled,
        onPress: () =>
          this.props.navigation.navigate("ListingScreen", { type: "fav" }),
      },

      {
        title: Translate("Bids"),
        icon: Images.bell,
        tintColor: "#a6a4b1",
        onPress: () => {},
      },
      {
        title: Translate("Selling"),
        icon: Images.homefilled,
        tintColor: "#a6a4b1",
        onPress: () => {
          this.props.navigation.navigate("ListingScreen", { type: "buy" });
        },
      },
      {
        title: Translate("Renting"),
        icon: Images.homefilled,
        tintColor: "#a6a4b1",
        onPress: () => {
          this.props.navigation.navigate("ListingScreen", { type: "rent" });
        },
      },
      {
        title: Translate("Recent Viewed"),
        icon: Images.homefilled,
        tintColor: "#a6a4b1",
        onPress: () => {
          this.props.navigation.navigate("ListingScreen", { type: "recent" });
        },
      },
      {
        title: Translate("Payment Option"),
        icon: Images.compose,
        tintColor: "#a6a4b1",
        onPress: () => {},
      },
      // {
      //   title: Translate("Help"),
      //   icon: Images.settings,
      //   tintColor: "#a6a4b1",
      //   onPress: () => {},
      // },
      {
        title: Translate("Contact Us"),
        icon: Images.contactUs,
        tintColor: "#9ee19f",
        onPress: () => this.props.navigation.navigate("Contact"),
      },
    ];

    // if (this.props.isAdmin) {
    //   menuItems.push({
    //     title: Translate("Admin Dashboard"),
    //     tintColor: "#8aced8",
    //     icon: Images.checklist,
    //     onPress: () => this.props.navigation.navigate("AdminDashboard"),
    //   });
    // }

    return (
      <IMUserProfileComponent
        user={this.props.user}
        onUpdateUser={(user) => this.onUpdateUser(user)}
        onLogout={this.onLogout}
        menuItems={menuItems}
        onPressSettings={() => this.onPressSettings()}
        appStyles={AppStyles}
      />
    );
  }
}

const mapStateToProps = ({ auth, languageReducer }) => {
  return {
    user: auth.user,
    isAdmin: !!auth.user?.isAdmin,
    updator: languageReducer.updator,
  };
};

export default connect(mapStateToProps, {
  dispatchLogOut: dispatchLogOut,
  dispatchSetUserData: dispatchSetUserData,
  logoutUser,
})(MyProfileScreen);
