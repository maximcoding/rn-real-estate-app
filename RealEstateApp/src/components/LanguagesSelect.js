import React, { useState } from "react";
import { StyleSheet, View, Button, TouchableOpacity } from "react-native";
import { useSafeArea } from "react-native-safe-area-context";
import RNPickerSelect from "react-native-picker-select";
import { LocalizationContext } from "../core/i18n/Localization";
import { DEFAULT_LANGUAGE_CODE } from "../core/i18n";
import { Translate } from "../core/i18n/IMLocalization";
import { AppStyles } from "../AppStyles";
import { Colors } from "../themes";

export const LanguagesSelect = () => {
  // const { t, i18n } = React.useContext(LocalizationContext);
  const insets = useSafeArea();
  const [selectedValue, setSelectedValue] = useState(
    DEFAULT_LANGUAGE_CODE.languageTag
  );
  const handleLanguageChange = (value) => {
    // i18n.changeLanguage(value);
    setSelectedValue(value);
  };
  return (
    <TouchableOpacity>
      <View style={styles.container}>
        <RNPickerSelect
          placeholder={{
            label: Translate("Select Language"),
          }}
          value={selectedValue}
          onValueChange={handleLanguageChange}
          items={[
            { label: "Arabic", value: "ar" },
            { label: "English", value: "en" },
            { label: "Hebrew", value: "he" },
            { label: "Russian", value: "ru" },
            { label: "French", value: "fr" },
          ]}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: Colors.grey,
  },
});
