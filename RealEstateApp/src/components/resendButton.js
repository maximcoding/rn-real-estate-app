import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default function ResendButton({ resendSms }) {
  let [counter, setCounter] = useState(60);

  useEffect(() => {
    if (counter == 0) {
      return;
    }
    setTimeout(() => {
      setCounter(counter - 1);
    }, 1000);
  }, [counter]);

  const resendCode = () => {
    setCounter(60);
    resendSms();
  };
  return (
    <View style={styles.row}>
      <Text style={styles.text}>Resend code in {counter}</Text>
      <TouchableOpacity
        style={styles.btn}
        disabled={counter !== 0}
        onPress={resendCode}
      >
        <Text style={{ color: counter !== 0 ? "gray" : "green", fontSize: 17 }}>
          Resend
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "65%",
    height: 50,
    alignSelf: "center",
  },
  text: {
    color: "black",
    fontSize: 17,
    paddingRight:10
  },
  btn: {
    justifyContent: "center",
    alignItems: "center",
  },
});
