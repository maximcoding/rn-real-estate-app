import React, { memo, useEffect, useState } from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Switch,
  Text,
  Image,
  View,
  ScrollView,
} from "react-native";
import { AppIcon, AppStyles, TwoColumnListStyle } from "../AppStyles";
import RNPickerSelect from "react-native-picker-select";
import { setI18nConfig } from "../core/i18n/IMLocalization";
import AsyncStorage from "@react-native-community/async-storage";
import { Picker } from "@react-native-community/picker";
import { Translate } from "../core/i18n/IMLocalization";
import { updateLanguage } from "../redux/actions/language";
import { useDispatch, useSelector } from "react-redux";
import { updateUser } from "../redux/actions/user";
import CountryPicker from "react-native-country-picker-modal";
import Icon from "react-native-vector-icons/Ionicons";
import Collapsible from "react-native-collapsible";
import Colors from "../themes/colors";
import Images from "../themes/images";
import { Fonts } from "../themes";
const listLanguage = [
  { value: "ar", label: "🇸🇦 Arabic" },
  { value: "en", label: "🇺🇸 English" },
  { value: "fr", label: "🇫🇷 French" },
  { value: "heb", label: "🇮🇱 Hebrew" },
  { value: "ru", label: "🇷🇺 Russian" },
];

const notificationsData = [
  {
    title: "New Reviews",
    value: "newReviews",
  },
  {
    title: "Auction Bid Changes",
    value: "bidChanges",
  },
  {
    title: "Email Notifications",
    value: "emailNotifications",
  },
  {
    title: "New Messages",
    value: "newMessages",
  },
  {
    title: "News And New Features",
    value: "newFeatures",
  },
  {
    title: "Disable All Notifications",
    value: "disabledNotifications",
  },
];

const currencyUnits = [
  { value: "dollar", label: "$ Dollar" },
  { value: "euro", label: "€ Euro " },
  { value: "sign", label: "₪ Sign " },
];

const listUnits = [
  { value: "meter", label: "Meters" },
  { value: "feet", label: "Feets" },
];

function SettingScreen() {
  let dispatch = useDispatch();
  let { user } = useSelector((state) => state.auth);

  let [state, setState] = useState({
    country: user.country ? user.country : Translate("Select"),
    language: "en",
    squareUnits: user.unit,
    detect: true,
    newReviews: true,
    bidChanges: true,
    emailNotifications: true,
    newMessages: true,
    newFeatures: true,
    disabledNotifications: false,
    currencyUnit: user.currency,
  });
  let [visible, setVisible] = useState(false);
  let [show, setShow] = useState(false);

  const starting = async () => {
    let lang = await AsyncStorage.getItem("language");
    setState({ ...state, language: lang });
  };

  useEffect(() => {
    starting();
  }, []);

  const onChangeLanguage = async (languageSelected) => {
    setState({
      ...state,
      language: languageSelected,
    });
    setI18nConfig(languageSelected);
    await AsyncStorage.setItem("language", languageSelected);
    dispatch(updateLanguage());
  };

  const onChangeUnits = (value) => {
    setState({
      ...state,
      squareUnits: value,
    });
    dispatch(updateUser({ ...user, unit: value }, user._id));
  };

  const onChangeCurrency = (value) => {
    setState({
      ...state,
      currencyUnit: value,
    });
    dispatch(updateUser({ ...user, currency: value }, user._id));
  };

  const onSelect = (country) => {
    setState({ ...state, country: country.name });
    dispatch(updateUser({ ...user, country: country.name }, user._id));
  };

  const changeNotifications = (value) => {
    if (value == "disabledNotifications" && state[value] == false) {
      setState({
        ...state,
        newReviews: false,
        bidChanges: false,
        emailNotifications: false,
        newMessages: false,
        newFeatures: false,
        disabledNotifications: true,
      });
    } else if (value == "disabledNotifications" && state[value] == true) {
      setState({
        ...state,
        newReviews: !false,
        bidChanges: !false,
        emailNotifications: !false,
        newMessages: !false,
        newFeatures: !false,
        disabledNotifications: !true,
      });
    } else {
      setState({ ...state, [value]: !state[value] });
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: Colors.backgroundColor }}>
      <ScrollView>
        <View style={TwoColumnListStyle.listingView}>
          <Text style={AppStyles.text.listTitle}>
            {Translate("Country or Region")}
          </Text>
          <View style={styles.rowC}>
            <Text>{state.country}</Text>
            <CountryPicker
              {...{
                withCountryNameButton: true,
                withFilter: true,
                withFlag: true,
                withAlphaFilter: true,
                withEmoji: true,
                withFlagButton: true,
                renderFlagButton: () => (
                  <TouchableOpacity
                    onPress={() => setVisible(true)}
                    style={{ paddingHorizontal: 15 }}
                  >
                    <Icon name="ios-caret-down-sharp" color="gray" />
                  </TouchableOpacity>
                ),
                onClose: () => setVisible(false),

                onSelect,
              }}
              visible={visible}
            />
          </View>
        </View>
        {/* <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("Signing In");
          }}
          style={TwoColumnListStyle.listingView}
        >
          <Text style={TwoColumnListStyle.listingName}>
            {Translate("Signing In")}
          </Text>
          <Image style={styles.arrowIcon} source={Images.rightArrow} />
        </TouchableOpacity> */}

        <View style={TwoColumnListStyle.listingView}>
          <View>
            <Text style={AppStyles.text.listTitle}>
              {Translate("Notifications")}
            </Text>
            <Text style={AppStyles.text.description}>
              {Translate("Turn on to receive order updates, price.")}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => setShow(!show)}
            style={{ paddingHorizontal: 15 }}
          >
            {show ? (
              <Icon name="ios-caret-up-sharp" color="gray" />
            ) : (
              <Icon name="ios-caret-down-sharp" color="gray" />
            )}
          </TouchableOpacity>
        </View>
        <Collapsible collapsed={!show}>
          <View
            style={{
              width: "100%",
              height: 400,
              backgroundColor: Colors.backgroundColor,
            }}
          >
            {notificationsData.map((item) => {
              return (
                <View style={TwoColumnListStyle.listingView}>
                  <Text style={AppStyles.text.listTitle}>{item.title}</Text>

                  <Switch
                    onValueChange={() => changeNotifications(item.value)}
                    thumbColor={"#ffff"}
                    trackColor={{ false: "#DDDD", true: "#0073eb" }}
                    value={state[item.value]}
                  />
                </View>
              );
            })}
          </View>
        </Collapsible>
        <View style={{ ...TwoColumnListStyle.listingView, marginTop: 10 }}>
          <Text style={AppStyles.text.listTitle}>
            {Translate("Auto-Detect Language")}
          </Text>
          <Switch
            onValueChange={() => setState({ ...state, detect: !state.detect })}
            thumbColor={"#ffff"}
            trackColor={{ false: "#DDDD", true: "#0073eb" }}
            value={state.detect}
          />
        </View>
        <View style={TwoColumnListStyle.listingView}>
          <Text style={AppStyles.text.listTitle}>
            {Translate("Translation")}
          </Text>

          <RNPickerSelect
            placeholder={{
              label: Translate("Select Language"),
              color: "black",
            }}
            items={listLanguage}
            onValueChange={(value) => onChangeLanguage(value)}
            style={AppStyles.pickerSelectStyles}
            value={state.language}
          />
        </View>
        <View
          style={{
            ...TwoColumnListStyle.listingView,
            marginTop: 10,
            marginBottom: 10,
          }}
        >
          <Text style={TwoColumnListStyle.listingName}>
            {Translate("Square Units")}
          </Text>

          <RNPickerSelect
            placeholder={{
              label: "Select Units",
              color: "black",
            }}
            items={listUnits}
            onValueChange={(value) => onChangeUnits(value)}
            style={AppStyles.pickerSelectStyles}
            value={state.squareUnits}
          />
        </View>

        <View
          style={{
            ...TwoColumnListStyle.listingView,
          }}
        >
          <Text style={TwoColumnListStyle.listingName}>
            {Translate("Currency")}
          </Text>

          <RNPickerSelect
            placeholder={{
              label: "Select Currency",
              color: "#000000",
            }}
            items={currencyUnits}
            onValueChange={(value) => onChangeCurrency(value)}
            style={AppStyles.pickerSelectStyles}
            value={state.currencyUnit}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            //props.navigation.navigate("User Agreement");
          }}
          style={TwoColumnListStyle.listingView}
        >
          <Text style={[TwoColumnListStyle.listingName, { color: "#d3d3d3" }]}>
            {Translate("User Agreement")}
          </Text>
          <Image style={AppStyles.navigationIcon} source={Images.rightArrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            //props.navigation.navigate("Privacy");
          }}
          style={TwoColumnListStyle.listingView}
        >
          <Text style={[TwoColumnListStyle.listingName, { color: "#d3d3d3" }]}>
            {Translate("Privacy")}
          </Text>
          <Image style={AppStyles.navigationIcon} source={Images.rightArrow} />
        </TouchableOpacity>
        <TouchableOpacity
          //onPress={() => props.navigation.navigate("About")}
          style={TwoColumnListStyle.listingView}
        >
          <Text style={[TwoColumnListStyle.listingName, { color: "#d3d3d3" }]}>
            {Translate("About")}
          </Text>
          <Image style={AppStyles.navigationIcon} source={Images.rightArrow} />
        </TouchableOpacity>
        <TouchableOpacity style={TwoColumnListStyle.listingView}>
          <Text style={TwoColumnListStyle.listingName}>
            {Translate("Share Screen")}
          </Text>
          <Image style={AppStyles.navigationIcon} source={Images.rightArrow} />
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mapView: {
    width: "100%",
    height: "100%",
    backgroundColor: Colors.backgroundColor,
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
  arrowIcon: { height: 20, width: 20 },
  rowC: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default memo(SettingScreen);
