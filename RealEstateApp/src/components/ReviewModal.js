import React, { useState, memo } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import TextButton from "react-native-button";
import StarRating from "react-native-star-rating";
import { firebaseReview } from "../firebase";
import { AppStyles, ModalHeaderStyle, AppIcon } from "../AppStyles";
import Button from "react-native-button";
import { Translate } from "../core/i18n/IMLocalization";
import { postReview } from "../redux/actions/review";
import Modal from "react-native-modal";
let { width } = Dimensions.get("window");
import Colors from "../themes/colors";
import Fonts from "../themes/fonts";
import Images from "../themes/images";
export default memo(function ReviewModal(props) {
  let [state, setState] = useState({
    starCount: 0,
    content: "",
    data: props.listing,
  });
  let [loader, setLoader] = useState(false);
  const onCancel = () => {
    props.onCancel();
  };

  const stopLoader = () => {
    setLoader(false);
    props.onDone();
  };

  const onPostReview = () => {
    if (!state.content) {
      alert(Translate("Please enter a review before submitting."));
      return;
    }
    setLoader(true);
    const { user, dispatch } = props;
    const { data, starCount, content } = state;
    dispatch(postReview(user, data, starCount, content, stopLoader));
  };

  return (
    <Modal
      animationType="slide"
     // onRequestClose={onCancel}
      onBackdropPress={onCancel}
      isVisible={true}
      style={{ margin: 0 }}
    >
      <View style={styles.body}>
        <>
          <View style={styles.bodyContainer}>
            <View style={ModalHeaderStyle.bar}>
              <Text style={ModalHeaderStyle.title}>
                {Translate("Add Review")}
              </Text>
              <TextButton
                style={{ ...ModalHeaderStyle.rightButton, paddingRight: 10 }}
                onPress={onCancel}
              >
                {Translate("Cancel")}
              </TextButton>
            </View>
            <StarRating
              containerStyle={styles.starRatingContainer}
              disabled={false}
              maxStars={5}
              starSize={25}
              starStyle={styles.starStyle}
              selectedStar={(rating) =>
                setState({ ...state, starCount: rating })
              }
              emptyStar={Images.starNoFilled}
              halfStarColor={Colors.foregroundColor}
              fullStar={Images.starFilled}
              rating={state.starCount}
            />
            <TextInput
              multiline={true}
              style={styles.input}
              onChangeText={(text) => setState({ ...state, content: text })}
              value={state.content}
              placeholder={Translate("Start typing")}
              placeholderTextColor={Colors.grey}
              underlineColorAndroid="transparent"
            />
            <Button
              containerStyle={styles.btnContainer}
              style={styles.btnText}
              onPress={() => onPostReview()}
            >
              {loader ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                Translate("Add review")
              )}
            </Button>
          </View>
        </>
      </View>
    </Modal>
  );
});

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: "flex-end",
  },
  bodyContainer: {
    alignSelf: "center",
    width: width,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  input: {
    width: "90%",
    fontSize: 19,
    height: 200,
    borderColor: "#d3d3d3",
    borderWidth: 0.7,
    textAlignVertical: "top",
    lineHeight: 26,
    fontFamily: Fonts.family.main,
    color: Colors.text,
    alignSelf: "center",
    padding: 10,
    borderRadius: 10,
    marginBottom: 20,
  },
  starRatingContainer: {
    width: 90,
    marginVertical: 12,
    marginHorizontal: 20,
  },
  starStyle: {
    tintColor: Colors.foregroundColor,
  },
  btnContainer: {
    width: width,
    height: 48,
    justifyContent: "center",
    backgroundColor: Colors.foregroundColor,
  },
  btnText: {
    color: Colors.white,
  },
});
