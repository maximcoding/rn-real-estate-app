import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import StarRating from "react-native-star-rating";
import FastImage from "react-native-fast-image";
import { AppIcon, AppStyles } from "../AppStyles";
import moment from "moment";
import ReplyModal from "../components/replyModal";
import Fonts from "../themes/fonts";
import Colors from "../themes/colors";
import Images from '../themes/images'

export default function Review({ item, index, i, showReply }) {
  let [visible, setVisible] = useState(false);

  const onCancel = () => {
    setVisible(false);
  };

  const replyNow = () => {
    setVisible(true);
  };
  return (
    <View
      style={[
        styles.reviewItem,
        { backgroundColor: i == index ? "#f0f5f5" : "white" },
      ]}
      key={i}
    >
      <View style={styles.info}>
        <FastImage
          style={styles.userPhoto}
          resizeMode={FastImage.resizeMode.cover}
          source={
            item.image
              ? { uri: item.image }
              : require("../../assets/images/agent.jpeg")
          }
        />
        <View style={styles.detail}>
          <Text style={styles.username}>
            {item.firstName && item.firstName} {item.lastName && item.lastName}
          </Text>
          <Text style={styles.reviewTime}>
            {moment(item.createdAt).fromNow(true)} ago
          </Text>
        </View>
        <StarRating
          containerStyle={styles.starRatingContainer}
          disabled={true}
          maxStars={5}
          starSize={22}
          starStyle={styles.starStyle}
          emptyStar={Images.starNoFilled}
          fullStar={Images.starFilled}
          halfStarColor={Colors.foregroundColor}
          rating={item.starCount}
        />
      </View>
      <Text style={styles.reviewContent}>{item.content}</Text>

      {showReply && (
        <TouchableOpacity onPress={replyNow}>
          <Text style={styles.reply}>Reply</Text>
        </TouchableOpacity>
      )}

      {item.replies.length > 0 &&
        item.replies.map((reply) => {
          return (
            <View style={styles.replyContainer}>
              <View style={styles.replyRow}>
                <Text style={{ fontWeight: "700" }}>Agent</Text>
                <Text style={styles.replyTime}>
                  {moment(reply.createdAt).fromNow(true)} ago
                </Text>
              </View>
              <Text style={{ color: "gray", paddingTop: 3 }}>{reply.text}</Text>
            </View>
          );
        })}

      {visible && <ReplyModal onCancel={onCancel} review={item} />}
    </View>
  );
}

const styles = StyleSheet.create({
  reviewItem: {
    padding: 10,
    marginLeft: 10,
  },

  info: {
    flexDirection: "row",
  },
  userPhoto: {
    width: 44,
    height: 44,
    borderRadius: 22,
  },
  detail: {
    paddingLeft: 10,
    flex: 1,
  },
  username: {
    color: Colors.title,
    fontWeight: "bold",
  },
  reviewTime: {
    color: "#bcbfc7",
    fontSize: 12,
  },
  replyTime: {
    color: "gray",
    fontSize: 12,
    paddingLeft: 5,
  },
  starRatingContainer: {
    padding: 10,
  },
  starStyle: {
    tintColor: "#21c064",
  },
  reviewContent: {
    color: "gray",
    marginTop: 10,
  },
  reply: {
    fontSize: 12,
    paddingTop: 3,
    alignSelf: "flex-end",
    paddingRight: 10,
    color: "#4da6ff",
    fontWeight: "700",
  },
  replyContainer: {
    padding: 10,
    backgroundColor: "#E8E8E8",
    marginTop: 5,
    width: "90%",
    marginLeft: "auto",
  },
  replyRow: {
    flexDirection: "row",
    alignItems: "center",
  },
});
