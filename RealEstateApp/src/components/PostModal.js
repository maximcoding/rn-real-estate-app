import React, { useState, useEffect } from "react";
import {
  Modal,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
  Text,
  View,
  Dimensions,
  SafeAreaView,
  I18nManager,
} from "react-native";
import Geolocation from "@react-native-community/geolocation";
import ModalSelector from "react-native-modal-selector";
import {
  AppStyles,
  ListStyle,
  ModalHeaderStyle,
  ModalSelectorStyle,
} from "../AppStyles";
import TextButton from "react-native-button";
import FastImage from "react-native-fast-image";
import { useSelector } from "react-redux";
import ImagePicker from "react-native-image-picker";
import Icon from "react-native-vector-icons/FontAwesome";
import EIcon from "react-native-vector-icons/Entypo";
import SelectLocationModal from "../components/SelectLocationModal";
import ActionSheet from "react-native-actionsheet";
import Geocoder from "react-native-geocoding";
import ServerConfig from "../ServerConfig";
import { Translate } from "../core/i18n/IMLocalization";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import NumberFormat from "react-number-format";
import RNPickerSelect from "react-native-picker-select";
import {
  getInitialState,
  onPost,
  typesData,
  newConTypes,
  conDateTypes,
} from "../utils/postHelper";
import { getSymbol } from "../utils/currencyConverter";
import SliderCustomLabel from "./SliderCustomLabel";
let { width } = Dimensions.get("window");
import Colors from "../themes/colors";
import Fonts from "../themes/fonts";

export default function PostModal({
  categories,
  onCancel,
  isVisible,
  selectedItem,
}) {
  let [state, setState] = useState(getInitialState(categories, selectedItem));
  let { user, currencyRates } = useSelector((state) => state.auth);
  let ActionSheetRef;
  Geocoder.init("AIzaSyA4JsBWizKjDWSrf-vxPvcn8WZEYVn_LUU");
  useEffect(() => {
    Geolocation.getCurrentPosition(
      (position) => {
        setState({ ...state, location: position.coords });
        onChangeLocation(position.coords);
      },
      (error) => console.log(error.message),
      { enableHighAccuracy: false, timeout: 1000 }
    );
  }, []);

  const selectLocation = () => {
    setState({ ...state, locationModalVisible: true });
  };

  const onChangeLocation = async (location) => {
    try {
      let json = await Geocoder.from(location.latitude, location.longitude);
      const addressComponent = json.results[0].formatted_address;
      setState({
        ...state,
        address: addressComponent,
        locationModalVisible: false,
      });
    } catch (error) {
      setState({ ...state, address: "Unknown" });
    }
  };

  const onSelectLocationDone = (location) => {
    setState({ ...state, location, locationModalVisible: false });
    onChangeLocation(location);
  };

  const onSelectLocationCancel = () => {
    setState({ ...state, locationModalVisible: false });
  };

  const onPressAddPhotoBtn = () => {
    const options = {
      title: Translate("Select a photo"),
      storageOptions: {
        skipBackup: true,
        path: "images",
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        alert(response.error);
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        setState({
          ...state,
          localPhotos: [...state.localPhotos, response],
        });
      }
    });
  };

  const showActionSheet = (index) => {
    setState({
      ...state,
      selectedPhotoIndex: index,
    });
    ActionSheetRef.show();
  };

  const onActionDone = (index) => {
    if (index == 0) {
      var array = [...state.localPhotos];
      array.splice(state.selectedPhotoIndex, 1);
      setState({ ...state, localPhotos: array });
    }
  };

  const onPostFunc = () => {
    onPost(
      state,
      setState,
      ServerConfig,
      user,
      onCancel,
      selectedItem,
      currencyRates
    );
  };

  return (
    <Modal
      visible={isVisible}
      animationType="slide"
      transparent={false}
      onRequestClose={onCancel}
    >
      <SafeAreaView>
        <Text style={[AppStyles.text.modalTitle]}>
          {selectedItem ? Translate("Edit Listing") : Translate("Add Listing")}
        </Text>
        <View style={ModalHeaderStyle.bar}>
          <TextButton style={[AppStyles.text.headerButton]} onPress={onCancel}>
            {Translate("Cancel")}
          </TextButton>
        </View>
      </SafeAreaView>
      <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
        <View style={[styles.section]}>
          <Text style={AppStyles.text.listTitle}>{Translate("Title")}</Text>
          <View style={{ paddingTop: 5 }}>
            <TextInput
              style={[AppStyles.text.placeholder]}
              onChangeText={(text) => setState({ ...state, title: text })}
              placeholder={Translate("Start typing")}
              underlineColorAndroid="transparent"
            />
          </View>
        </View>
        <View style={styles.horizontalLine}></View>
        <View style={styles.section}>
          <Text style={ListStyle.title}>{Translate("Description")}</Text>
          <View style={{ paddingTop: 5 }}>
            <TextInput
              style={AppStyles.text.placeholder}
              multiline={true}
              numberOfLines={2}
              onChangeText={(text) => setState({ ...state, description: text })}
              value={state.description}
              placeholder={Translate("Start typing")}
              underlineColorAndroid="transparent"
            />
          </View>
        </View>
        <View style={styles.horizontalLine}></View>
        <View style={styles.section}>
          <View style={styles.row}>
            <Text style={ListStyle.title}>{Translate("Rent or Sell")}</Text>
            <RNPickerSelect
              placeholder={{
                label: Translate("Select"),
                value: null,
                ...AppStyles.text.placeholder,
              }}
              items={typesData(Translate)}
              onValueChange={(value) => {
                setState({
                  ...state,
                  type: value,
                });
              }}
              style={AppStyles.pickerSelectStyles}
              value={state.type}
              useNativeAndroidPickerStyle={false}
            />
          </View>
          <View style={styles.row}>
            <Text style={ListStyle.title}>Is new Construction?</Text>
            <RNPickerSelect
              placeholder={{
                label: Translate("Select"),
                value: null,
                ...AppStyles.text.placeholder,
              }}
              items={newConTypes}
              onValueChange={(value) => {
                setState({
                  ...state,
                  newConstruction: value,
                });
              }}
              style={AppStyles.pickerSelectStyles}
              value={state.newConstruction}
              useNativeAndroidPickerStyle={false}
            />
          </View>
          <View style={styles.row}>
            <Text style={ListStyle.title}>{Translate("Select a Built")}</Text>
            <RNPickerSelect
              placeholder={{
                label: Translate("Select"),
                value: null,
                ...AppStyles.text.placeholder,
              }}
              items={conDateTypes}
              onValueChange={(value) => {
                setState({
                  ...state,
                  built: value,
                });
              }}
              style={AppStyles.pickerSelectStyles}
              value={state.built}
              useNativeAndroidPickerStyle={false}
            />
          </View>
        </View>

        <View style={styles.section}>
          <View style={styles.row}>
            <Text style={ListStyle.title}>
              {Translate("Price") + " " + Translate("in")} {getSymbol(user)}
            </Text>
            <>
              <NumberFormat
                value={state.price}
                renderText={(text) => <Text></Text>}
                displayType={"text"}
                thousandSeparator={true}
                mask="_"
                onValueChange={(values) => {
                  const { formattedValue, value } = values;
                  setState({ ...state, formattedPrice: formattedValue });
                }}
              />
              <TextInput
                style={AppStyles.text.placeholder}
                value={state.formattedPrice}
                onChangeText={(text) => setState({ ...state, price: text })}
                placeholder={Translate("Enter Here...")}
                keyboardType="number-pad"
                underlineColorAndroid="transparent"
                textAlign={I18nManager.isRTL ? "right" : "left"}
                maxLength={15}
              />
            </>
          </View>

          <View style={styles.row}>
            <Text style={ListStyle.title}>
              {Translate("Square in")}{" "}
              <Text
                style={[
                  AppStyles.text.listTitle,
                  { textTransform: "capitalize" },
                ]}
              >
                {user.unit}
              </Text>
            </Text>
            <TextInput
              style={AppStyles.text.placeholder}
              value={state.square}
              onChangeText={(text) => setState({ ...state, square: text })}
              placeholder={Translate("Enter Here...")}
              keyboardType="number-pad"
              underlineColorAndroid="transparent"
              maxLength={15}
              textAlign={I18nManager.isRTL ? "right" : "left"}
            />
          </View>
        </View>
        <View style={styles.section}>
          <ModalSelector
            touchableActiveOpacity={0.9}
            data={state.categories}
            sectionTextStyle={ModalSelectorStyle.sectionTextStyle}
            optionTextStyle={ModalSelectorStyle.optionTextStyle}
            optionContainerStyle={ModalSelectorStyle.optionContainerStyle}
            cancelContainerStyle={ModalSelectorStyle.cancelContainerStyle}
            cancelTextStyle={ModalSelectorStyle.cancelTextStyle}
            selectedItemTextStyle={ModalSelectorStyle.selectedItemTextStyle}
            backdropPressToClose={true}
            cancelText={Translate("Cancel")}
            initValue={state.category.name}
            onChange={(option) => {
              setState({
                ...state,
                category: { id: option.key, name: option.label },
              });
            }}
          >
            <View style={styles.rowCat}>
              <Text style={ListStyle.title}>{Translate("Category")}</Text>
              <Text
                style={[
                  AppStyles.text.placeholder,
                  {
                    fontSize: Fonts.size.regular,
                    fontFamily: "Arial",
                    color: Colors.greyPlaceholder,
                  },
                ]}
              >
                {state.category.name}
              </Text>
            </View>
          </ModalSelector>
          <View style={styles.row}>
            <Text style={ListStyle.title}>{Translate("Number of Floors")}</Text>
            <TextInput
              style={[AppStyles.text.input]}
              value={state.noFloor}
              onChangeText={(text) => setState({ ...state, noFloor: text })}
              placeholder={Translate("Enter Here...")}
              keyboardType="number-pad"
              underlineColorAndroid="transparent"
              maxLength={3}
              textAlign={I18nManager.isRTL ? "right" : "left"}
            />
          </View>
          {state.type == "buy" && (
            <View style={styles.row}>
              <Text style={ListStyle.title}>{Translate("Deposit")}</Text>
              <TextInput
                style={AppStyles.text.placeholder}
                value={state.deposit}
                onChangeText={(text) => setState({ ...state, deposit: text })}
                placeholder={Translate("Enter deposit") + " %"}
                keyboardType="number-pad"
                underlineColorAndroid="transparent"
                maxLength={2}
                textAlign={I18nManager.isRTL ? "right" : "left"}
              />
            </View>
          )}
        </View>
        <View style={styles.section}>
          <View style={styles.row}>
            <Text style={ListStyle.title}>{Translate("Location")}</Text>
            <TouchableOpacity onPress={selectLocation}>
              <EIcon
                name="location"
                color="gray"
                size={18}
                style={{ paddingLeft: 9 }}
              />
            </TouchableOpacity>
          </View>
          <View>
            <TextInput
              multiline={true}
              style={styles.addressInput}
              onChangeText={(text) => setState({ ...state, address: text })}
              value={state.address}
              underlineColorAndroid="transparent"
              textAlignVertical={"top"}
            />
          </View>
        </View>
        <View style={styles.section}>
          <View>
            {[
              { title: Translate("Bathrooms"), value: "bathroom" },
              { title: Translate("Bedrooms"), value: "bedroom" },
            ].map((o) => {
              return (
                <>
                  <View style={[styles.appSettingsTypeContainer]}>
                    <Text style={ListStyle.title}>{o.title}</Text>
                    <TextInput
                      underlineColorAndroid="transparent"
                      style={AppStyles.text.placeholder}
                      onChangeText={(text) => {
                        setState({
                          ...state,
                          [o.value]: text,
                        });
                      }}
                      keyboardType={"phone-pad"}
                      placeholder={Translate("Enter number")}
                      value={state[o.value]}
                      maxLength={3}
                      textAlign={I18nManager.isRTL ? "right" : "left"}
                    />
                  </View>
                </>
              );
            })}
          </View>
        </View>
        <View style={styles.horizontalLine}></View>
        <View style={styles.section}>
          <Text style={AppStyles.text.listTitle}>
            {Translate("Add Photos")}
          </Text>
          <ScrollView style={styles.photoList} horizontal={true}>
            {state.localPhotos.map((photo, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  showActionSheet(index);
                }}
              >
                <FastImage style={styles.photo} source={{ uri: photo.uri }} />
              </TouchableOpacity>
            ))}
            <TouchableOpacity onPress={onPressAddPhotoBtn}>
              <View
                style={[
                  styles.addButton,
                  styles.photo,
                  { backgroundColor: Colors.green },
                ]}
              >
                <Icon name="camera" size={30} color="white" />
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
        {state.locationModalVisible && (
          <SelectLocationModal
            location={state.location}
            onCancel={onSelectLocationCancel}
            onDone={onSelectLocationDone}
          />
        )}
      </ScrollView>
      <TextButton
        containerStyle={[
          styles.addButtonContainer,
          { backgroundColor: Colors.green },
        ]}
        onPress={onPostFunc}
        style={[styles.addButtonText]}
        disabled={state.loading}
      >
        {state.loading ? (
          <ActivityIndicator size="small" color={"white"} />
        ) : (
          Translate("Post Listing")
        )}
      </TextButton>
      <ActionSheet
        ref={(o) => (ActionSheetRef = o)}
        title={Translate("Confirm to delete?")}
        options={[Translate("Confirm"), Translate("Cancel")]}
        cancelButtonIndex={1}
        destructiveButtonIndex={0}
        onPress={(index) => {
          onActionDone(index);
        }}
      />
    </Modal>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  container: {
    justifyContent: "center",
    height: 65,
    alignItems: "center",
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: Colors.border,
  },

  input: {
    width: "100%",
    fontSize: 16,
    paddingRight: 0,
    fontFamily: Fonts.family.main,
    color: Colors.text,
    paddingVertical: 8,
  },
  addressInput: {
    fontSize: 19,
    fontFamily: Fonts.family.main,
    color: Colors.text,
    padding: 10,
    borderColor: "#d3d3d3",
    borderWidth: 1,
    height: 150,
    borderRadius: 3,
    margin: 10,
    textAlign: "justify",
  },
  priceInput: {
    flex: 1,
    borderRadius: 5,
    borderColor: Colors.grey,
    borderWidth: 0.5,
    height: 40,
    textAlign: "right",
    paddingRight: 5,
    fontFamily: Fonts.family.main,
    color: Colors.text,
  },
  title: {
    flex: 2,
    textAlign: "left",
    alignItems: "center",
    color: Colors.title,
    fontSize: 19,
    fontFamily: Fonts.family.main,
    fontWeight: "bold",
  },
  value: {
    fontFamily: Fonts.family.main,
    fontSize: 16,
    paddingRight: 25,
  },
  section: {
    backgroundColor: Colors.white,
    marginVertical: 10,
    paddingHorizontal: 10,
  },
  horizontalLine: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    minHeight: 10,
    alignSelf: "center",
    width: width / 1.1,
  },
  row: {
    height: 35,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  rowCat: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 10,
    justifyContent: "space-between",
  },

  addPhotoTitle: {
    color: Colors.title,
    fontSize: 19,
    paddingLeft: 10,
    marginTop: 10,
    fontFamily: Fonts.family.main,
    fontWeight: "bold",
  },
  photoList: {
    height: 70,
    marginTop: 20,
    marginRight: 10,
  },
  location: {
    alignItems: "center",
    width: "100%",
  },
  photo: {
    marginLeft: 10,
    width: 70,
    height: 70,
    borderRadius: 10,
  },

  addButton: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.foregroundColor,
  },
  photoIcon: {
    width: 50,
    height: 50,
  },
  addButtonContainer: {
    backgroundColor: Colors.foregroundColor,
    borderRadius: 5,
    padding: 15,
    margin: 10,
    marginVertical: 27,
    marginHorizontal: 20,
  },
  activityIndicatorContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
  },
  addButtonText: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 15,
  },

  titleB: {
    fontSize: 16,
    fontWeight: "700",
    color: "rgba(0,0,0,0.5)",
    paddingHorizontal: 10,
    textTransform: "capitalize",
  },

  appSettingsTypeContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 40,
  },
  text: {
    fontSize: 17,
    color: "gray",
    paddingRight: 15,
  },
  divider: {
    height: 0.5,
    width: "100%",
    alignSelf: "flex-end",
    backgroundColor: "#d3d3d3",
  },
});
