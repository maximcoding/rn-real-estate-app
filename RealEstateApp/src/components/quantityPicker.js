import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";

import Colors from "../themes/colors";
import Fonts from "../themes/fonts";

export default function QuantityPicker({ title, getValue, value, name }) {
  return (
    <View style={styles.main}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.viewMain}>
        <TouchableOpacity
          onPress={() => {
            getValue(0, name);
          }}
          style={[
            styles.button,
            { backgroundColor: value == 0 ? Colors.primary : Colors.white },
          ]}
        >
          <Text
            style={[
              styles.btnText,
              { color: value == 0 ? Colors.white : Colors.grey },
            ]}
          >
            Any
          </Text>
          <View
            style={[
              styles.btnSelector,
              { backgroundColor: value == 0 ? Colors.primary : Colors.grey },
            ]}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            getValue(1, name);
          }}
          style={[
            styles.button,
            { backgroundColor: value == 1 ? Colors.primary : Colors.white },
          ]}
        >
          <Text
            style={[
              styles.btnText,
              { color: value == 1 ? Colors.white : Colors.grey },
            ]}
          >
            1+
          </Text>
          <View
            style={[
              styles.btnSelector,
              { backgroundColor: value == 1 ? Colors.primary : Colors.grey },
            ]}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            getValue(2, name);
          }}
          style={[
            styles.button,
            { backgroundColor: value == 2 ? Colors.primary : Colors.white },
          ]}
        >
          <Text
            style={[
              styles.btnText,
              { color: value == 2 ? Colors.white : Colors.grey },
            ]}
          >
            2+
          </Text>
          <View
            style={[
              styles.btnSelector,
              { backgroundColor: value == 2 ? Colors.primary : Colors.grey },
            ]}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            getValue(3, name);
          }}
          style={[
            styles.button,
            { backgroundColor: value == 3 ? Colors.primary : Colors.white },
          ]}
        >
          <Text
            style={[
              styles.btnText,
              { color: value == 3 ? Colors.white : Colors.grey},
            ]}
          >
            3+
          </Text>
          <View
            style={[
              styles.btnSelector,
              { backgroundColor: value == 3 ? Colors.primary : Colors.grey },
            ]}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            getValue(4, name);
          }}
          style={[
            styles.button,
            { backgroundColor: value == 4 ? Colors.primary : Colors.white },
          ]}
        >
          <Text
            style={[
              styles.btnText,
              { color: value == 4  ? Colors.white : Colors.grey },
            ]}
          >
            4+
          </Text>
          <View
            style={[
              styles.btnSelector,
              { backgroundColor: value == 4 ? Colors.primary : Colors.grey },
            ]}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            getValue(5, name);
          }}
          style={[
            styles.button,
            { backgroundColor: value == 5 ? Colors.primary : Colors.white },
          ]}
        >
          <Text
            style={[
              styles.btnText,
              { color: value == 5 ? Colors.white : Colors.grey },
            ]}
          >
            5+
          </Text>
          <View
            style={[
              styles.btnSelector,
              { backgroundColor: value == 5 ? Colors.primary : Colors.grey },
            ]}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderBottomColor,
    paddingBottom: 15,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 16,
    fontWeight: "700",
    paddingVertical: 10,
    color: 'rgba(0,0,0,0.5)',
  },
  viewMain: {
    height: 40,
    width: "100%",
    backgroundColor: Colors.backgroundColor,
    borderRadius: 7,
    flexDirection: "row",
  },
  button: {
    height: "100%",
    width: "17%",
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
    backgroundColor: true ? Colors.primary : Colors.white,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 7,
  },
  btnText: {
    fontWeight: "600",
  },
  btnSelector: {
    height: "60%",
    width: 1,
    position: "absolute",
    right: 0,
  },
});
