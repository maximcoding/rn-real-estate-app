import React, { memo } from "react";
import { StyleSheet } from "react-native";
import IntlPhoneInput from "react-native-intl-phone-input";

export default ({ defaultCountry, onChangeText, inputProps }) => {
  return (
    <IntlPhoneInput
      containerStyle={styles.container}
      onChangeText={onChangeText}
      defaultCountry={defaultCountry}
      inputProps={inputProps}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    // minHeight:180
    borderColor: "rgba(0,0,0,.2)",
    borderRadius: 5,
    // marginBottom: 20,
  },
  iconView: {
    width: 70,
    alignItems: "center",
  },
  title: {
    color: "white",
    fontSize: 18,
    fontWeight: "600",
    textTransform: "uppercase",
  },
  subtitle: {
    color: "white",
    opacity: 0.8,
  },
});
