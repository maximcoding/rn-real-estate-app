import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native";

import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/Ionicons";
let { width } = Dimensions.get("window");
import { renderChangePhone } from "../core/profile/ui/components/IMUserFormData";
import { AppStyles } from "../AppStyles";

export default function VerifyModal({
  navigation,
  showWarning,
  setShowWarning,
}) {
  return (
    <Modal isVisible={showWarning} style={styles.modalS}>
      <View style={styles.modalContent}>
        <Icon name="warning" style={styles.emailIcon} />
        <Text style={styles.title}>Verify Phone Number </Text>
        <Text style={styles.textDes}>
          Before accessing house details and all other features you need to
          verify phone number Thanks
        </Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setShowWarning(false);
            navigation.navigate("Profile", {
              screen: "EditDetails",
              params: {
                data: renderChangePhone,
                appStyles: AppStyles,
              },
            });
          }}
        >
          <Text style={{ color: "white" }}>VERIFY</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalS: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalContent: {
    width: width - 40,
    backgroundColor: "white",
    alignItems: "center",
    padding: 20,
    borderRadius: 2,
  },
  emailIcon: {
    fontSize: 130,
    color: "#00cc66",
    paddingBottom: 20,
  },
  textTitle: {
    fontSize: 20,
    color: "gray",
    fontWeight: "bold",
    paddingBottom: 15,
  },

  textDes: {
    color: "gray",
    fontSize: 14,
    textAlign: "center",
    paddingBottom: 50,
  },
  button: {
    width: "90%",
    height: 40,
    backgroundColor: "#00cc66",
    justifyContent: "center",
    alignItems: "center",
  },
});
