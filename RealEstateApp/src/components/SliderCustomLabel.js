import React from "react";
import { View, Text, StyleSheet, Animated } from "react-native";
import NumberFormat from "react-number-format";
import { useSelector } from "react-redux";


const AnimatedView = Animated.createAnimatedComponent(View);

SliderCustomLabel.defaultProps = {
  leftDiff: 0,
};

const width = 50;
import Colors from "../themes/colors";
import Fonts from "../themes/fonts";

function LabelBase(props) {
  const { position, value, leftDiff, pressed, user } = props;
  const scaleValue = React.useRef(new Animated.Value(0.1)); // Behaves oddly if set to 0
  const cachedPressed = React.useRef(pressed);

  React.useEffect(() => {
    Animated.timing(scaleValue.current, {
      toValue: pressed ? 1 : 0.1,
      duration: 100,
      delay: pressed ? 0 : 1000,
      useNativeDriver: false,
    }).start();
    cachedPressed.current = pressed;
  }, [pressed]);

  return (
    Number.isFinite(position) &&
    Number.isFinite(value) && (
      <AnimatedView
        style={[
          styles.sliderLabel,
          {
            left: position - width / 2,
            transform: [
              // { translateY: width },
              // { scale: scaleValue.current },
              // { translateY: -width },
            ],
          },
        ]}
      >
        <NumberFormat
          value={value}
          renderText={(text) => (
            <Text style={styles.sliderLabelText}>{text}</Text>
          )}
          displayType={"text"}
          thousandSeparator={true}
          
        />
        {/* <Text style={styles.sliderLabelText}>{value}</Text> */}
      </AnimatedView>
    )
  );
}

export default function SliderCustomLabel(props) {
  const {
    leftDiff,
    oneMarkerValue,
    twoMarkerValue,
    oneMarkerLeftPosition,
    twoMarkerLeftPosition,
    oneMarkerPressed,
    twoMarkerPressed,
  } = props;

  let { user } = useSelector((state) => state.auth);

  return (
    <View style={styles.parentView}>
      <LabelBase
        position={oneMarkerLeftPosition}
        value={oneMarkerValue}
        leftDiff={leftDiff}
        pressed={oneMarkerPressed}
       
      />
      <LabelBase
        position={twoMarkerLeftPosition}
        value={twoMarkerValue}
        leftDiff={leftDiff}
        pressed={twoMarkerPressed}
        
      />
    </View>
  );
}

const styles = StyleSheet.create({
  parentView: {
    position: "relative",
  },
  sliderLabel: {
    top: -10,
    position: "absolute",
    alignSelf: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  sliderLabelText: {
    left: 17,
    fontSize: Fonts.size.small,
    textAlign: "center",
    color: Colors.text,
    fontWeight: "600",
  },
});
