import React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Switch,
  Text,
  Image,
  View,
  BackHandler,
  TextInput,
  ScrollView,
} from "react-native";
import { ListItem } from "react-native-elements";
import Geolocation from "@react-native-community/geolocation";
import { firebaseListing } from "../firebase";
import { AppIcon, AppStyles, ListStyle, HeaderButtonStyle } from "../AppStyles";
import HeaderButton from "../components/HeaderButton";
import { AppConfig } from "../AppConfig";
import MapView, { Marker } from "react-native-maps";
import FilterViewModal from "../components/FilterViewModal";
import { Translate } from "../core/i18n/IMLocalization";
import { timeFormat } from "../utils/timeFormat";
import Ionicons from "react-native-vector-icons/Ionicons";
import RNPickerSelect from "react-native-picker-select";
import Colors from "../themes/colors";

class SigningInScreen extends React.Component {
  static navigationOptions = ({ screenProps }) => {
    let currentTheme = AppStyles.navThemeConstants.light;
    return {
      title: Translate("Signing In"),
      headerTintColor: currentTheme.activeTintColor,
      headerTitleStyle: AppStyles.text.headerTitleStyle,
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      country: "Israel",
    };
    this.didFocusSubscription = props.navigation.addListener(
      "didFocus",
      (payload) =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );

    this.unsubscribe = null;
  }

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      (payload) =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  componentWillUnmount() {}

  onBackButtonPressAndroid = () => {
    this.props.navigation.goBack();

    return true;
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Colors.backgroundColor }}>
        <ScrollView>
          <View
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <Text
              style={{
                fontSize: 14,
                fontWeight: "500",
                paddingTop: 25,
                color: "rgba(0,0,0,0.4)",
              }}
            >
              {Translate("SECURITY")}
            </Text>
          </View>
          <TouchableOpacity
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.7)",
              }}
            >
              {Translate("Change Password")}
            </Text>
            <Image
              style={{ height: 20, width: 20 }}
              source={require("../../assets/icons/right-arrow.png")}
            />
          </TouchableOpacity>
          <View
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <View>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "500",
                  color: "rgba(0,0,0,0.7)",
                }}
              >
                {Translate("Face ID sign in")}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "500",
                  color: "rgba(0,0,0,0.5)",
                }}
              ></Text>
            </View>
            <Switch
              onValueChange={() =>
                this.setState({ useExact: !this.state.useExact })
              }
              thumbColor={"#ffff"}
              trackColor={{ false: "#DDDD", true: "#0073eb" }}
              value={this.state.useExact}
            />
          </View>
          <TouchableOpacity
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.7)",
              }}
            >
              {Translate("Two Steps verification")}
            </Text>
            <Image
              style={{ height: 20, width: 20 }}
              source={require("../../assets/icons/right-arrow.png")}
            />
          </TouchableOpacity>
          <View
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <Text
              style={{
                fontSize: 14,
                fontWeight: "500",
                paddingTop: 25,
                color: "rgba(0,0,0,0.4)",
              }}
            >
              {Translate("SOCIAL SIGN IN")}
            </Text>
          </View>
          <TouchableOpacity
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.7)",
              }}
            >
              Facebook
            </Text>
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.3)",
              }}
            >
              undefined
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.7)",
              }}
            >
              Google
            </Text>
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.3)",
              }}
            >
              undefined
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 60,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 10,
              borderBottomWidth: 1,
              borderBottomColor: Colors.border,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.7)",
              }}
            >
              Apple ID
            </Text>
            <Text
              style={{
                fontSize: 16,
                fontWeight: "500",
                color: "rgba(0,0,0,0.3)",
              }}
            >
              undefined
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mapView: {
    width: "100%",
    height: "100%",
    backgroundColor: Colors.backgroundColor,
  },
  filtersButton: {
    marginRight: 10,
  },
  toggleButton: {
    marginRight: 7,
  },
});

export default SigningInScreen;
